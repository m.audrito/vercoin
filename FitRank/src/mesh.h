#pragma once

#ifndef __MESH_H__
#define __MESH_H__

#include <utility>
#include <vector>
#include <opencv2/core/core.hpp>

// Strutture dati per il modello 3D

struct TriangleIndices {
    short v1, v2, v3;
};

struct Mesh {
    std::vector<cv::Point3f> vertices_;
    std::vector<TriangleIndices> elements_;
public:
    Mesh() = default;
    Mesh(std::vector<cv::Point3f> vertices, std::vector<TriangleIndices> elements) : vertices_(std::move(vertices)), elements_(std::move(elements)) {}

    const std::vector<cv::Point3f> &vertices() const { return vertices_; }
    std::vector<cv::Point3f> &vertices() { return vertices_; }
    const std::vector<TriangleIndices> &elements() const { return elements_; }
};

int load_model(const bool flag_sx, Mesh &main_object, const char *file_name);
Mesh scaleY(const Mesh& mesh, const float current_y_scale);
Mesh scaleX(const Mesh& mesh, const float current_x_scale);
Mesh scale(const Mesh& mesh, const float current_scale);

#endif
