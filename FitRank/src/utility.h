#pragma once

#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <vector>

#include <opencv2/core/core.hpp>
using namespace std;
using namespace cv;
/**
 * Calcola la distanza di un punto dalla linea passante per due punti.
 * @param begin Primo punto della linea.
 * @param end Secondo punto della linea.
 * @param x Punto di cui calcolare la distanza.
 * @return Distanza tra il punto e la linea.
 */
inline double distance_to_Line(const cv::Point2f &begin, const cv::Point2f &end, const cv::Point2f &x) { // distanza di x dalla linea definita dai punti begin ed end
	const auto v = end - begin;
	return (x - begin).cross(v) / norm(v);
}

template<typename T> double sqrDistance(const cv::Point_<T> &a, const cv::Point_<T> &b) noexcept
{
	cv::Point2d v = b - a;
	return v.dot(v);
}

template<typename T> double sqrDistance(const cv::Point3_<T> &a, const cv::Point3_<T> &b) noexcept
{
	cv::Point3d v = b - a;
	return v.dot(v);
}

template<typename T> double distance(const T &a, const T &b) noexcept
{
	return sqrt(sqrDistance(a, b));
}

template<typename Container, typename T, typename EndT> double distance(const Container &container, const T &begin, const EndT &end)
{
	auto it = begin;
	double result = 0;
	while (true)
	{
		auto prev = it;
		++it;
		result += distance(container[*prev], container[*it]);
		if (it == end) return result;
	   // result += distance(container[*prev], container[*it]);
	}
}

template<typename Container, typename IndexT, int N> double distance(const Container &container, IndexT (&indices)[N])
{
	return distance(container, indices + 0, indices + N);
}



template<typename T, typename U>
std::vector<T> vectorAsBigAs(const vector<U> &u)
{
	vector<T> result;
	result.resize(u.size());
	return result;
}

template<typename T, typename U>
vector<T> convert(const vector<U> &u)
{
	vector<T> result;
	result.reserve(u.size());
	std::copy(u.begin(), u.end(), std::back_inserter(result));
	return result;
}



template<class ForwardIt, class Value, class Fn>
ForwardIt min_element_fn_threshold(const ForwardIt first, const ForwardIt last, const Value threshold, Fn fn)
{
	ForwardIt smallest = last;
	Value smallestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (currentValue > threshold && (smallest == last || currentValue < smallestValue)) {
			smallest = current;
			smallestValue = currentValue;
		}
	}
	return smallest;
}

template<class ForwardIt, class Value, class Fn>
ForwardIt max_element_fn_threshold(const ForwardIt first, const ForwardIt last, const Value threshold, Fn fn)
{
	ForwardIt biggest = first;
	Value biggestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (currentValue > threshold && (biggest == first || currentValue < biggestValue)) {
			biggest = current;
			biggestValue = currentValue;
		}
	}
	return biggest;
}




template<class ForwardIt, class Fn>
ForwardIt max_element_fn(const ForwardIt first, const ForwardIt last, Fn fn)
{
	ForwardIt largest = last;
	std::decay_t<decltype(fn(*first))> largestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (largest == last || largestValue < currentValue) {
			largest = current;
			largestValue = currentValue;
		}
	}
	return largest;
}

template<class ForwardIt, class Fn>
ForwardIt min_element_fn(const ForwardIt first, const ForwardIt last, Fn fn)
{
	ForwardIt smallest = last;
	std::decay_t<decltype(fn(*first))> smallestValue{};
	for (ForwardIt current = first; current != last; ++current) {
		auto currentValue = fn(*current);
		if (smallest == last || currentValue < smallestValue) {
			smallest = current;
			smallestValue = currentValue;
		}
	}
	return smallest;
}


cv::Point2f operator*(cv::Mat M, const cv::Point2f& p);


cv::Point3f midpoint(const std::vector<cv::Point3f>& vertices);
cv::Point2f midpoint(const std::vector<cv::Point2f>& vertices);
cv::Point2f midpoint(cv::Point2f p1, cv::Point2f p2);
cv::Point2d midpoint(cv::Point2d p1, cv::Point2d p2);
// Explicit form y = m*x + q
struct Line_s {
	double m;
	double q;
	int type;
};

class zernike
{
public:
	zernike();
	double calczernike(cv::Mat image, int n, int m);
};

Line_s fit_line(const std::vector<cv::Point>& pp);
Line_s fit_line_w_ransac(std::vector<cv::Point>& pp, int iterations, double maxError, int inliers_min_percent, bool first_half = false);
Line_s fit_line_w_ransac_2(std::vector<cv::Point>& pp, int iterations, double maxError);
Line_s fit_line_w_ransac_3(std::vector<cv::Point>& pp, int iterations, double maxError, int even_odd);
std::vector<cv::Point> assign_to_line(Line_s &line, double maxErr, std::vector<std::vector<cv::Point>*> &points);
std::vector<cv::Point> assign_to_line_2(Line_s &line, double maxErr, std::vector<cv::Point> points);
cv::Point2d compute_corner(Line_s& line1, Line_s& line2);
bool angle_between_lines_is_bad(const Line_s& l1, const Line_s& l2);;
std::vector<double> convolve(const std::vector<double>& a, const std::vector<double>& b);

int distance(std::vector<cv::Point>const& image, std::vector<cv::Point>const& tempImage);

double distance_hausdorff(const std::vector<cv::Point> & a, const std::vector<cv::Point> & b);

double cross(Point v1, Point v2);
bool getIntersectionPoint(Point a1, Point a2, Point b1, Point b2, Point & intPnt);
int indexofSmallestElement(double array[], int size);
cv::Rect find_bb_over_blobs(Mat list, int blob1, int blob2);

char* getCmdOption(char ** begin, char ** end, const string & option);
bool cmdOptionExists(char** begin, char** end, const string& option);
#endif
