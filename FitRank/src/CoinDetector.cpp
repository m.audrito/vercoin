#include "CoinDetector.h"
#include "imgprocfunctions.h"
#include <vector>
#include <opencv2/core/core.hpp>


CoinDetector::CoinDetector()
{
	x = 0;
	y = 0;

}

CoinDetector::Refine(Mat img, Vec3f  circle)
{

}


void CoinDetector::Scan(Mat img)
{
	Mat gray,hsv,img_LAB;
	cvtColor(img, gray, COLOR_BGR2GRAY);   // VEDER RBG O BGR
	cvtColor(img, hsv, COLOR_BGR2HSV);
	cvtColor(img, img_LAB, COLOR_BGR2Lab);
	//	with open('euro_coin_detector_classifier.json') as infile :
	//Coin.classifier = json.load(infile)['classification']
	//autoLevels(img_LAB); //34ms in esecuzione
	////medianBlur(img_LAB, img_LAB, 9); //144ms in esecuzione
	//std::vector<cv::Mat> lab_planes(3);
	//split(img_LAB, lab_planes);  // now we have the L image in lab_planes[0]

	//// apply the CLAHE algorithm to the L channel
	//Ptr<CLAHE> clahe = createCLAHE();
	//clahe->setClipLimit(4);
	//Mat dst;
	//clahe->apply(lab_planes[0], dst);

	//// Merge the the color planes back into an Lab image
	//dst.copyTo(lab_planes[0]);
	//merge(lab_planes, img_LAB);

	//// convert back to BGR
	//Mat image_clahe;
	//cvtColor(img_LAB, image_clahe, COLOR_Lab2BGR);
	

	
	// Adaptive Thresholding
	Mat	gray_blur,thresh;
	//cvtColor(image_clahe, gray, COLOR_BGR2GRAY);
	GaussianBlur(gray, gray_blur, Size(15, 15), 0);
	adaptiveThreshold(gray_blur, thresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 11, 2);
	//GaussianBlur(lab_planes[0], gray_blur, Size(15, 15), 0);
	//adaptiveThreshold(gray_blur, thresh, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 11, 2);


	//Circle detection
	vector<Vec3f> circles;
	double minDist = 200;
	double param1 = 20;
	double param2 = 40;
	int minRadius = 100;
	int maxRadius = 150;
	HoughCircles(gray_blur, circles, HOUGH_GRADIENT, 1, minDist, param1, param2, minRadius, maxRadius);
	for ( auto cc : circles)
	{
		Point2i center ( cc[0],cc[1] );
		int diam = static_cast<int>(cc[2]);
		circle(gray_blur, center, diam, Scalar(0, 255, 0), 3);
	}
	//	coins = None
	//	if circles is not None:
	//// Initialize coins using those detected circles
	//	coins = map(lambda c : Coin(c[0], c[1], c[2]), circles[0])


}



CoinDetector::~CoinDetector()
{
}
