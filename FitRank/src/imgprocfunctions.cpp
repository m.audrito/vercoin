
#include "imgprocfunctions.h"
#include "utility.h"
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

static void build_histogram(const Mat &mat, int (&hists)[4][256]) {
	memset(hists, 0, sizeof(hists));

	const int channels = mat.channels();
	assert(channels <= 4);
	// calcolo istogramma su 3 canali
	for (int y = 0; y < mat.rows; ++y) {
		const uchar* ptr = mat.ptr<uchar>(y);
		for (int x = 0; x < mat.cols; ++x) {
			for (int i = 0; i < channels; ++i) {
				hists[i][*ptr++]++;
			}
		}
	}
}

static void scaleAndOffset(Mat &mat, float(&offset)[4], float(&scale)[4]) {
	assert(mat.depth() == CV_8U);
	const int channels = mat.channels();
	for (int y = 0; y < mat.rows; ++y) {
		uchar* ptr = mat.ptr<uchar>(y);
		for (int x = 0; x < mat.cols; ++x) {
			for (int i = 0; i < channels; ++i) {
				*ptr = saturate_cast<uchar>(int((*ptr + offset[i]) * scale[i])); // PAN: Force int-conversion to use truncation rather than rounding (for compatibility with original code - investigate rounding better)
				++ptr;
			}
		}
	}

}

void autoLevels(Mat &mat) { // Esecuzione in 34ms. Accettabile, eseguito solo all'inizio.
	constexpr double discard_ratio = 0.05;
	int hists[4][256];
	build_histogram(mat, hists);

	// calculate scale and offset directly on the histograms (by accumulating channel by channel)
	const int total = mat.cols*mat.rows;
	const int discard_low = int(discard_ratio * total);
	const int discard_high = total - discard_low;
	float offset[4], scale[4];
	for (int i = 0; i < mat.channels(); ++i) {
		int currMin = 0;
		int currMax = 255;
		int acc = hists[i][0];
		for (int j = 1; j < 255; ++j) {
			acc += hists[i][j];
			if (acc < discard_low) {
				currMin = j + 1;
			} else if (acc > discard_high) {
				currMax = j;
				break;
			}
		}
		offset[i] = float(-currMin);
		scale[i] = 255.f / (currMax - currMin);
	}

	scaleAndOffset(mat, offset, scale);
}

void sigmoide_contrasto(const Mat& src, Mat& dst, float E, float m) {  // Ottimizzato, esecuzione in 4ms
	Mat lut(1, 256, CV_8U);
	for (int i = 0; i < 256; i++)
		lut.at<uchar>(i) = saturate_cast<uchar>(255.0f / (1.0f + pow(m / i, E)));
	LUT(src, lut, dst);
}

static void computeMinMaxValuesAuto(const Mat& im, int* min, int* max)
{
	// wiki.cmci.info/documents/120206pyip_cooking/python_imagej_cookbook#automatic_brightnesscontrast_button
	std::vector<unsigned long long> hist(256, 0);

	for (int y = 0; y < im.rows; ++y)
	{
		uchar* ptr = im.data + y * im.step;
		for (int x = 0; x < im.cols; ++x)
		{
			++hist[ptr[x]];
		}
	}

	const unsigned long long limit = im.cols * im.rows / 10;
	const unsigned long long threshold = im.cols * im.rows / 5000;

	int i = -1;
	while (true)
	{
		++i;
		auto count = hist[i];
		if (count > limit)
		{
			count = 0;
		}
		if ((count > threshold) || i >= 255)
		{
			break;
		}
	}
	*min = i;
	i = 256;
	while (true)
	{
		--i;
		auto count = hist[i];
		if (count > limit)
		{
			count = 0;
		}
		if ((count > threshold) || i < 1)
		{
			break;
		}
	}
	*max = i;
}

static void do_enhance(Mat& im, int min, int max)
{
	for (int y = 0; y < im.rows; ++y)
	{
		uchar* ptr = im.data + y * im.step;
		for (int x = 0; x < im.cols; ++x)
		{
			ptr[x] = saturate_cast<uchar>(((ptr[x] - min) * 255) / (max - min));
		}
	}
}

static void contrast_enhance(Mat& im)
{
	int min, max;
	computeMinMaxValuesAuto(im, &min, &max);
	do_enhance(im, min, max);
}

Mat color_balance(const Mat& im)
{
	Mat b;
	vector<Mat> ch;
	split(im, ch);

	for (auto& c : ch) contrast_enhance(c);
	merge(ch, b);

	return b;
}

static bool bimodal_test(const std::vector<double>& y)
{
	int modes = 0;
	for (size_t k = 1; k < y.size() - 1; ++k)
	{
		if (y[k - 1] < y[k] && y[k + 1] < y[k])
		{
			++modes;
			if (modes > 2) return false;
		}
	}
	return (modes == 2);
}

static int intermodes(const Mat& im, const Mat* mask, bool cut_lower_histo)
{
	std::vector<double> histo(256, 0.0);
	if (mask)
	{
		for (int y = 0; y < im.rows; ++y)
		{
			uchar* ptr = im.data + y * im.step;
			uchar* mptr = mask->data + y * mask->step;
			for (int x = 0; x < im.cols; ++x)
			{
				if (mptr[x])
				{
					const uchar v = ptr[x];
					++histo[v];
				}
			}
		}
	}
	else
	{
		for (int y = 0; y < im.rows; ++y)
		{
			uchar* ptr = im.data + y * im.step;
			for (int x = 0; x < im.cols; ++x)
			{
				const uchar v = ptr[x];
				++histo[v];
			}
		}
	}
	if (cut_lower_histo)
	{
		for (int i = 0; i < 80; ++i)
		{
			histo[i] = 0;
		}
	}

	int iter = 0;
	while (!bimodal_test(histo))
	{
		auto current = 0.0;
		auto next = histo[0];

		for (size_t i = 0; i < histo.size() - 1; ++i)
		{
			const auto previous = current;
			current = next;
			next = histo[i + 1];
			histo[i] = (previous + current + next) / 3.0;
		}
		histo[histo.size() - 1] = (current + next) / 3.0;
		++iter;
		if (iter > 5000)
		{
			return -1;
		}
	}

	int tt = 0;
	for (int i = 1; i < histo.size() - 1; ++i)
	{
		if (histo[i - 1] < histo[i] && histo[i + 1] < histo[i])
		{
			tt += i;
		}
	}
	return tt / 2;
}

void do_threshold(const Mat& im, Mat& bw)
{
	if (bw.cols != im.cols || bw.rows != im.rows) bw = Mat(im.size(), CV_8U);

	std::vector<Mat> channels;
	split(im, channels);
	const size_t n = channels.size();
	std::vector<int> th_v(n);

	for (size_t i = 0; i < n; ++i)
	{
		th_v[i] = intermodes(channels[i], nullptr, i == 0); // cut lower histo on blue channel
	}

	for (int y = 0; y < im.rows; ++y)
	{
		const auto ptr = im.data + y * im.step;
		const auto bwptr = bw.data + y * bw.step;
		for (int x = 0; x < im.cols; ++x)
		{
			const auto v = ptr + n * x;
			uchar mv = 255;
			for (size_t i = 0; i < n; ++i)
			{
				if (v[i] < th_v[i])
				{
					mv = 0;
				}
			}
			bwptr[x] = mv;
		}
	}
}

Mat do_threshold(const Mat& im)
{
	Mat bw(im.rows, im.cols, CV_8U);
	do_threshold(im, bw);
	return bw;
}

// Segmentation using K-clustering based on -LAB image and speeding up the procesisng resizing the image to 200 width image 
Mat kClusterLABimage(Mat image)
{
	Mat temp;

	const int MAX_CLUSTERS = 3;  // number of cluster because in the image there is a white sheet , foot with dark soks and floor
	//autoLevels(image); //34ms in esecuzione
	//medianBlur(image, image, 9);
	Mat img_LAB;
	cvtColor(image, img_LAB, COLOR_BGR2Lab);
	autoLevels(img_LAB); //34ms in esecuzione
	medianBlur(img_LAB, img_LAB, 9); //144ms in esecuzione
	int target_width = 200;
	int target_height = int(round(target_width * image.rows / image.cols));
	Mat img_rez(target_height, target_width, CV_8UC3);
	resize(img_LAB, img_rez, img_rez.size(), 0, 0, CV_INTER_AREA);

	/*************************/
	//  K-clustering processing and transform mat to permit this analisys
	Mat reshaped_image = img_rez.reshape(1, img_rez.cols * img_rez.rows);
	Mat reshaped_image32f;
	reshaped_image.convertTo(reshaped_image32f, CV_32FC1, 1.0 / 255.0);
	Mat labels;
	TermCriteria criteria{ TermCriteria::COUNT, 100, 1 };
	Mat centers;
	//double compactness = 
	kmeans(reshaped_image32f, MAX_CLUSTERS, labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, centers);
	//double compactness = kmeans(reshaped_image32f, MAX_CLUSTERS, labels, criteria, 1, KMEANS_PP_CENTERS, centers);

	/*************************/
	// LAB_image resized version of image in LAB and there will be the segmentation
	Mat LAB_image(img_rez.rows, img_rez.cols, CV_8UC3);
	MatIterator_<Vec3b> LAB_first = LAB_image.begin<Vec3b>();
	MatIterator_<Vec3b> LAB_last = LAB_image.end<Vec3b>();
	MatConstIterator_<int> label_first = labels.begin<int>();

	Mat centers_u8;

	centers.convertTo(centers_u8, CV_8UC1, 255.0);
	Mat centers_u8c3 = centers_u8.reshape(3);

	while (LAB_first != LAB_last) {
		const Vec3b& lab = centers_u8c3.ptr<Vec3b>(*label_first)[0];
		*LAB_first = lab;
		++LAB_first;
		++label_first;
	}
	/*********************************/
	// with centroid and label found, we will  map the original LAB image considering the distance of 
	//pixel value with the clusters calculated
	int channels = img_LAB.channels();
	int nRows = img_LAB.rows;
	int nCols = img_LAB.cols * channels;
	if (img_LAB.isContinuous())
	{
		nCols *= nRows;
		nRows = 1;
	}
	int i, j;
	Vec3d d1, d2, d3;
	Vec3b* p;
	Vec3b* pt_centroids = centers_u8c3.ptr<Vec3b>(0);
	for (i = 0; i < img_LAB.rows; ++i)
	{
		p = img_LAB.ptr<Vec3b>(i);
		for (j = 0; j < img_LAB.cols; ++j)
		{
			//	double dist[] = { norm(p[j] , pt_centroids[0]),norm(p[j] , pt_centroids[1]),norm(p[j] , pt_centroids[2]) };
			//	int index = indexofSmallestElement(dist, 3);
			//	int index = rand() % 3;
			double d_1 = sqrt((p[j](0) - pt_centroids[0](0))*(p[j](0) - pt_centroids[0](0))
				+ (p[j](1) - pt_centroids[0](1))*(p[j](1) - pt_centroids[0](1))
				+ (p[j](2) - pt_centroids[0](0))*(p[j](2) - pt_centroids[0](2)));
			double d_2 = sqrt((p[j](0) - pt_centroids[1](0))*(p[j](0) - pt_centroids[1](0))
				+ (p[j](1) - pt_centroids[1](1))*(p[j](1) - pt_centroids[1](1))
				+ (p[j](2) - pt_centroids[1](0))*(p[j](2) - pt_centroids[1](2)));
			double d_3 = sqrt((p[j](0) - pt_centroids[2](0))*(p[j](0) - pt_centroids[2](0))
				+ (p[j](1) - pt_centroids[2](1))*(p[j](1) - pt_centroids[2](1))
				+ (p[j](2) - pt_centroids[2](0))*(p[j](2) - pt_centroids[2](2)));
			double dist[] = { d_1,d_2,d_3 };
			int index = indexofSmallestElement(dist, 3);
			p[j] = pt_centroids[index];
		}
	}

	Mat res;

	cvtColor(img_LAB, res, COLOR_Lab2BGR);
	
	return res;
}


void clahe_LChannel(Mat &image)
{
	Mat img_LAB;
	cvtColor(image, img_LAB, COLOR_BGR2Lab);
	autoLevels(img_LAB); //34ms in esecuzione
	medianBlur(img_LAB, img_LAB, 9); //144ms in esecuzione
	std::vector<cv::Mat> lab_planes(3);
	split(img_LAB, lab_planes);  // now we have the L image in lab_planes[0]

	// apply the CLAHE algorithm to the L channel
	Ptr<CLAHE> clahe = createCLAHE();
	clahe->setClipLimit(4);
	Mat dst;
	clahe->apply(lab_planes[0], dst);

	// Merge the the color planes back into an Lab image
	dst.copyTo(lab_planes[0]);
	merge(lab_planes, img_LAB);

	// convert back to BGR
	Mat image_clahe;
	cvtColor(img_LAB, image_clahe, CV_Lab2BGR);
	image_clahe.copyTo(image);

}


void BlobDetector(Mat img)
{
	Mat im;
	cvtColor(img, im, COLOR_BGR2GRAY);
		// Setup SimpleBlobDetector parameters.
	SimpleBlobDetector::Params params;

	// Change thresholds
	params.minThreshold = 10;
	params.maxThreshold = 220;

	// Filter by Area.
	params.filterByArea = true;
	params.minArea = 350;

	// Filter by Circularity
	params.filterByCircularity = true;
	params.minCircularity = 0.87;

	// Filter by Convexity
	params.filterByConvexity = false;
	params.minConvexity = 0.1;

	// Filter by Inertia
	params.filterByInertia = true;
	params.minInertiaRatio = 0.01;

#if CV_MAJOR_VERSION < 3   // If you are using OpenCV 2
	// Set up detector with params
	SimpleBlobDetector detector(params);

	// You can use the detector this way
	// detector.detect( im, keypoints);
#else

	// Set up detector with params
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
	// Detect blobs.
	vector<KeyPoint> keypoints;
	detector->detect(im, keypoints);

	// Draw detected blobs as red circles.
	// DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
	Mat im_with_keypoints;
	drawKeypoints(im, keypoints, im_with_keypoints, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);


#endif
}