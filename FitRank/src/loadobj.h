#pragma once

#ifndef __LOADOBJ_H__
#define __LOADOBJ_H__

#include <stdexcept>
#include "mesh.h"

class file_not_found_error : public std::runtime_error
{
public:
    file_not_found_error(const char *value) : std::runtime_error(value) {}
};

/**
* Carica in memoria il modello 3D su cui basare la stima. Utilizza una struttura Mesh dotata di un vettore di vertici e un
* vettore di indici per la costruzione dei triangoli del poligono.
* @param filename Nome del file OBJ da leggere.
* @returns Variabile Mesh in cui salvare il modello letto.
*/
Mesh load_obj(const char* filename);
void save_obj(const Mesh &mesh, const char *nomefile, float scala);

#endif
