################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
    ../src/balance.cpp \
    ../src/debugging.cpp \
	../src/loadobj.cpp \
    ../src/main.cpp \
    ../src/mb_main_n.cpp \
	../src/mesh.cpp \
    ../src/utility.cpp

OBJS += \
    ../src/balance.o \
    ../src/debugging.o \
	../src/loadobj.o \
    ../src/main.o \
    ../src/mb_main_n.o \
	../src/mesh.o \
    ../src/utility.o

CPP_DEPS += \
    ../src/balance.d \
    ../src/debugging.d \
	../src/loadobj.d \
    ../src/main.d \
    ../src/mb_main_n.d \
	../src/mesh.d \
    ../src/utility.d

# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++14 -I/usr/local/include -I/usr/local/include/opencv -O3 -march=native -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


