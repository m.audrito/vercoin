
#include <iostream>
#include <bitset>
#include <vector>
#include <string>
#include "constants.h"
#include "config.h"

#include "ClassProcessImg.h"
#include "debugging.h"
#include "utility.h"
#include <opencv2/core/utility.hpp>


using namespace std;


class InputParser {
public:
	InputParser(int &argc, char **argv) {
		for (int i = 1; i < argc; ++i)
			this->tokens.push_back( string(argv[i]));
	}
	//
	const  string& getCmdOption(const  string &option) const {
		 vector< string>::const_iterator itr;
		itr =  find(this->tokens.begin(), this->tokens.end(), option);
		if (itr != this->tokens.end() && ++itr != this->tokens.end()) {
			return *itr;
		}
		static const  string empty_string("");
		return empty_string;
	}

	const  string& getFileNameImg() const { return *(this->tokens.begin());
		/*static const  string empty_string(" ");
		string fn = *(this->tokens.begin());
		if (fn.compare( empty_string)==0)
			return empty_string;
		else
			return fn;*/
		
	
	}
	//
	bool cmdOptionExists(const  string &option) const {
		return  find(this->tokens.begin(), this->tokens.end(), option)
			!= this->tokens.end();
	}
private:
	 vector < string> tokens;
};

Error_type parsingInputParameters(int argc, char **argv, AlgoParams &params)
{
	InputParser input(argc, argv);
	params.fname = input.getFileNameImg();
	if (input.cmdOptionExists( "-sx")) 
			params.foot_sx = true;
	else
			params.foot_sx = false;
	

	if (input.cmdOptionExists( "-f")) 
			params.coin_type=input.getCmdOption( "-f");

		else
			return ERROR_COIN_FORMAT_IN_ARGV;
		


	if (cmdOptionExists(argv, argv + argc, "-save")) {
		params.debug = 1;
		params.debug_save_path = input.getCmdOption( "-save");
	}
	else {
		params.debug = 0;
		params.debug_save_path = "";
	}

	return NO_ERROR;

	//cv::String keys =
	//	"{@image1 |  | image1 to process  }"			// mandatory, image to process 
	//	"{sx      |  | foot side}"						// left foot , default value dx
	//	"{f       |  | type of coin}"					// format coin  
	//	"{help    |  | show help message}"				// optional, show help optional
	//	"{save	  |  | path to save debug images}";		// optional
	//
	//
	//cv::CommandLineParser parser(argc, argv, keys);
	//parser.about("Application FitRank Coin  v1.0.0");
	//if (parser.has("help")) {
	//	parser.printMessage();
	//	return GENERIC_ERROR;
	//}
	//if (!parser.check()) {
	//	parser.printErrors();

	//	return(GENERIC_ERROR);
	//}
	////string input_image_path = parser.get<string>(0); // read @image (mandatory, error if not present)
	//params.SetFname(parser.get<string>(0)); // read @image (mandatory, error if not present)

	//if (parser.has("sx"))
	//	params.foot_sx = true;
	//else
	//	params.foot_sx = false;

	//string temp= parser.get<string>("f");
	//bool res= find(Coins_type.begin(), Coins_type.end(), temp)!=Coins_type.end();
	//if (res)

	//	params.SetCoinType( temp);
	//else
	//	return ERROR_COIN_FORMAT_IN_ARGV;
	//
	////
	//bool t = parser.has("save");
	//if (t)
	//{
	//	String str = parser.get<String>("save");
	////	params.SetDebugPath();
	//	params.debug = 1;
	//}
	//else
	//{
	//	params.SetDebugPath(" ");
	//	params.debug = 0;
	//}

	
	/*try
	{
		params.coin_type = parser.get<Coins_type>("f");
	}
	catch (const std::exception& e){
		return (ERROR_COIN_FORMAT_IN_ARGV);
	}*/
		

	
}
//--------------------------------------------------------------------------------------------------
int main(int argc, char **argv) {


	
	AlgoParams params{};
	
	Error_type return_error;
		
	/*--------------------------------------------------------------*/
	//Parsing parameters for input
	
	return_error = parsingInputParameters(argc, argv, params);
	set_imsave_path(params.debug_save_path);
	ClassProcessImage Image;
	if (return_error ==  NO_ERROR)
	{
		/*--------------------------------------------------------------*/
		// INIZIO ELABORAZIONE
		return_error = Image.Process(&params);
		if (return_error != NO_ERROR) { 
			cout << "{ \"ok\": false,  \"flag\": \"" << Error_type_v[return_error] << "\" }" << endl;
		}
		else {
			cout << "{ \"ok\": true,  \"flag\": \"" << (Quality_IMG_v[Image.qualitaimg])
				<< "\", \"heel_toe\": \"" << Image.Dim.heel_toe
				<< "\", \"neck_height\": \"" << Image.Dim.neck_height
				<< "\", \"width\": \"" << Image.Dim.width
				<< "\", \"instep\": \"" << Image.Dim.instep
				<< "\", \"metatarsal_girth\": \"" << Image.Dim.metatarsal_girth
				<< "\" }" << endl;
		}

	}

}

// DImensioni:
//"heel_toe				<< final_dimensions[0]
//neck_height		    << final_dimensions[1]
//"width				<< final_dimensions[2] 
//instep				<< final_dimensions[3] 
//"metatarsal_girth     << final_dimensions[4]  
//"Lenght_on_mask       << final_dimensions[5] 


//qualitaimg: numero tra 0 (qualit� alta) e 209 (qualit� pessima). Aumenta con blur, ombre e scarsa luminosità. -1 in caso di errore.
//flagimg: usa 7 bit per impostare 7 flag sull'immagine
//    MSB    7    6    5    4    3    2    1    0    LSB
//    bit0: se 1 Image size piu piccola di 1200x900
//    bit1: se 1 blur alto
//    bit2: se 1 contorno non trovato dovuto a tante ombre
//    bit3: se 1 il foglio non é abbastanza planare 
//    bit4: se 1 calcolo larghezza non avvenuto solo lunghezza
//    bit5: 
//    bit6:
