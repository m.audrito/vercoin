#pragma once

#include <opencv2/opencv.hpp>
#include "config.h"



using namespace std;
using namespace cv;

struct Mask_feet {
	Mat mask_bw;
	Rect bb;
};

struct Coin
{
	Mat mask_coin;
	Rect bb_coin;
};

struct Dimensions
{
	float heel_toe;
	float neck_height;
	float width;
	float instep;
	float metatarsal_girth;
	
};

class ClassProcessImage
{
private:
	
	Mat imageOriginal;
	Mat gray;
	
			
	vector<Coin> coins;
	Mask_feet  mask;
	Mat img_segmented;
	
public:
	Dimensions Dim;
	int qualitaimg;
	
	ClassProcessImage();
	
	/* Get Members*/

	Mat GetOriginalImage();
	Mat GetOriginalGrayImage();
	Mat GetMask();
	Rect GetROI();
	Mat GetImageSegmented();

	/* Set Members*/
	Error_type Process(AlgoParams *);

	short int LoadResizeImage(string fname);
	void SetOriginalImage(Mat im);
	void SetImageSegmented(Mat im);
	
	short int ExtractCoins(const short int);
	~ClassProcessImage();
};

