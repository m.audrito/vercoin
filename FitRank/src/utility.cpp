
#include "utility.h"
#include <opencv2/core/core.hpp>
#include <random>

using namespace std;
using namespace cv;

#define RANDOM_SHUFFLE      my_random_shuffle
//--------------------------------------------------------------------------------------------------
template< class RandomIt >
void my_random_shuffle(RandomIt first, RandomIt last)
{
	typename std::iterator_traits<RandomIt>::difference_type i, n;
	n = last - first;
	for (i = n - 1; i > 0; --i) {
		using std::swap;
		swap(first[i], first[std::rand() % (i + 1)]);
	}
}

Point3f midpoint(const std::vector<Point3f>& vertices)
{
	Point3f medio = {0.0f, 0.0f, 0.0f};
	for (auto& vertice : vertices)
	{
		medio += vertice;
	}
	medio.x /= float(vertices.size());
	medio.y /= float(vertices.size());
	medio.z /= float(vertices.size());
	return medio;
}

Point2f midpoint(const std::vector<Point2f>& vertices)
{
	Point2f medio = { 0.0f, 0.0f };
	for (auto& vertice : vertices)
	{
		medio += vertice;
	}
	medio.x /= float(vertices.size());
	medio.y /= float(vertices.size());
	return medio;
}

Point2f midpoint(Point2f p1, Point2f p2)
{
	Point2f medio = { (p1.x + p2.x) / 2,(p1.y + p2.y) / 2 };
	return medio;
}
Point2d midpoint(Point2d p1, Point2d p2)
{
	Point2d medio = { (p1.x + p2.x) / 2,(p1.y + p2.y) / 2 };
	return medio;
}
Line_s fit_line(const std::vector<Point>& pp)
{
	double sum_x = 0.0;
	double sum_y = 0.0;
	double sum_xy = 0.0;
	double sum_x2 = 0.0;


	for (auto& p : pp)
	{
		sum_x += p.x;
		sum_y += p.y;
		sum_xy += p.x * p.y;
		sum_x2 += p.x * p.x;
	}

	const double mean_x = sum_x / pp.size();
	const double mean_y = sum_y / pp.size();
	const double varx = sum_x2 - sum_x * mean_x;
	const double cov = sum_xy - sum_x * mean_y;

	if (abs(varx) > 100.0 * std::numeric_limits<double>::epsilon())
	{
		const double m = cov / varx;
		const double q = mean_y - m * mean_x;
		return {m, q, 1};
	}
	else
	{
		const double m = 0.0;
		const double q = mean_x;
		return {m, q, 2};
	}
}

static double distance_point_to_line(Point p, Point2d p0, Point2d r)
{
	const Point2d s(p.x - p0.x, p.y - p0.y);
	return abs(s.x * r.y - s.y * r.x);
}

Line_s fit_line_w_ransac(std::vector<Point>& pp, int iterations, double maxError, int inliers_min_percent, bool first_half)
{
	double min_error = std::numeric_limits<double>::max();
	Line_s best_line = {0, 0, 0};

	if (pp.size() < 2) return best_line;

	std::random_device rd;
	std::mt19937 g(rd());

	for (int i = 0; i < iterations; ++i)
	{   // versione SHUFFLE di  Massimo Bortolato che permette di usare la versione di RELEASE
		if (first_half) RANDOM_SHUFFLE(&pp[0], &pp[pp.size() / 2]);
		else RANDOM_SHUFFLE(pp.begin(), pp.end());
	   /* 
	   if (first_half)
		{
			shuffle(pp.begin(), pp.begin() + pp.size() / 2, g);
		}
		else
		{
			shuffle(pp.begin(), pp.end(), g);
		}*/
		std::vector<Point> inliers(pp.begin(), pp.begin() + 2);
		std::vector<Point> outliers(pp.begin() + 2, pp.end());
		auto line = fit_line(inliers);

		if (line.type == 0) continue;
		auto& m = line.m;
		auto& q = line.q;

		auto f = sqrt(1.0 + m * m);
		Point2d p0;
		Point2d r;
		if (line.type == 1)
		{
			r = Point2d(1.0 / f, m / f);
			p0 = Point2d(0.0, q);
		}
		else
		{
			r = Point2d(m / f, 1.0 / f);
			p0 = Point2d(q, 0.0);
		}
		for (auto& p : outliers)
			if (distance_point_to_line(p, p0, r) < maxError)
				inliers.push_back(p);

		const size_t min_inliers = pp.size() * inliers_min_percent / 100;
		if (inliers.size() < min_inliers) continue;

		line = fit_line(inliers);
		if (line.type == 0) continue;
		m = line.m;
		q = line.q;
		f = sqrt(1 + m * m);
		if (line.type == 1)
		{
			r = Point2d(1.0 / f, m / f);
			p0 = Point2d(0.0, q);
		}
		else
		{
			r = Point2d(m / f, 1.0 / f);
			p0 = Point2d(q, 0.0);
		}
		double error = 0.0;
		for (auto& p : inliers)
			error += distance_point_to_line(p, p0, r);

		if (error < min_error)
		{
			min_error = error;
			best_line = line;
		}
	}

	return best_line;
}

Point2d compute_corner(Line_s& line1, Line_s& line2)
{
	auto& t1 = line1.type;
	auto& t2 = line2.type;

	auto& m1 = line1.m;
	auto& q1 = line1.q;
	auto& m2 = line2.m;
	auto& q2 = line2.q;

	if (t1 == 1 && t2 == 1)
	{
		const double n = q2 - q1;
		const double d = m1 - m2;
		if (abs(d) > std::numeric_limits<double>::epsilon())
		{
			const double x = n / d;
			const double y = m1 * x + q1;
			return Point2d(x, y);
		}
	}
	else if (t1 == 1 && t2 == 2)
	{
		const double x = q2;
		const double y = m1 * x + q1;
		return Point2d(x, y);
	}
	else if (t1 == 2 && t2 == 1)
	{
		const double x = q1;
		const double y = m2 * x + q2;
		return Point2d(x, y);
	}
	return Point2d(0, 0);
}

bool angle_between_lines_is_bad(const Line_s& l1, const Line_s& l2)
{
	if (l1.type == 1 && l2.type == 1)
	{
		const double m1 = l1.m;
		const double m2 = l2.m;
		return abs((m2 - m1) / (1 + m1 * m2)) < 1.0;
	}
	if (l1.type == 2 && l2.type == 1)
	{
		return l2.m > 1.0;
	}
	if (l1.type == 1 && l2.type == 2)
	{
		return l1.m > 1.0;
	}
	return true;
}

std::vector<double> convolve(const std::vector<double>& a, const std::vector<double>& b)
{
	std::vector<double> r(a.size());
	const int h = static_cast<int>(b.size()) / 2;

	for (int i = 0; i < a.size(); ++i)
	{
		for (int k = -h; k <= h; ++k)
		{
			if ((i + k) >= a.size() || (i + k) < 0) continue;
			r[i] += a[i + k] * b[h - k];
		}
	}

	return r;
}
//--------------------------------------------------------------------------------------------------
Line_s fit_line_w_ransac_2(vector<Point> &pp, int iterations, double maxError) {

	size_t max_num_inliers = 0;
	Line_s best_line = { 0,0,0 };

	if (pp.size() < 10) return best_line;

	for (int i = 0; i < iterations; ++i) {
		RANDOM_SHUFFLE(pp.begin(), pp.end());
		vector<Point> inliers(pp.begin(), pp.begin() + 2);
		vector<Point> outliers(pp.begin() + 2, pp.end());
		auto line = fit_line(inliers);

		if (line.type == 0) continue;
		auto &m = line.m;
		auto &q = line.q;

		auto f = sqrt(1.0 + m * m);
		Point2d p0;
		Point2d r;
		if (line.type == 1) {
			r = Point2d(1.0 / f, m / f);
			p0 = Point2d(0.0, q);
		}
		else {
			r = Point2d(m / f, 1.0 / f);
			p0 = Point2d(q, 0.0);
		}

		for (auto &p : outliers) {
			if (distance_point_to_line(p, p0, r) < maxError) {
				inliers.push_back(p);
			}
		}

		if (inliers.size() > max_num_inliers) {
			max_num_inliers = inliers.size();
			best_line = fit_line(inliers);
		}
	}

	//cout << "Linea " << max_num_inliers << " / " << pp.size() << "(" << best_line.m << ")" <<  endl;

	return best_line;
}

vector<Point> assign_to_line(Line_s &line, double maxErr, vector<vector<Point>*> &points) {

	vector<Point> assigned;

	if (line.type == 0) return assigned;

	Point2d r, p0;
	auto m = line.m;
	auto q = line.q;
	auto f = sqrt(1 + m * m);
	if (line.type == 1) {
		r = Point2d(1.0 / f, m / f);
		p0 = Point2d(0.0, q);
	}
	else {
		r = Point2d(m / f, 1.0 / f);
		p0 = Point2d(q, 0.0);
	}

	for (auto pts_ptr : points) {
		auto &pts = *pts_ptr;
		vector<Point>pts2;
		pts2.reserve(pts.size());
		for (auto &p : pts) {
			if (distance_point_to_line(p, p0, r) < maxErr) {
				assigned.push_back(p);
			}
			else {
				pts2.push_back(p);
			}
		}
		swap(pts2, pts);
	}

	line = fit_line(assigned);

	return assigned;
}

vector<Point> assign_to_line_2(Line_s &line, double maxErr, vector<Point> points) {

	vector<Point> assigned;

	if (line.type == 0) return assigned;

	Point2d r, p0;
	auto m = line.m;
	auto q = line.q;
	auto f = sqrt(1 + m * m);
	if (line.type == 1) {
		r = Point2d(1.0 / f, m / f);
		p0 = Point2d(0.0, q);
	}
	else {
		r = Point2d(m / f, 1.0 / f);
		p0 = Point2d(q, 0.0);
	}

	for (auto p : points) {
		//auto pts = pts_ptr;
		vector<Point>pts2;
		pts2.reserve(points.size());
	//	for (auto p : pts) {
			if (distance_point_to_line(p, p0, r) < maxErr) {
				assigned.push_back(p);
			}
			else {
				pts2.push_back(p);
			}
		}
	//	swap(pts2, pts);
	//}

	line = fit_line(assigned);

	return assigned;
}

cv::Point2f operator*(cv::Mat M, const cv::Point2f& p)
{
	cv::Mat_<double> src(3/*rows*/, 1 /* cols */);

	src(0, 0) = p.x;
	src(1, 0) = p.y;
	src(2, 0) = 1.0;

	cv::Mat_<double> dst = M * src; //USE MATRIX ALGEBRA 
	return cv::Point2f(dst(0, 0), dst(1, 0));
}

double factorial(double n)
{
	if (n < 0)
		return(0.0);
	if (n == 0)
		return(1.0);
	else
		return(n*factorial(n - 1.0));
}

double radialpolynomial(double p, int n, int m) {
	double radial = 0;
	double c = 0;
	for (int s = 0; s <= ((n - m) / 2); s++) {
		c = ((pow(-1.0, (double)s))*(factorial((double)n - (double)s))) / (factorial((double)s)*(factorial(((double)n + (double)m) / 2.0 - (double)s))*(factorial(((double)n - (double)m) / 2.0 - (double)s)));
		radial = radial + c * (pow(p, (double)n - 2.0*(double)s));
	}
	return radial;
}

zernike::zernike()
{
}

double zernike::calczernike(Mat image, int n, int m)
{
	double zr = 0;
	double zi = 0;
	double z = 0;
	int cnt = 0;
	double p = 0;
	double radial = 0;
	double theta = 0;
	int N = image.rows;
	for (int y = 0; y < N; y++) {
		for (int x = 0; x < N; x++) {
			p = sqrt(pow(2.0*(double)x - (double)N + 1.0, 2.0) + pow((double)N - 1.0 - 2.0*(double)y, 2.0)) / (double)N;
			/*double y1 = -1 + (2*(x+0.5))/N;
			double x1 = -1 + (2*(x+0.5))/N;
			p = (double)sqrt(x1*x1+y1*y1);*/
			if (p <= 1) {
				radial = radialpolynomial(p, n, m);
				theta = atan(((double)N - 1.0 - 2.0*(double)y) / (2.0*(double)x - (double)N + 1.0));
				uchar  c_value = image.at<uchar>(x, y);
				double value = (double)c_value;
				zr = zr + value * radial*cos((double)m*theta);
				zi = zi + value * radial*sin((double)m*theta);
				cnt = cnt + 1;
			}
		}
	}
	zr = (((double)n + 1) / (double(cnt)))*(zr);
	zi = (((double)n + 1) / (double(cnt)))*(zi);
	z = sqrt(pow(zi, 2.0) + pow(zr, 2.0));
	return z;
}


int distance(vector<Point>const& image, vector<Point>const& tempImage)
{
	int totalDistance = 0;

	for (Point imagePoint : image)
	{
		int minDistance = numeric_limits<int>::max();

		for (Point tempPoint : tempImage)
		{
			Point diff = imagePoint - tempPoint;
			int length = (diff.x * diff.x) + (diff.y * diff.y);

			if (length < minDistance) minDistance = length;
			if (length == 0) break;
		}

		if (minDistance > totalDistance) totalDistance = minDistance;
	}
	return totalDistance;
}

double distance_hausdorff(const std::vector<cv::Point> & a, const std::vector<cv::Point> & b)
{
	int maxDistAB = distance(a, b);
	int maxDistBA = distance(b, a);
	int maxDist = std::max(maxDistAB, maxDistBA);

	return std::sqrt((double)maxDist);
}


double cross(Point v1, Point v2) {
	return v1.x*v2.y - v1.y*v2.x;
}

int indexofSmallestElement(double array[], int size)
{
	int index = 0;

	for (int i = 1; i < size; i++)
	{
		if (array[i] < array[index])
			index = i;
	}

	return index;
}
bool getIntersectionPoint(Point a1, Point a2, Point b1, Point b2, Point & intPnt) {
	Point p = a1;
	Point q = b1;
	Point r(a2 - a1);
	Point s(b2 - b1);

	if (cross(r, s) == 0) { return false; }

	double t = cross(q - p, s) / cross(r, s);

	intPnt = p + t * r;
	return true;
}


/**
* Funzione di parsing linea comando, obbiettivo trovare il comando e restituisce il puntatore al argomento del comando.
* @begin:inizio linea comando
* @end :fine linea comando
* @option: opzione da trovare
* @return puntatore al relativo comando*/

char* getCmdOption(char ** begin, char ** end, const string & option)
{
	char ** itr = find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const string& option)
{
	return std::find(begin, end, option) != end;
}