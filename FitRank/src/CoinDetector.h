#pragma once
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>

using namespace cv;
using namespace std;

class CoinDetector
{
public:
	CoinDetector();
	void Scan(Mat);
	void Refine(Mat img, Vec3f  circle);
	~CoinDetector();

	// variables
	int x, y;
	Mat mask;
};



