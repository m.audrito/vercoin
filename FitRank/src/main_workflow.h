#pragma once

#include <opencv2/core/mat.hpp>
#include <opencv2/opencv.hpp>
#include <vector>
#include <string>
#include "ClassProcessImg.h"
#include "mesh.h"

// VErsione creata per permettere il passaggio di parametri alla seconda parte della soluzione, versione di biocubuca
//--------------------------------------------------------------------------------------------------
/// Struttura dei parametri dell'algoritmo
///
struct AlgoParams {
	int a4_minAreaPerCent;

	int findCorners_skipPixels;
	int findCorners_roiSize;
	int findCorners_skipPixels2;

	int ransac1_iterations;
	int ransac1_minInliersPercent;
	int ransac1_minInliersPercent_2nd;
	double ransac1_maxError;

	int ransac2_iterations;
	int ransac2_minInliersPercent;
	int ransac2_minInliersPercent_2nd;
	double ransac2_maxError;
	int num_max_blobs;
	float bb_calc_width;
	float threeshold_bounding_box; 
	int debug;
	char *debug_save_path;
	int virtual_img_WIDTH; // Virual Image
	int virtual_img_HEIGTH; // Virual Image
	float sheet_size_WIDTH; // sheet size in mm
	float sheet_size_HEIGTH;
	float calibrated_length;
	char * sheet_format;
	int crop_px;

	int sigmoid_M_max;
	int sigmoid_M_min;
	int sigmoid_E_max;
	int sigmoid_E_min;
	
	bool foot_sx;
	//-----parameter for deform model and get measure
	const char* Model_3D_file;
	std::vector<int> indices_Mesh_Height;
	std::vector<int> indices_Mesh_Ball_girth_circumference;
	std::vector<int> indices_Mesh_Instep_circumference;
	std::vector<int> indices_Mesh_Length;
	std::vector<int> indices_Mesh_Width;
	float angle_Height;
	float angle_Width;
	float angle_Length;
};


struct Mask_feet {
	cv::Mat mask;
	cv::Rect bb;
};
//--------------------------------------------------------------------------------------------------
struct Process_A4_s {
	cv::Mat im;
	cv::Mat gray;
	std::vector<cv::Point2f> vertices;
	Mask_s a4_mask;
	cv::Mat img_segmented;
	Mask_feet feet_mask;
	int qualitaimg = 0;
	bool foot_inside_sheet;
	
};



//Dimensioni immagine
//#define IMG_WIDTH 1200
//#define IMG_HEIGHT 900


//Dimensioni immagine virtuale
#define IMG_WIDTH_GL 1272
#define IMG_HEIGHT_GL 900

//Costanti limite correzione contrasto
#define MIN_M 105
#define MAX_M 135
#define MIN_E 103
#define MAX_E 110

// Veritici modello mesh
#define NUM 6

// Dimensioni FOGLI 
static const int A4_WIDTH = 297;
static const int A4_HEIGTH = 210;
static const int USA_LETTER_WIDTH = 279;
static const int USA_LETTER_HEIGTH = 216;
static const int A5_WIDTH = 210;
static const int A5_HEIGTH = 148;


//Dimensioni immagine virtuale
static const int VIRTUAL_IMG_HEIGTH = 900;

// DEFINIZIONE FUNZIONI
static int imload_and_resize(const char *fname, Mat &im, const AlgoParams *params);
static int imload_and_resize(const char *fname, ClassProcessImage &ProcessSheet, const AlgoParams *params);
int getFootDimensions( float(&dimensioni)[6], Process_A4_s &foglio_a4_1,  AlgoParams *params);
//int processImage(const char *image_above_fname, const AlgoParams *params, Process_A4_s &foglio_a4_1);
int processImage(ClassProcessImage ProcessIimage, const AlgoParams *params);
char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);
void AdjRatioVirtualImage(AlgoParams *params, const float WIDTH, const float HEIGTH, const int vr_img);