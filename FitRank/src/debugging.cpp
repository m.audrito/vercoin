#include <iostream>
#include "debugging.h"

using namespace std;
using namespace cv;

void draw_line(cv::Mat& im, Line_s& line, int len, const cv::Scalar& color)
{
	cv::Point p0, p1;

	if (line.type == 1)
	{
		p0 = cv::Point(0, int(line.q));
		p1 = cv::Point(len, int(len * line.m + line.q));
	}
	else
	{
		p0 = cv::Point(int(line.q), 0);
		p1 = cv::Point(int(len * line.m + line.q), len);
	}
	cv::line(im, p0, p1, color, 1, cv::LINE_AA);
}

void draw_line(cv::Mat& im, Line_s& line, const cv::Point2d& c, int len, const cv::Scalar& color)
{
	cv::Point p0, p1;

	const double f = sqrt(1 + line.m * line.m);

	if (line.type == 1)
	{
		const cv::Point2d r(1.0 / f, line.m / f);
		p0 = c - len * r;
		p1 = c + len * r;
	}
	else
	{
		const cv::Point2d r(line.m / f, 1.0 / f);
		p0 = c - len * r;
		p1 = c + len * r;
	}
	cv::line(im, p0, p1, color, 2, cv::LINE_AA);
}

//--------------------------------------------------------------------------------------------------
static char imsave_path[512];

void set_imsave_path(const char* path)
{
	strncpy(imsave_path, path, 512); //strncpy_s(imsave_path, path, 512);
}

void set_imsave_path(string path)
{

	strncpy(imsave_path, path.c_str(), 512); //strncpy_s(imsave_path, path, 512);
}

void imsave(const char* fname, const cv::Mat& im, const char* msg)
{
	cv::Mat im_ = im.clone();
	if (msg)
	{
		int bl;
		const auto sz = cv::getTextSize(msg, cv::FONT_HERSHEY_DUPLEX, 2.0, 1, &bl);
		rectangle(im_, cv::Rect(0, 0, sz.width + 20 + 20, bl + 50 + 20), {255, 255, 255}, -1);
		putText(im_, msg, cv::Point(20, 50), cv::FONT_HERSHEY_DUPLEX, 2.0, {0, 0, 255}, 1, cv::LINE_AA);
	}
	String fname_(fname);
	imwrite(fname_, im_);
}

void imsave(const cv::Mat& im, const char* msg)
{
	static int c = 0;

	char fname[256];
	snprintf(fname, 256, "%s%i.jpg", imsave_path, c++);

	imsave(fname, im, msg);
}

void draw_line_strip(cv::Mat im2, const std::vector<cv::Point>& sequence)
{
	if (!sequence.empty())
	{
		auto p0 = sequence[0];
		for (size_t i = 1; i < sequence.size(); ++i)
		{
			const auto& p1 = sequence[i];
			line(im2, p0, p1, {0, 0, 255}, 3, cv::LINE_AA);
			p0 = p1;
		}
	}
}

/// Funzione di visualizzazione su schermo
/**
* VIsualizza su schermo l'immagine .
* @param img immagine.
* @param str intestazione immagine

*/
void Visualizza(const cv::Mat img, std::string str)
{
	cv::namedWindow(str, cv::WindowFlags::WINDOW_NORMAL);
	cv::resizeWindow(str, 600, lround(600 * img.rows / img.cols));
	imshow(str, img);
	cv::waitKey(1);
}


void Visualizza_punti(cv::Mat img, std::vector<cv::Point3f> contour_points, const std::string s)
{


	Visualizza(img, s);

}

void Visualizza_punti(cv::Mat img, std::vector<cv::Point2f> contour_points, const std::string s)
{

	for (auto &p : contour_points)
	{
		cv::circle(img, cv::Point(lround(p.x), lround(p.y)), 12, CV_RGB(255, 0, 0), -3);
		std::cout << "contorno  " << p << std::endl;
	}
	Visualizza(img, s);

}