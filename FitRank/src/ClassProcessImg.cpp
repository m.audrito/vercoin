
#include "constants.h"
#include "config.h"
#include "ClassProcessImg.h"
#include "imgprocfunctions.h"
#include "utility.h"
#include "debugging.h"
#include "CoinDetector.h"
#include <opencv2/opencv.hpp>


ClassProcessImage::ClassProcessImage()
{
	qualitaimg = 0;
	Dim.heel_toe = 0;
	Dim.instep = 0;
	Dim.metatarsal_girth = 0;
	Dim.neck_height = 0;
	Dim.width = 0;
}

Error_type ClassProcessImage::Process(AlgoParams * params)
{
	int r = LoadResizeImage(params->fname);
	if (r == 0)
		{ if (params->debug >= 1) imsave(GetOriginalImage(), "Original"); 	}
	else
		return  ERROR_LOAD_IMAGE; 

	/*r = ExtractCoins(1);
	if (r == 0)
		{	if (params->debug >= 1) imsave(coins[0].mask_coin, "Coins detected");		}
	else
		return  ERROR_NO_COIN_DETECTED;*/

	CoinDetector coin;
	coin.Scan(imageOriginal);
}


Mat ClassProcessImage::GetOriginalImage() { return imageOriginal; }
Mat ClassProcessImage::GetOriginalGrayImage() { return gray; }
Mat ClassProcessImage::GetMask() { return mask.mask_bw; }
Rect ClassProcessImage::GetROI() { return mask.bb; }
Mat ClassProcessImage::GetImageSegmented() { return img_segmented; }

void ClassProcessImage::SetOriginalImage(Mat im) { im.copyTo(imageOriginal); }
void ClassProcessImage::SetImageSegmented(Mat im){ im.copyTo(img_segmented); }
short int ClassProcessImage::ExtractCoins(const short int numCoins) {

	
	Mat im,im_gray;
	imageOriginal.copyTo(im);
	
	cvtColor(im, im_gray, COLOR_BGR2GRAY);
	//Mat th = do_threshold(imageOriginal);
	/***************************/

	/*erode(th, tmp, Mat::ones(3, 3, CV_8U));
	dilate(tmp, th, Mat::ones(3, 3, CV_8U));*/

	clahe_LChannel(im);
	Coin tmp;
	im.copyTo(tmp.mask_coin);
	coins.push_back(tmp);

	BlobDetector(im);
	Mat grayscale_image;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	//findContours(grayscale_image, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);
	findContours(im_gray, contours, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS);
	sort(begin(contours), end(contours), [](const vector<Point>& c1, const vector<Point>& c2) {	return c1.size() > c2.size();});
	{
		RNG rng(12345);
		Mat drawing = Mat::zeros(im_gray.size(), CV_8UC3);
		for (size_t i = 0; i < contours.size(); i++)
		{
			Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0, 256), rng.uniform(0, 256));
			drawContours(drawing, contours, (int)i, color, 2, LINE_8, hierarchy, 0);
		}
		Mat cth;
		cvtColor(im_gray, cth, COLOR_GRAY2RGB);
		//	rectangle(cth, bb, Vec3b(0, 0, 255), 1, LINE_AA);
		imsave(cth, "Threshold for extract the silhouette");
		//imsave(th, "A4");
	}
	
	

	return 0;




}
short int ClassProcessImage::LoadResizeImage (string fname) {
	//int ClassProcessImage::LoadResizeImage(const char *fname) {
	Mat im = imread(fname, IMREAD_COLOR);
	if (im.cols == 0 || im.rows == 0) return 1;
	if (im.cols > im.rows) {
		flip(im, im, 1);
		transpose(im, im);
		SetOriginalImage(im);
		
	}
	return 0;
}
ClassProcessImage::~ClassProcessImage()
{
}
