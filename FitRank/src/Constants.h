#pragma once

#include <vector>
#include <string>


using namespace std;

// COSTANTI DELL'ALGORITMO
enum Error_type {
	NO_ERROR,
	ERROR_LOAD_IMAGE,
	ERROR_IMAGE_SZ_DISAGREE,
	ERROR_NO_COIN_DETECTED,
	ERROR_NO_FOOT,
	ERROR_IMAGE_BLURED,
	ERROR_SHOOT_TOO_HIGH,
	ERROR_WARPING,
	ERROR_COIN_FORMAT_IN_ARGV,
	GENERIC_ERROR

};

enum Quality_IMG {
	Picture_Good_quality,
	Picture_size_less_then_1200x900_size,
	Picture_with_blurs_possible_uncertainties_in_measurements,
	Picture_with_shadows_possible_uncertainties_in_measurements

};

static const vector<string> Error_type_v{
	"NO_ERROR",
	"ERROR_LOAD_IMAGE",
	"ERROR_IMAGE_SZ_DISAGREE",
	"ERROR_NO_COIN_DETECTED",
	"ERROR_NO_FOOT",
	"ERROR_IMAGE_BLURED " ,
	"ERROR_SHOOT_TOO_HIGH",
	"ERROR_WARPING",
	"ERROR_COIN_FORMAT_IN_ARGV",
	"GENERIC_ERROR"

};

static const vector<string> Quality_IMG_v{
	"Picture_Good_quality",
	"Picture_size_less_then_1200x900_size",
	"Picture_with_blurs_possible_uncertainties_in_measurements",
	"Picture_with_shadows_possible_uncertainties_in_measurements"

};

//Dimensioni immagine virtuale
#define IMG_WIDTH_GL 1272
#define IMG_HEIGHT_GL 900

//Costanti limite correzione contrasto
#define MIN_M 105
#define MAX_M 135
#define MIN_E 103
#define MAX_E 110

// Veritici modello mesh
#define NUM 6

// Diametro monete 
static const float diam_Eu2 = static_cast<float> (pow(25.75, 2));
static const float diam_Eu1 = static_cast<float> (pow(23.255, 2));
static const float diam_Eu50c = static_cast<float> (pow(24.25, 2));
static const float diam_Eu20c = static_cast<float> (pow(22.25, 2));
static const float diam_Eu10c = static_cast<float> (pow(19.75, 2));
static const float diam_Eu5c = static_cast<float> (pow(21.25, 2));
static const float diam_Eu2c = static_cast<float> (pow(18.75, 2));
static const float diam_Eu1c = static_cast<float> (pow(16.25, 2));


static const vector <string> Coins_type{
					"Eu2",
					"Eu1",
					"Eu50c",
					"Eu20c",
					"Eu10c",
					"Eu5c",
					"Eu2c",
					"Eu1c",
					"none"
};



//Dimensioni immagine virtuale
static const int VIRTUAL_IMG_HEIGTH = 900;

constexpr double pi = 3.141592653589793238462643383279502884;
constexpr double _180overpi = 180.0 / pi;
constexpr double _360overpi = 360.0 / pi;
