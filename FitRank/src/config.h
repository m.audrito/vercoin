#pragma once
#include <vector>
#include <string>



//--------------------------------------------------------------------------------------------------
// Struttura dei parametri dell'algoritmo
//
struct AlgoParams {

	
	bool foot_sx=false;
	unsigned short int debug=0; //0= NO debug / 1=debug save images in local folder /11 =MACHINE LEARNING
	string fname = "";
	string	coin_type = "";
	string debug_save_path = "";
			   
		//-----parameter for deform model and get measure
	const char* Model_3D_file= "res/pie_inicio_midres_no_angled_translated_simplified.obj";;
	vector<int> indices_Mesh_Height = { 961, 1150 };
	vector<int> indices_Mesh_Ball_girth_circumference=  { 957, 925, 929, 878, 874, 609, 846, 838, 529, 550, 552, 591, 1295, 1277, 1278, 1418, 1415, 1422 };
	vector<int> indices_Mesh_Instep_circumference= { 972, 989, 985, 992, 1395, 1393, 1216, 1378, 1403, 1150, 161, 155, 496, 494, 770, 779, 784, 799, 801, 805, 807, 887, 895, 889, 899, 900, 962, 961 };
	vector<int> indices_Mesh_Length = { 761,184 };
	vector<int> indices_Mesh_Width = { 1278, 838 };
	float angle_Height = 30.0f;
	float angle_Width = 18.8f;
	float angle_Length = 5.3f;
	float bb_calc_width = 0.5; // based on Jurca paper


	// Parameter for processing images
	int a4_minAreaPerCent=10;
	int crop_px = 0;

	int sigmoid_M_max = 155;
	int sigmoid_M_min = 135;
	int sigmoid_E_max = 10;
	int sigmoid_E_min = 3;
	/*int findCorners_skipPixels;
	int findCorners_roiSize;
	int findCorners_skipPixels2;

	int ransac1_iterations;
	int ransac1_minInliersPercent;
	int ransac1_minInliersPercent_2nd;
	double ransac1_maxError;

	int ransac2_iterations;
	int ransac2_minInliersPercent;
	int ransac2_minInliersPercent_2nd;
	double ransac2_maxError;*/

	
	int num_max_blobs=4;
	
	float threeshold_bounding_box = 0.1f;

	//char *debug_save_path;
	
	// Warping parameters
	int virtual_img_WIDTH; // Virual Image
	int virtual_img_HEIGTH; // Virual Image
	float sheet_size_WIDTH; // sheet size in mm
	float sheet_size_HEIGTH;
	float calibrated_length;
	

	//void setParameters(AlgoParams &params)
	//{
	//	params.a4_minAreaPerCent = 10;// 10; 
	//	//params.findCorners_skipPixels = 40;
	//	//params.findCorners_roiSize = 80;
	//	//params.findCorners_skipPixels2 = 2;
	//	//params.ransac1_iterations = 150;
	//	//params.ransac1_minInliersPercent = 40;
	//	//params.ransac1_minInliersPercent_2nd = 30;
	//	//params.ransac1_maxError = 2.0;
	//	//params.ransac2_iterations = 100;
	//	//params.ransac2_minInliersPercent = 70;
	//	//params.ransac2_minInliersPercent_2nd = 35;
	//	//params.ransac2_maxError = 2.0;
	//	params.num_max_blobs = 4;
	//	params.sigmoid_M_min = 105;
	//	params.sigmoid_M_max = 135;
	//	params.sigmoid_E_max = 10;
	//	params.sigmoid_E_min = 3;

	//	params.bb_calc_width = 0.5; // based on Jurca paper 
	//	params.threeshold_bounding_box = 0.1f;
	//	params.debug = 0;
	//	params.debug_save_path = " ";
	//	params.crop_px = 0; // on A4 5 px means 1,2mm based on 1200x900 resolution

	//	// parameters for 3D model
	//	params.Model_3D_file = "res/pie_inicio_midres_no_angled_translated_simplified.obj";
	//	params.indices_Mesh_Ball_girth_circumference = { 957, 925, 929, 878, 874, 609, 846, 838, 529, 550, 552, 591, 1295, 1277, 1278, 1418, 1415, 1422 };
	//	params.indices_Mesh_Instep_circumference = { 972, 989, 985, 992, 1395, 1393, 1216, 1378, 1403, 1150, 161, 155, 496, 494, 770, 779, 784, 799, 801, 805, 807, 887, 895, 889, 899, 900, 962, 961 };
	//	params.indices_Mesh_Height = { 961, 1150 };
	//	params.indices_Mesh_Length = { 761,184 };
	//	params.indices_Mesh_Width = { 1278, 838 };
	//	params.angle_Height = 30.0f;
	//	params.angle_Width = 18.8f;
	//	params.angle_Length = 5.3f;

	//}
};








