#include <vector>
#include <limits>
#include <iostream>
#include <future>
#include <bitset>
#include <string>
#include <opencv2/opencv.hpp>
//#include "main_workflow.h"
#include "mesh.h"
#include "utility.h"
#include "loadobj.h"
#include "debugging.h"
#include "balance.h"
#include "ClassProcessImg.h"
#include "Constants.h"


//--------------------------------------------------------------------------------------------------
using namespace std;
using namespace cv;

//--------------------------------------------------------------------------------------------------
struct Corner_s {
	Line_s line1;
	Line_s line2;
	Point2d corner;
};

struct A4_2_s {
	Mat gray;
	vector<Point2d> vertices;
};



//// Segmentazione mediante K-mean clustering
Mat K_Cluster(const AlgoParams *params, Mat image)
{
	const int MAX_CLUSTERS = 3;
	Scalar colorTab[] =
	{
		Scalar(0, 0, 255),
		Scalar(255,100,100),
		Scalar(0,255,255)
	};
	Mat reshaped_image = image.reshape(1, image.cols * image.rows);
	Mat reshaped_image32f;
	reshaped_image.convertTo(reshaped_image32f, CV_32FC1, 1.0 / 255.0);
	Mat labels;
	TermCriteria criteria{ cv::TermCriteria::COUNT, 100, 1 };
	Mat centers;
	double compactness = kmeans(reshaped_image32f, MAX_CLUSTERS, labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, centers);

	//show_result(labels, centers, image.rows,image.cols);

	Mat rgb_image(image.rows, image.cols, CV_8UC3);
	MatIterator_<Vec3b> rgb_first = rgb_image.begin<Vec3b>();
	MatIterator_<Vec3b> rgb_last = rgb_image.end<Vec3b>();
	MatConstIterator_<int> label_first = labels.begin<int>();

	Mat centers_u8;
	centers.convertTo(centers_u8, CV_8UC1, 255.0);
	Mat centers_u8c3 = centers_u8.reshape(3);
	centers_u8c3.row(0) = colorTab[0];
	centers_u8c3.row(1) = colorTab[1];
	centers_u8c3.row(2) = colorTab[2];
	while (rgb_first != rgb_last) {
		const Vec3b& rgb = centers_u8c3.ptr<Vec3b>(*label_first)[0];
		*rgb_first = rgb;
		++rgb_first;
		++label_first;
	}
	return rgb_image;
}
//
//
//
//// Segmentation using K-clustering based on -LAB image and speeding up the procesisng resizing the image to 200 width image 
//Mat kClusterLABimage(const AlgoParams *params, Mat image)
//{
//	Mat temp;
//	
//	const int MAX_CLUSTERS = 3;  // number of cluster because in the image there is a white sheet , foot with dark soks and floor
//	//autoLevels(image); //34ms in esecuzione
//	//medianBlur(image, image, 9);
//	Mat img_LAB;
//	cvtColor(image, img_LAB, COLOR_BGR2Lab);
//	autoLevels(img_LAB); //34ms in esecuzione
//	medianBlur(img_LAB, img_LAB, 9); //144ms in esecuzione
//	int target_width = 200; 
//	int target_height = int(round(target_width * image.rows / image.cols));
//	Mat img_rez(target_height, target_width,  CV_8UC3);
//	resize(img_LAB, img_rez, img_rez.size(), 0, 0, CV_INTER_AREA);
//	
//	/*************************/
//	//  K-clustering processing and transform mat to permit this analisys
//	Mat reshaped_image = img_rez.reshape(1, img_rez.cols * img_rez.rows);
//	Mat reshaped_image32f;
//	reshaped_image.convertTo(reshaped_image32f, CV_32FC1, 1.0 / 255.0);
//	Mat labels;
//	TermCriteria criteria{ TermCriteria::COUNT, 100, 1 };
//	Mat centers;
//	double compactness = kmeans(reshaped_image32f, MAX_CLUSTERS, labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, centers);
//	//double compactness = kmeans(reshaped_image32f, MAX_CLUSTERS, labels, criteria, 1, KMEANS_PP_CENTERS, centers);
//
//	/*************************/
//	// LAB_image resized version of image in LAB and there will be the segmentation
//	Mat LAB_image(img_rez.rows, img_rez.cols, CV_8UC3);
//	MatIterator_<Vec3b> LAB_first = LAB_image.begin<Vec3b>();
//	MatIterator_<Vec3b> LAB_last = LAB_image.end<Vec3b>();
//	MatConstIterator_<int> label_first = labels.begin<int>();
//
//	Mat centers_u8;
//
//	centers.convertTo(centers_u8, CV_8UC1, 255.0);
//	Mat centers_u8c3 = centers_u8.reshape(3);
//
//	while (LAB_first != LAB_last) {
//		const Vec3b& lab = centers_u8c3.ptr<Vec3b>(*label_first)[0];
//		*LAB_first = lab;
//		++LAB_first;
//		++label_first;
//	}
//	/*********************************/
//	// with centroid and label found, we will  map the original LAB image considering the distance of 
//	//pixel value with the clusters calculated
//	int channels = img_LAB.channels();
//	int nRows = img_LAB.rows;
//	int nCols = img_LAB.cols * channels;
//	if (img_LAB.isContinuous())
//	{
//		nCols *= nRows;
//		nRows = 1;
//	}
//	int i, j;
//	Vec3d d1, d2, d3;
//	Vec3b* p;
//	Vec3b* pt_centroids= centers_u8c3.ptr<Vec3b>(0);
//	for (i = 0; i < img_LAB.rows; ++i)
//		{
//			p = img_LAB.ptr<Vec3b>(i);
//			for (j = 0; j < img_LAB.cols; ++j)
//			{
//			//	double dist[] = { norm(p[j] , pt_centroids[0]),norm(p[j] , pt_centroids[1]),norm(p[j] , pt_centroids[2]) };
//			//	int index = indexofSmallestElement(dist, 3);
//			//	int index = rand() % 3;
//				double d_1 = sqrt((p[j](0) - pt_centroids[0](0))*(p[j](0) - pt_centroids[0](0))
//								+ (p[j](1) - pt_centroids[0](1))*(p[j](1) - pt_centroids[0](1))
//								+ (p[j](2) - pt_centroids[0](0))*(p[j](2) - pt_centroids[0](2)));
//				double d_2 = sqrt((p[j](0) - pt_centroids[1](0))*(p[j](0) - pt_centroids[1](0))
//								+ (p[j](1) - pt_centroids[1](1))*(p[j](1) - pt_centroids[1](1))
//								+ (p[j](2) - pt_centroids[1](0))*(p[j](2) - pt_centroids[1](2)));
//				double d_3 = sqrt((p[j](0) - pt_centroids[2](0))*(p[j](0) - pt_centroids[2](0))
//								+ (p[j](1) - pt_centroids[2](1))*(p[j](1) - pt_centroids[2](1))
//								+ (p[j](2) - pt_centroids[2](0))*(p[j](2) - pt_centroids[2](2)));
//				double dist[] = { d_1,d_2,d_3 };
//				int index = indexofSmallestElement(dist, 3);
//				p[j]= pt_centroids[index];
//			}
//		}
//
//	Mat res;
//	
//	cvtColor(img_LAB, res,COLOR_Lab2BGR);
//	if (params->debug >= 1)
//	{
//		imsave(img_LAB, "K clustering on LAB_image");
//		
//	}
//	return res;
//}
////
////void PROVA_HOUGH(Process_A4_s M)
////{
////	Mat src, src1;// Declare the output variables
////	Mat dst, cdst, cdstP,tmp;
////	M.img_segmented.copyTo(src1);
////	cvtColor(src1, src, CV_BGR2GRAY); //durata 3ms
////	// Edge detection
////	//Canny(src, dst, 70, 180, 3);
////	// Copy edges to the images that will display the results in BGR
////
////	
////	vector<vector<Point> > contours;
////	vector<Vec4i> hierarchy;
////	double thresh = 50;
////	erode(src, tmp, Mat::ones(10, 10, CV_8U));
////	dilate(tmp, src, Mat::ones(10, 10, CV_8U));
////	Canny(src, dst, thresh, thresh * 3, 3);
////	
////	//findContours(dst, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
////	cvtColor(dst, cdst, COLOR_GRAY2BGR);
////	cdstP = cdst.clone();
////	// Standard Hough Line Transform
////	vector<Vec2f> lines; // will hold the results of the detection
////	HoughLines(dst, lines, 1, CV_PI / 180, 50, 0, 0); // runs the actual detection
////	// Draw the lines
////	for (size_t i = 0; i < lines.size(); i++)
////	{
////		float rho = lines[i][0], theta = lines[i][1];
////		Point pt1, pt2;
////		double a = cos(theta), b = sin(theta);
////		double x0 = a * rho, y0 = b * rho;
////		pt1.x = cvRound(x0 + 1000 * (-b));
////		pt1.y = cvRound(y0 + 1000 * (a));
////		pt2.x = cvRound(x0 - 1000 * (-b));
////		pt2.y = cvRound(y0 - 1000 * (a));
////		line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);
////	}
////	// Probabilistic Line Transform
////	vector<Vec4i> linesP; // will hold the results of the detection
////	HoughLinesP(dst, linesP, 1, CV_PI / 180, 50, 50, 5); // runs the actual detection
////	// Draw the lines
////	for (size_t i = 0; i < linesP.size(); i++)
////	{
////		Vec4i l = linesP[i];
////		line(cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, LINE_AA);
////	}
////	// Show results
////	/*imshow("Source", src);
////	imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst);
////	imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP);*/
////}
////
////void PROVA_HOUGH(Mat M)
////{
////	Mat src, src1;// Declare the output variables
////	Mat dst, cdst, cdstP;
////	M.copyTo(src);
////	//cvtColor(src1, src, CV_BGR2GRAY); //durata 3ms
////	// Edge detection
////	Canny(src, dst, 70, 180, 3);
////	// Copy edges to the images that will display the results in BGR
////	cvtColor(dst, cdst, COLOR_GRAY2BGR);
////	cdstP = cdst.clone();
////	// Standard Hough Line Transform
////	vector<Vec2f> lines; // will hold the results of the detection
////	HoughLines(dst, lines, 1, CV_PI / 180, 150, 0, 0); // runs the actual detection
////	// Draw the lines
////	for (size_t i = 0; i < lines.size(); i++)
////	{
////		float rho = lines[i][0], theta = lines[i][1];
////		Point pt1, pt2;
////		double a = cos(theta), b = sin(theta);
////		double x0 = a * rho, y0 = b * rho;
////		pt1.x = cvRound(x0 + 1000 * (-b));
////		pt1.y = cvRound(y0 + 1000 * (a));
////		pt2.x = cvRound(x0 - 1000 * (-b));
////		pt2.y = cvRound(y0 - 1000 * (a));
////		line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);
////	}
////	// Probabilistic Line Transform
////	vector<Vec4i> linesP; // will hold the results of the detection
////	HoughLinesP(dst, linesP, 1, CV_PI / 180, 50, 50, 5); // runs the actual detection
////	// Draw the lines
////	for (size_t i = 0; i < linesP.size(); i++)
////	{
////		Vec4i l = linesP[i];
////		line(cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, LINE_AA);
////	}
////	// Show results
////	/*imshow("Source", src);
////	imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst);
////	imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP);*/
////}


/*******************************************************************************/
//--------------------------------------------------------------------------------------------------
static int imload_and_resize(const char *fname, Mat &im, const AlgoParams *params) {

	im = imread(fname, IMREAD_COLOR);
	if (im.cols == 0 || im.rows == 0) return MB_ERROR_FILE_IMAGE;
	if (im.cols > im.rows) {
		flip(im, im, 1);
		transpose(im, im);
	}


	/* Sarebbe bene fare il resize dell'immagine verso una dimensione fissa, ad es. QXGA
	 */


	if (params->debug >= 1) imsave(im, "Original");
	return 0;
}





//Mat match_shape(Mat cnn,Mat stats,Mat centroids, Mat im_th)
//{
//	//const auto cptr = reinterpret_cast<int32_t*>(cnn.data + y * cnn.step);
//	//load template shape and find contours to use cv::Mastchshapes
//		
//	Mat plot_contour2,plot_contour;
//	Mat template_A5 = imread("C:\\Users\\Massimo\\Desktop\\template\\A5_template_mask_feet_outside_dx.jpg", CV_LOAD_IMAGE_GRAYSCALE);
//				/*if (params->foot_sx == 1)
//					flip(tt,tt, -1);*/
//		
//				vector<vector<Point>> contour_template;
//				vector<Vec4i> hierarchy_template;
//				int maxIdx_template;
//				Canny(template_A5, template_A5, 7 * 10, 7 * 22);
//				findContours(template_A5, contour_template, hierarchy_template, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
//				maxIdx_template = 0;
//				// TOGLIERE IL SORT E FARE RICERCA MASSIMO
//				sort(begin(contour_template), end(contour_template), [](const vector<Point>& c1, const vector<Point>& c2) {
//					//	return contourArea(c1, false) < contourArea(c2, false);
//					return c1.size() > c2.size();
//				});
//				Scalar color(rand() & 255, rand() & 255, rand() & 255);
//				drawContours(plot_contour2, contour_img, idx, color, 8, 8);
//				drawContours(plot_contour2, contour_template, 0, Scalar(128, 255, 255), 8, 8);
//			
//			
//		//	////	// find contour in image segmented
//				
//				vector<vector<Point>> contour_img;
//				vector<Vec4i> hierarchy_img;
//				
//				//autoLevels(img_org);
//				Mat grayscale_image;
//		
//				//cvtColor(im_th, grayscale_image, CV_BGR2GRAY);
//				im_th.copyTo(grayscale_image);
//				Canny(grayscale_image, grayscale_image, 50, 150); //durata 3ms (compreso dilate ed erode)
//				dilate(grayscale_image, grayscale_image, Mat());
//				erode(grayscale_image, grayscale_image, Mat());
//		//
//		//
//		//	////	
//		//	//////	findContours(grayscale_image, contour_img, hierarchy_img, RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS);
//				findContours(grayscale_image, contour_img, hierarchy_img, RETR_CCOMP, CHAIN_APPROX_NONE);
//				vector<double> vd,vd_hausdorff;
//				vector<int> id_cnt_image;
//		//
//		//
//		//	////
//		//
//				sort(begin(contour_img), end(contour_img), [](const vector<Point>& c1, const vector<Point>& c2) {
//					//return contourArea(c1, false) > contourArea(c2, false);
//					return c1.size() > c2.size();
//				});
//		//	//	/* COnvex hull to close conotur*/
//		//	//	
//		for (int idx = 0; idx < 4; idx++)
//					{
//						vector<Point> res;
//						Scalar color(rand() & 255, rand() & 255, rand() & 255);
//						drawContours(plot_contour, contour_img, idx, color, 8, 8);
//						//convexHull(contour_img[idx], res);
//						//result[idx].swap(res);
//					//	drawContours(grayscale_image, contour_img, idx, color, FILLED, 8);
//						vd.push_back(matchShapes(contour_template[0], contour_img[idx], CV_CONTOURS_MATCH_I2, 0));
//						vd_hausdorff.push_back(distance_hausdorff(contour_template[0], contour_img[idx]));
//						//drawContours(plot_contour3, res, 0, Scalar(0, 222, 0), FILLED, 8);
//						//polylines(plot_contour3, contour_img[idx], true, color);
//			
//					
//					}
//	/*sort(cnn.data, cnn.data + cnn.row * cnn.step, [](const vector<Point>& c1, const vector<Point>& c2) {
//
//		return c1.size() > c2.size();
//	});*/
//		return grayscale_image;
//
//}


//--------------------------------------------------------------------------------------------------
static Mask_s extract_A4(Mat &im, const AlgoParams *params) {
	Mat tmp,tmp2;
	Mat th = do_threshold(im);
	/***************************/
	//th.copyTo(tmp2);
	//Harris_corners_detector(tmp2);
	//PROVA_HOUGH(tmp2);
	
	
	erode(th, tmp, Mat::ones(3, 3, CV_8U));
	dilate(tmp, th, Mat::ones(3, 3, CV_8U));

	Mat cnn;
	Mat stats;
	Mat centroids;
	const int n = connectedComponentsWithStats(th, cnn, stats, centroids, 4);

	const int32_t Area = im.cols*im.rows;
	const int32_t minArea = Area * params->a4_minAreaPerCent / 100;

	Rect bb;
	int32_t max_area = 0;
	int max_i = 0;
	int32_t max_area_next = 0;
	int max_i_next = 0;
	for (int i = 1; i < n; ++i) {
		const int area = stats.at<int32_t>(i, CC_STAT_AREA);

		if (area > minArea && area > max_area) {
			bb.x = stats.at<int32_t>(i, CC_STAT_LEFT);
			bb.y = stats.at<int32_t>(i, CC_STAT_TOP);
			bb.width = stats.at<int32_t>(i, CC_STAT_WIDTH);
			bb.height = stats.at<int32_t>(i, CC_STAT_HEIGHT);
			max_area_next = max_area;
			max_i_next = max_i;
			max_area = area;
			max_i = i;
		}
	}
	//Mat res = match_shape(cnn, stats, centroids, th);
	for (int y = 0; y < th.rows; ++y) {
		const auto cptr = reinterpret_cast<int32_t*>(cnn.data + y * cnn.step);
		const auto tptr = th.data + y * th.step;
		for (int x = 0; x < th.cols; ++x) {
			/*if (cptr[x] != max_i) {
				tptr[x] = 0;
			}*/
			if ((cptr[x] != max_i)&&(cptr[x] != max_i_next)) {
				tptr[x] = 0;
			}
		}
	}

	if (params->debug >= 1) {
		Mat cth;
		cvtColor(th, cth, COLOR_GRAY2RGB);
		rectangle(cth, bb, Vec3b(0, 0, 255), 1, LINE_AA);
		imsave(cth, "Threshold for A4");
		imsave(th, "A4");
	}

	return { th, bb };
}

//--------------------------------------------------------------------------------------------------
static void check_range(const Mat &im, int * const x0, int * const x1, int * const y0, int * const y1) {
	const int x = 0;
	const int X = im.cols - 1;
	const int y = 0;
	const int Y = im.rows - 1;

	if (*x0 < x) *x0 = x;
	if (*x1 > X) *x1 = X;
	if (*y0 < y) *y0 = y;
	if (*y1 > Y) *y1 = Y;
}

//--------------------------------------------------------------------------------------------------
static void check_range(const Mat &im, Rect &r) {
	const int x = 0;
	const int X = im.cols - 1;
	const int y = 0;
	const int Y = im.rows - 1;

	int &x0 = r.x;
	int x1 = r.x + r.width;
	int &y0 = r.y;
	int y1 = r.y + r.height;

	if (x0 < x) x0 = x;
	if (x1 > X) x1 = X;
	if (y0 < y) y0 = y;
	if (y1 > Y) y1 = Y;

	r.width = x1 - x0;
	r.height = y1 - y0;
}

//--------------------------------------------------------------------------------------------------
enum {
	C_TOP_LEFT,
	C_TOP_RIGHT,
	C_BOTTOM_LEFT,
	C_BOTTOM_RIGHT,
};

static Corner_s find_corner(const Mat &im, const Rect &bb, const AlgoParams *params, int corner) {

	int x0 = bb.x;
	int x1 = x0 + bb.width - 1;
	int y0 = bb.y;
	int y1 = y0 + bb.height - 1;

	check_range(im, &x0, &x1, &y0, &y1);

	vector<Point> pp_hor;
	vector<Point> pp_ver;

	const int step = params->findCorners_skipPixels2;

	if (corner == C_TOP_LEFT) {
		for (int x = x1; x >= x0; x -= step) {
			for (int y = y0; y <= y1; ++y) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y1; y >= y0; y -= step) {
			for (int x = x0; x <= x1; ++x) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_TOP_RIGHT) {
		for (int x = x0; x <= x1; x += step) {
			for (int y = y0; y <= y1; ++y) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y1; y >= y0; y -= step) {
			for (int x = x1; x >= x0; --x) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_BOTTOM_LEFT) {
		for (int x = x1; x >= x0; x -= step) {
			for (int y = y1; y >= y0; --y) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y0; y <= y1; y += step) {
			for (int x = x0; x <= x1; ++x) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	else if (corner == C_BOTTOM_RIGHT) {
		for (int x = x0; x <= x1; x += step) {
			for (int y = y1; y >= y0; --y) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_hor.emplace_back(x, y);
					break;
				}
			}
		}
		for (int y = y0; y <= y1; y += step) {
			for (int x = x1; x >= x0; --x) {
				const uchar v = im.at<uchar>(y, x);
				if (v) {
					pp_ver.emplace_back(x, y);
					break;
				}
			}
		}
	}
	
	auto line1 = fit_line_w_ransac(pp_hor, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent, true);
	if (line1.type == 0) line1 = fit_line_w_ransac(pp_hor, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent_2nd, true);
	auto line2 = fit_line_w_ransac(pp_ver, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent, true);
	if (line2.type == 0)  line2 = fit_line_w_ransac(pp_ver, params->ransac2_iterations, params->ransac2_maxError, params->ransac2_minInliersPercent_2nd, true);

	if (angle_between_lines_is_bad(line1, line2))
		return {};

	const Point2d c = compute_corner(line1, line2);

	return { line1, line2, c };
}

//--------------------------------------------------------------------------------------------------
std::vector<Point2f> find_a4_corners(const Mat &orim, Mask_s &a4, const AlgoParams *params) {

	auto &im = a4.mask;
	auto &bb = a4.bb;

	int x0 = bb.x;
	int x1 = x0 + bb.width;
	int y0 = bb.y;
	int y1 = y0 + bb.height;

	check_range(im, &x0, &x1, &y0, &y1);

	vector<Point> p_top;  
	for (int x = x0; x < x1; x += params->findCorners_skipPixels) {
		for (int y = y0; y < y1; ++y) {
			const uchar v = im.at<uchar>(y, x);
			if (v) {
				p_top.emplace_back(x, y);
				break;
			}
		}
	}

	vector<Point> p_left;
	for (int y = y0; y < y1; y += params->findCorners_skipPixels) {
		for (int x = x0; x < x1; ++x) {
			const uchar v = im.at<uchar>(y, x);
			if (v) {
				p_left.emplace_back(x, y);
				break;
			}
		}
	}

	vector<Point> p_bottom;
	for (int x = x0; x < x1; x += params->findCorners_skipPixels) {
		for (int y = y1; y > y0; --y) {
			const uchar v = im.at<uchar>(y, x);
			if (v) {
				p_bottom.emplace_back(x, y);
				break;
			}
		}
	}

	vector<Point> p_right;
	for (int y = y0; y < y1; y += params->findCorners_skipPixels) {
		for (int x = x1; x > x0; --x) {
			const uchar v = im.at<uchar>(y, x);
			if (v) {
				p_right.emplace_back(x, y);
				break;
			}
		}
	}


	auto line_top = fit_line_w_ransac(p_top, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
	auto line_left = fit_line_w_ransac(p_left, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
	auto line_bottom = fit_line_w_ransac(p_bottom, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);
	auto line_right = fit_line_w_ransac(p_right, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent);

	if (line_top.type == 0) line_top = fit_line_w_ransac(p_top, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
	if (line_left.type == 0) line_left = fit_line_w_ransac(p_left, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
	if (line_bottom.type == 0) line_bottom = fit_line_w_ransac(p_bottom, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);
	if (line_right.type == 0) line_right = fit_line_w_ransac(p_right, params->ransac1_iterations, params->ransac1_maxError, params->ransac1_minInliersPercent_2nd);

	const auto c1 = compute_corner(line_top, line_left);
	const auto c2 = compute_corner(line_top, line_right);
	const auto c3 = compute_corner(line_bottom, line_left);
	const auto c4 = compute_corner(line_bottom, line_right);

	vector<Point2f> vertices;
	Rect r1, r2, r3, r4;
	Corner_s p1, p2, p3, p4;


	const auto is_null = [](const Point2d &p) {
		return p.x == 0.0 && p.y == 0.0;
	};
	if (!is_null(c1) && !is_null(c2) && !is_null(c3) && !is_null(c4)) {

		const int S = params->findCorners_roiSize;
		r1 = Rect(lround(c1.x - S + 0.5), lround(c1.y - S + 0.5), 2 * S, 2 * S);
		r2 = Rect(lround(c2.x - S + 0.5), lround(c2.y - S + 0.5), 2 * S, 2 * S);
		r3 = Rect(lround(c3.x - S + 0.5), lround(c3.y - S + 0.5), 2 * S, 2 * S);
		r4 = Rect(lround(c4.x - S + 0.5), lround(c4.y - S + 0.5), 2 * S, 2 * S);

		check_range(orim, r1);
		check_range(orim, r2);
		check_range(orim, r3);
		check_range(orim, r4);

		const Mat corner_mask(orim.size(), CV_8U, { 0,0,0 });
		const auto i1 = orim(r1);
		auto m1 = corner_mask(r1);
		do_threshold(i1, m1);
		const auto i2 = orim(r2);
		auto m2 = corner_mask(r2);
		do_threshold(i2, m2);
		const auto i3 = orim(r3);
		auto m3 = corner_mask(r3);
		do_threshold(i3, m3);
		const auto i4 = orim(r4);
		auto m4 = corner_mask(r4);
		do_threshold(i4, m4);

		p1 = find_corner(corner_mask, r1, params, C_TOP_LEFT);
		p2 = find_corner(corner_mask, r2, params, C_TOP_RIGHT);
		p3 = find_corner(corner_mask, r3, params, C_BOTTOM_LEFT);
		p4 = find_corner(corner_mask, r4, params, C_BOTTOM_RIGHT);

		vertices = { p1.corner, p2.corner, p3.corner, p4.corner };
		if (is_null(p1.corner) || is_null(p2.corner) || is_null(p3.corner) || is_null(p4.corner)) {
			vertices.clear();
		}
	}


	if (params->debug >= 1) {
		Mat cim;
		orim.copyTo(cim);

		draw_line(cim, line_top, 3000, {0, 0, 255});
		draw_line(cim, line_left, 3000, { 0, 0, 255 });
		draw_line(cim, line_bottom, 3000, { 0, 0, 255 });
		draw_line(cim, line_right, 3000, { 0, 0, 255 });

		rectangle(cim, r1, Vec3b(0, 0, 255), 2, LINE_AA);
		rectangle(cim, r2, Vec3b(0, 0, 255), 2, LINE_AA);
		rectangle(cim, r3, Vec3b(0, 0, 255), 2, LINE_AA);
		rectangle(cim, r4, Vec3b(0, 0, 255), 2, LINE_AA);

		imsave(cim, "A4 vertices (1)");

		orim.copyTo(cim);
		draw_line(cim, p1.line1, p1.corner, 3000, {0, 0, 255});
		draw_line(cim, p1.line2, p1.corner, 3000, {0, 0, 255});
		draw_line(cim, p2.line1, p2.corner, 3000, {0, 0, 255});
		draw_line(cim, p2.line2, p2.corner, 3000, {0, 0, 255});
		draw_line(cim, p3.line1, p3.corner, 3000, {0, 0, 255});
		draw_line(cim, p3.line2, p3.corner, 3000, {0, 0, 255});
		draw_line(cim, p4.line1, p4.corner, 3000, {0, 0, 255});
		draw_line(cim, p4.line2, p4.corner, 3000, {0, 0, 255});

		circle(cim, p1.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
		circle(cim, p2.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
		circle(cim, p3.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
		circle(cim, p4.corner, 3, Vec3b(255, 255, 0), 1, LINE_AA);
		/******************************* draw text on vertices**************/
		char txt[20];
		snprintf(txt, 20, "%i", 0);
		putText(cim, txt, p1.corner + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		snprintf(txt, 20, "%i", 1);
		putText(cim, txt, p2.corner + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		snprintf(txt, 20, "%i", 2);
		putText(cim, txt, p3.corner + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		snprintf(txt, 20, "%i", 3);
		putText(cim, txt, p4.corner + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		/*********************************************************/
		imsave(cim, "A4 vertices (2nd method threshold)");
	}

	return vertices;
}

//--------------------------------------------------------------------------------------------------
static Point2f my_normalize(const Point2f &p, double *len_ = nullptr) {
	const auto len = sqrt(p.x*p.x + p.y*p.y);
	if (len_) *len_ = len;
	return p / len;
}


static void flip_image(vector<Point2f> &vv, Mask_s &a4, Mat &im, const AlgoParams *params) {

	auto p0 = vv[0];
	auto p1 = vv[1];
	auto p2 = vv[2];
	auto p3 = vv[3];

	auto &mask = a4.mask;

	const double D = 20.0;

	const auto r0 = my_normalize(p3 - p0);
	const auto r1 = my_normalize(p2 - p1);

	p0 += D*r0;
	p1 += D*r1;
	p2 -= D*r1;
	p3 -= D*r0;

	double L, R;
	const auto l = my_normalize(p2 - p0, &L);
	const auto r = my_normalize(p3 - p1, &R);

	int cl = 0;
	for (int t=0; t<L; ++t) {
		const Point2d p = p0 + t*l;
		const auto x = int(p.x);
		const auto y = int(p.y);
		if (mask.at<uchar>(y,x) == 0) ++cl;
	}

	int cr = 0;
	for (int t=0; t<R; ++t) {
		const Point2d p = p1 + t*r;
		const auto x = int(p.x);
		const auto y = int(p.y);
		if (mask.at<uchar>(y,x) == 0) ++cr;
	}


	if (cr > cl) {
		flip(im, im, -1);
		flip(mask, mask, -1);

		const float w = float(im.cols); // TODO: PAN: Check for an off-by-one error
		const float h = float(im.rows);
		for(auto &p: vv)
		{
			p.x = w - p.x;
			p.y = h - p.y;
		}

		swap(vv[0], vv[3]);
		swap(vv[1], vv[2]);
	}


	if (params->debug >= 1) {

		Mat cim;
		im.copyTo(cim);

		draw_vertices(cim, vv, { 0, 0, 255 }, 3, false);

		imsave(cim, (cr > cl) ? "Rotated (yes)" : "Rotated (no)");
	}
}


//--------------------------------------------------------------------------------------------------
/// Routine che estrae i vertici del foglio A4 dall'immagine
/// Riempie la struttura Process_A4_Args_s con i risultati
//static int process_for_a4(Process_A4_Args_s &args) {
//
//    auto &im = args.im;
//
//    //--- PREPROCESSING --------------------------------
//    /// Viene fatto un piccolo preprocessing:
//    /// bilanciamento del colore e blur 3x3
//    im = color_balance(im);
//    blur(im, im, {3,3});
//    if (args.params->debug >= 1) imsave(im, "Balanced");
//
//    //--- FOGLIO A4 ------------------------------------
//    /// Si estrae il foglio A4 mediante thresholding
//    /// usando l'allgoritmo Intermodes.
//    /// Poi si fittano delle rette lungo i lati del foglio per individuare la posizione
//    /// precisa dei suoi vertici
//    /// Se serve, l'immagine viene flippata dx/sx
//
//
//
//    auto a4_mask = extract_A4(im, args.params);
//    auto a4_vertices = find_a4_corners(im, a4_mask, args.params);
//	int gamma = MIN_E;
//	int m = MIN_M;
//	while ( (a4_vertices.size() != 4)&& (gamma < MAX_E))
//	{
//				sigmoide_contrasto(im, im, gamma - 100.f, float(m)); 
//				a4_mask = extract_A4(im, args.params);
//			    a4_vertices = find_a4_corners(im, a4_mask, args.params);
//				if((m < MAX_M))
//				{ ++gamma;}
//				else 
//					++m;
//
//	}
//	if (a4_vertices.size() != 4)
//			{
//
//				return MB_ERROR_NO_A4;
//			}
//    flip_image(a4_vertices, a4_mask, im, args.params);
//	swap(a4_vertices[2], a4_vertices[3]);
//    args.a4 = {im, a4_vertices, a4_mask};
//    return MB_ERROR_OK;
//}



vector<Point> first_II_elements_threshold(vector<vector<Point>> cnt, Mat img, float threeshold, Point2f mc)
{
	
	// find Maximum two values in the vector contour size
		vector<vector<Point>> ret;
		int index_I = 0;
		int index_II = 0;
		int max_val_I = 0;
		int max_val_II = 0;
		for (int i = 0; i < cnt.size(); i++)
		{
			if ((cnt[i].size() >= max_val_I )&&  (cnt[i].size() > max_val_II))
			{
				max_val_II = max_val_I;
				index_II = index_I;
				max_val_I = cnt[i].size();
				index_I = i;
			}
			else
				if (cnt[i].size() > max_val_II)
				{
					max_val_II = cnt[i].size();
					index_II = i;
				}

		}
		
		vector<Point>temp_cnt;
		temp_cnt = cnt[index_I];
		Rect bb_cnt = boundingRect(temp_cnt);

		if (bb_cnt.area() == img.total() * 0.99)
		{
			temp_cnt = cnt[1];
			bb_cnt = boundingRect(temp_cnt);

		}
		Moments mu = moments(temp_cnt, true);
		//  Get the mass centers:
		Point2f mc_img = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);
		
		/********************************************/
		Mat dst;
		dst=Mat::zeros(img.size(), CV_8U);
		//const Scalar Color = { 255,255,255 };
		rectangle(dst, bb_cnt.tl(), bb_cnt.br(), 255, 2);

		/********************************************/
		if (((mc.x >= (mc_img.x *0.77)) || (mc.x <= (mc_img.x *1.33))) && ((mc.y >= (mc_img.y *0.77)) || (mc.y <= (mc_img.y *1.33))))
		{
			
			return(temp_cnt);
		}
		else
		{
			temp_cnt.clear();
			temp_cnt.push_back( Point(0, 0));
			return(temp_cnt);
		}
}
vector<Point> extract_silhouette_K_clustering(Mat in_image, int *flagimg, const AlgoParams *params, Point2f mc)
{
	Mat K_clustering_segmentation = K_Cluster(params, in_image);
	//Mat K_clustering_segmentation;
	in_image.copyTo(K_clustering_segmentation);
	Mat plot_contour = Mat::zeros(K_clustering_segmentation.size(), CV_8UC3); 
	Mat plot_contour22 = Mat::zeros(K_clustering_segmentation.size(), CV_8UC3);
	// get the color on the center of the A4 sheet where more likely there will be  the feet
	Point cc_on_img_segmented;// find_center_a4_on_image_segmented(a4_1.vertices[0], a4_1.vertices[1], a4_1.vertices[2], a4_1.vertices[3]);
	auto r = getIntersectionPoint({ K_clustering_segmentation.cols,0 }, { 0,K_clustering_segmentation.rows }, { K_clustering_segmentation.cols,K_clustering_segmentation.rows }, {0,0}, cc_on_img_segmented);
	int re = cc_on_img_segmented.x;// -static_cast<int>(a4_1.vertices[0].x - params->crop_px);
	Vec3b color = K_clustering_segmentation.at<Vec3b>(cc_on_img_segmented.y, re);
	inRange(K_clustering_segmentation, color, color, plot_contour);
	/*int width_area = (static_cast<int>(a4_1.vertices[2].x) - static_cast<int>(a4_1.vertices[0].x));
	int heigth_area = (max(static_cast<int>(a4_1.vertices[2].y), static_cast<int>(a4_1.vertices[0].y)));
	Rect roi = Rect(a4_1.vertices[0].x, 0, width_area, heigth_area);*/
	
	vector<vector<Point>> contour;
	vector<Vec4i> hierarchy;
	findContours(plot_contour, contour, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	sort(begin(contour), end(contour), [](const vector<Point>& c1, const vector<Point>& c2) {

		return c1.size() > c2.size();
	});
	drawContours(plot_contour22, contour, 0, Scalar(128, 255, 255), 8, 8);
	if (params->debug >= 1)
	{
		imsave(K_clustering_segmentation, "K clustering on ROI image");
		imsave(plot_contour22, "K clustering on ROI image silhuoette");
	}
	return contour[0];
}

vector<Point>extract_silhouette(Mat in_image, int *flagimg, const AlgoParams *params, Point2f mc)
 {


	Mat im;
	in_image.copyTo(im);
	Mat th = do_threshold(im);

	Mat tmp, tmp2;
	erode(th, tmp, Mat::ones(3, 3, CV_8U));
	dilate(tmp, th, Mat::ones(3, 3, CV_8U));
	/***************************/
	tmp.copyTo(tmp2);
	//Harris_corners_detector(tmp2);
	//PROVA_HOUGH(tmp2);
	//auto a4_s = extract_A4_from_threeshold(tmp2, params);
	/***********************************************/
	/*Mat cnn;
	Mat stats;
	Mat centroids;
	const int n = connectedComponentsWithStats(th, cnn, stats, centroids, 4);

	const int32_t Area = im.cols*im.rows;
	const int32_t minArea = Area * params->a4_minAreaPerCent / 100;

	Rect bb;
	int32_t max_area = 0;
	int max_i = 0;
	for (int i = 1; i < n; ++i) {
		const int area = stats.at<int32_t>(i, CC_STAT_AREA);

		if (area > minArea && area > max_area) {
			bb.x = stats.at<int32_t>(i, CC_STAT_LEFT);
			bb.y = stats.at<int32_t>(i, CC_STAT_TOP);
			bb.width = stats.at<int32_t>(i, CC_STAT_WIDTH);
			bb.height = stats.at<int32_t>(i, CC_STAT_HEIGHT);
			max_area = area;
			max_i = i;
		}
	}

	for (int y = 0; y < th.rows; ++y) {
		const auto cptr = reinterpret_cast<int32_t*>(cnn.data + y * cnn.step);
		const auto tptr = th.data + y * th.step;
		for (int x = 0; x < th.cols; ++x) {
			if (cptr[x] != max_i) {
				tptr[x] = 0;
			}
		}
	}*/
	Mat grayscale_image;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
				//findContours(grayscale_image, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);
	findContours(th, contours, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS);
	sort(begin(contours), end(contours), [](const vector<Point>& c1, const vector<Point>& c2) {

				return c1.size() > c2.size();
			});
	if (params->debug >= 1) {
		Mat cth;
		cvtColor(th, cth, COLOR_GRAY2RGB);
	//	rectangle(cth, bb, Vec3b(0, 0, 255), 1, LINE_AA);
		imsave(cth, "Threshold for extract the silhouette");
		//imsave(th, "A4");
	}
	return(contours[0]);

}
/**
 * Estrae la sagoma del piede e del foglio partendo da una foto.
 * @param nomefile Nome del file contente la foto da elaborare.
 * @param flagimg Variabile per il salvataggio dei flag descrittivi dell’immagine.
 * @return Sagoma .
 */
vector<Point> estrazione_sagoma(Mat in_image, int *flagimg, const AlgoParams *params, Point2f mc) {


	auto input_image = in_image;
	//if ( (input_image.rows < height)) {
	//	*flagimg | 1;
	//	return {  };
	//}

	// lavoro sui 3/4 foglio parte alta in modo da togliere effetti caviglia

	/*Rect region_of_interest = Rect(0, 0, int(IMG_WIDTH*0.75), int(IMG_HEIGHT * 0.75));
	Mat image_roi = input_image(region_of_interest);*/



	autoLevels(input_image); //34ms in esecuzione
   // medianBlur(input_image, input_image, 9); //144ms in esecuzione

	//detect blur
	Mat grayscale_image;
	cvtColor(input_image, grayscale_image, CV_BGR2GRAY);

	Mat temporary_image;
	Laplacian(grayscale_image, temporary_image, CV_64F);
	Scalar mean, stddev; //0:1st channel, 1:2nd channel and 2:3rd channel
	meanStdDev(temporary_image, mean, stddev, Mat());
	if (stddev.val[0] * stddev.val[0] < 11) {
		*flagimg |= 2;
	}

	int conta_iterazioni = 0;

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	for (int gamma = params->sigmoid_E_min; gamma < params->sigmoid_E_max; ++gamma) {
		for (int m = params->sigmoid_M_min; m < params->sigmoid_M_max; ++m) {

			sigmoide_contrasto(input_image, temporary_image, gamma , float(m)); //durata 4ms
			cvtColor(temporary_image, grayscale_image, CV_BGR2GRAY); //durata 3ms

			Canny(grayscale_image, grayscale_image, 7 * 10, 7 * 22); //durata 3ms (compreso dilate ed erode)
			dilate(grayscale_image, grayscale_image, Mat());
			erode(grayscale_image, grayscale_image, Mat());


			//findContours(grayscale_image, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE);
			findContours(grayscale_image, contours, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_TC89_KCOS);
			const auto max_cnt =first_II_elements_threshold(contours, grayscale_image,params->threeshold_bounding_box, mc);
			
			if (max_cnt.size() != 1) {
				if (m == params->sigmoid_M_max && gamma == params->sigmoid_E_max)
					*flagimg |= 4;
				if (params->debug >= 1) {
					imsave(grayscale_image, "Bounding Box Piede Metodo sigmoide");
				}
				return { move(max_cnt) };
			}
			conta_iterazioni++;

		}
	}

	*flagimg |= 4;
	return { {} };
}



void fillSinglePoly(Mat &image, const vector<Point> &contour, const int color, const Point &offset = Point())
{
	auto x = contour.data();
	auto sz = int(contour.size());
	fillPoly(image, &x, &sz, 1, color, 8, 0, offset);
}

/**
 * Analizza i punti di una sagoma ed estrae i vertici del foglio.
 * @param paper_and_foot_path   Vettore di punti della sagoma da analizzare.
 * @param flagimg               Variabile per il salvataggio dei flag descrittivi dell’immagine.
 * @return Vertici (4) o array vuoto in caso di errore
 */
vector<Point> estrazione_vertici(const vector<Point>& paper_and_foot_path, int *flagimg, int width, int height) { // estrae i vertici, molto rapida e stabile
	Mat imgsagoma;

	RotatedRect rettangolo = minAreaRect(paper_and_foot_path);
	vector<Point2f> vertici(4);
	rettangolo.points(&vertici[0]);

	vector<Point> hull;
	convexHull(paper_and_foot_path, hull, false, true);

	// Individuazione vertici

	auto distamin = std::numeric_limits<double>::max();
	auto distbmin = std::numeric_limits<double>::max();
	auto distcmin = std::numeric_limits<double>::max();
	auto distdmin = std::numeric_limits<double>::max();
	vector<Point> vert(4);
	for (auto& point : hull)
	{
		const Point2f curPoint2f = point;

		const auto dista = distance(curPoint2f, vertici[0]);
		const auto distb = distance(curPoint2f, vertici[1]);
		const auto distc = distance(curPoint2f, vertici[2]);
		const auto distd = distance(curPoint2f, vertici[3]);

		if (dista <= distb && dista <= distc && dista <= distd && dista < distamin) {
			distamin = dista;
			vert[0] = point; //più vicino ad A
		}
		if (distb <= dista && distb <= distc && distb <= distd && distb < distbmin) {
			distbmin = distb;
			vert[1] = point; //più vicino a B
		}
		if (distc <= dista && distc <= distb && distc <= distd && distc < distcmin) {
			distcmin = distc;
			vert[2] = point; //più vicino a C
		}
		if (distd <= dista && distd <= distb && distd <= distc && distd < distdmin) {
			distdmin = distd;
			vert[3] = point; //più vicino a D
		}
	}

	//Verifica esattezza vertici trovati e qualità inquadratura
	//Elimina il quadrilatero e conta i punti bianchi. Se sono troppi un vertice è saltato
	{
		const auto area = contourArea(vert);
		//const float areaPre = contourArea(hull);

		Mat test_vertex(width, height, CV_8UC1);
		test_vertex = Scalar(0, 0, 0);
		fillSinglePoly(test_vertex, paper_and_foot_path, 255);
		fillConvexPoly(test_vertex, vert, 0);

		//
		const auto percentuale_foglio = area * 100.0 / (width*height);
		const auto percentuale_errore_vertici = countNonZero(test_vertex == 255)*100.0 / area;
	
	}
	return vert;
}




/*
* Calcola la matrice di warping .
* @param vertices Vettore contenente i vertici del foglio.

* @return warping matrix*/
Mat warping_Mat(const vector<Point2f> &vertices, float img_w,float img_h)
{
	// Image with FOOT ON Right SIDE of the sheet 
	//const vector<Point2f> puntiwarp{
	//		{ 0, img_h-1  },
	//		{ 0, 0 },
	//		{ img_w-1 , 0 },
	//		{ img_w-1 , img_h-1 },
	//};

	// Warping in order to have the toe on the right side of the screen 
	const vector<Point2f> puntiwarp{
			{ img_w  , 0 },
			{ img_w , img_h },
			{ 0, img_h   },
			{ 0, 0 }			
		};

	assert(sqrDistance(vertices[0], vertices[1]) < sqrDistance(vertices[3], vertices[1]));
	return ( getPerspectiveTransform(vertices, puntiwarp));
}

/*
 * Applica una trasformazione prospettica all’immagine per uniformare il punto di vista delle diverse possibili immagini.
 * @param img_final: immagine finalec contenente warping della sagoma
 * @param contour Sagoma da trasformare.
 * @return Sagoma trasformata, e la sua immagine schermo.
 */
vector<Point> warpsagoma_img(const vector<Point> &contour, Mat &img_final,Mat warp_Mat, int img_h, int img_w,int crop) {
	
	auto transformed_contour = vectorAsBigAs<Point2f>(contour);
	transformed_contour.resize(contour.size());
	perspectiveTransform(convert<Point2f>(contour), transformed_contour, warp_Mat);

	

	Mat sagoma_cut(img_h , img_w, CV_8UC1);
	//Mat sagoma_cut(img_h - 2 * crop, img_w - 2 * crop, CV_8UC1);
	sagoma_cut = 255;

	Rect roi=Rect ( crop,crop, img_w -crop,img_h-crop);

	fillSinglePoly(sagoma_cut, convert<Point>(transformed_contour), 0);
	
	
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(sagoma_cut, contours, hierarchy, CV_RETR_LIST, CHAIN_APPROX_NONE); //durata 1.2ms
	const auto contourIt = min_element_fn_threshold(contours.begin(), contours.end(), sagoma_cut.total() / 10.0, [](auto &&r) {return contourArea(r); }); // PAN: Maintained implicit assumption: area exists (we should check the return of max_element_fn)

	//sagoma_cut.copyTo(img_final);

	img_final = sagoma_cut(roi);
	if (contourIt != end(contours))
	{
		return { move(*contourIt) };
	}
	return { vector<Point>() };
}

/**
* Funzione calcolo parametri calibrati.
* @param nomefile: Nome della foto da elaborare.
* @param flagimg: Variabile per il salvataggio dei flag descrittivi dell’immagine.
* @param width: variabile larghezza minima del foglio 1200
* @param height: variabile lunghezza minima del foglio 900
* @return contorno sagoma immagine.
*/

float calibrated_dimension(float dim_foot_px,float dim_sheet_mm ,float dim_sheet_px, int crop) {
	return { (dim_foot_px + (2 * crop))*(dim_sheet_mm / (dim_sheet_px)) };

}

/**
* Funzione di calcolo larghezza piede.
* @param img_finale: imamagine virtuale da elaborare.
* @param bb_contour:  bounding box piede su cui applicare area di calcolo larghezza partendo da top del rettangolo.
* @param bb_width: percentuale da applicare sul bounding box per trovare area di calcolo larghezza
* @return rettangolo larghezza.
*/

Rect witdh_foot_Rect(const Mat img_finale,const Rect bb_contour, const float bb_width,vector<Point2f> sheet_vertices) {

	//Rect region_of_interest((bb_contour.width*bb_width), 0, (bb_contour.br().x- (bb_contour.width*bb_width)), img_finale.rows);
	Rect region_of_interest;
	float min_vertex_x = min(sheet_vertices[1].x, sheet_vertices[0].x);
	if (min_vertex_x > bb_contour.width)
		//foot inside the sheet
	{
		region_of_interest.x = ((bb_contour.width*bb_width));
		region_of_interest.y = 0;
		region_of_interest.width = (img_finale.cols- region_of_interest.x);//bb_contour.br().x -(bb_contour.width*bb_width));
		region_of_interest.height = img_finale.rows;
	}
	else
	{ // foot over the sheet
		region_of_interest.x = ((bb_contour.width*bb_width));
		region_of_interest.y = 0;
		region_of_interest.width = min_vertex_x - (bb_contour.width*bb_width);
		region_of_interest.height = img_finale.rows;
	}

	Mat image_roi =img_finale(region_of_interest);

	vector<vector<Point>> contour;
	vector<Vec4i> hierarchy2;


	findContours(image_roi, contour, hierarchy2, CV_RETR_LIST, CHAIN_APPROX_NONE);
		int index = 0;
		int max_val = 0;
		// find Maximum value in the vector contour size
		for (int i = 0; i < contour.size(); i++)
		{
			if (contour[i].size() >= max_val)
			{
				max_val = contour[i].size();
				index = i;
			}
		}
	
	
	Rect bb = boundingRect(contour[index]);
	if (min_vertex_x < bb_contour.width)
		//foot inside the sheet
	{ 
		Rect trasl_bb(bb.tl().x + (bb_contour.br().x - (bb_contour.width*bb_width)), bb.tl().y, bb.width, bb.height);
		return (trasl_bb);
	}
	else
	{
		Rect trasl_bb(bb.tl().x + (bb_contour.br().x - (bb_contour.width*bb_width)), bb.tl().y, bb.width, bb.height);
		return (trasl_bb);
	}
	
	
}

/**
* Funzione di estrazione della sagoma del foglio mediante  correzioen prospettica e calcolo lunghezza e larghezza piede.
* @param img:  mask della foto da elaborare.
* @param flagimg: Variabile per il salvataggio dei flag descrittivi dell’immagine.
* @param WM: matrice di warping
* @param dst: imamagine finale per debug
* @param params:parametri di setting per il corretto funzionamento algoritmo
* &dimensioni:array delle dimensioni
* &mc:center mass ottenuta elaborando la maschera con threesholding
* @return flag di  riuscita calcolo lunghezza e larghezza, zero se non si sono errori*/ 

int stima_dim_shadow_removal_virtual_image(Mat img, int *flagimg, const Mat WM, Mat &dst, const AlgoParams *params, float(&dimensioni)[NUM],Point2f mc, vector<Point2f> sheet_vertices_warped)
{
	// ELaborazione immagine ed estrazione sagoma
	/*const auto sagoma_cnt_bio = estrazione_sagoma(img, flagimg, params,mc);
	const auto &sagoma_cnt_th = extract_silhouette(img, flagimg, params, mc);*/
	const auto sagoma_cnt = extract_silhouette_K_clustering(img, flagimg, params, mc);
	
	if (!sagoma_cnt.empty())
	{
		if (sagoma_cnt.size()<100)
			return 1;
		else
		{ 

			dst = Mat::zeros(dst.size(), CV_8UC1);
			vector<vector<Point>> cnt;
			cnt.push_back(sagoma_cnt);
			const Scalar Color = { 255,255,255 };
			// Find external point to close contour
		
			drawContours(dst, cnt, 0, Color, CV_FILLED, 1);

			Rect bb_countour_biocubica = boundingRect(sagoma_cnt);
			dimensioni[0] = calibrated_dimension(bb_countour_biocubica.br().x, params->sheet_size_WIDTH, params->calibrated_length, 0);
			Rect temp = witdh_foot_Rect(dst, bb_countour_biocubica, params->bb_calc_width, sheet_vertices_warped);
			// CHECK IF WIDTH IS SAME SIZE SHEET!!!
			dimensioni[2] = calibrated_dimension(temp.height, params->sheet_size_WIDTH, params->calibrated_length, 0);
		
			if (params->debug >= 1) {
				cvtColor(dst, dst, COLOR_GRAY2BGR);
				rectangle(dst, bb_countour_biocubica.tl(), bb_countour_biocubica.br(), Scalar(0, 255, 0), 2);
				rectangle(dst, temp.tl(), temp.br(), Scalar(0, 0, 255), 2);
				imsave(dst, "Bounding Box width and Length");
			}
			return 0;
		}
		
	}
	else
		return 1;

}


int stima_dim_shadow_removal_virtual_image_inside_feet(Mat img, int *flagimg, const Mat WM, Mat &dst, const AlgoParams *params, float(&dimensioni)[NUM], Point2f mc, vector<Point2f> sheet_vertices_warped)
{
	// ELaborazione immagine ed estrazione sagoma
	const auto sagoma_cnt= estrazione_sagoma(img, flagimg, params,mc);
	

	if (!sagoma_cnt.empty())
	{
		dst = Mat::zeros(dst.size(), CV_8UC1);
		vector<vector<Point>> cnt;
		cnt.push_back(sagoma_cnt);
		const Scalar Color = { 255,255,255 };
		// Find external point to close contour

		drawContours(dst, cnt, 0, Color, CV_FILLED, 1);
		Rect bb_countour_biocubica = boundingRect(sagoma_cnt);

		float diff_area = (dst.size().area() - bb_countour_biocubica.area());

	
			dimensioni[0] = calibrated_dimension(bb_countour_biocubica.br().x, params->sheet_size_WIDTH, params->calibrated_length, 0);
			Rect temp = witdh_foot_Rect(dst, bb_countour_biocubica, params->bb_calc_width, sheet_vertices_warped);
			// CHECK IF WIDTH IS SAME SIZE SHEET!!!
			dimensioni[2] = calibrated_dimension(temp.height, params->sheet_size_WIDTH, params->calibrated_length, 0);
		
			if (params->debug >= 1) {
				cvtColor(dst, dst, COLOR_GRAY2BGR);
				rectangle(dst, bb_countour_biocubica.tl(), bb_countour_biocubica.br(), Scalar(0, 255, 0), 2);
				rectangle(dst, temp.tl(), temp.br(), Scalar(0, 0, 255), 2);
				imsave(dst, "Bounding Box width and Length");
			}
			return 0;
		
	}
	else
		return 1;

}

/**
* Funzione di calcolo dimensione piede con maschera ottenuta dalla fase di pre processing, mediante warping su immagine virtuale schermo)
* @param Img_input: immagine input
* @param img__final:immagine output
* @param warp_Mat:Matrice di warping
* @param dimensioni:array delle dimensioni
* @param params:parametri di setting per il corretto funzionamento algoritmo
* @return  calcolo lunghezza */


float stima_dim_on_mask(Mat in, Mat &img_final, Mat warp_Mat, Point2f &mc, const AlgoParams *params) {

	vector<vector<Point>> contour;
	vector<Vec4i> hierarchy2;

	float Mask_foot_lenght;
	Mat Img_input;
	in.copyTo(Img_input);
	findContours(Img_input, contour, hierarchy2, CV_RETR_LIST, CHAIN_APPROX_NONE);
	if (contour.size() == 0)
	{
		return 0;
	}
	else
	{
		int index = 0;
		int max_val = 0;
		// find Maximum value in the vector contour size
		for (int i = 0; i < contour.size(); i++)
		{
			if (contour[i].size() >= max_val)
			{
				max_val = contour[i].size();
				index = i;
			}
		}
		/*Mat mask_segmented=Mat(params->virtual_img_HEIGTH, params->virtual_img_WIDTH,CV_8U);
		warpPerspective(a4_struct.img_segmented,mask_segmented,warp_Mat,mask_segmented.size());*/

		auto mask_contour = warpsagoma_img(contour[index], img_final, warp_Mat, params->virtual_img_HEIGTH, params->virtual_img_WIDTH, params->crop_px);

		Rect bb_mask_contour = boundingRect(mask_contour);
		if (params->debug >= 1) {
			cvtColor(img_final, img_final, cv::COLOR_GRAY2BGR);
			rectangle(img_final, bb_mask_contour.tl(), bb_mask_contour.br(), Scalar(0, 0, 255), 2);
			imsave(img_final, "Bounding Box Piede Mask");
		}
		//Moments mu = moments(mask_contour, true);
		////  Get the mass centers:
		//mc = Point2f(mu.m10 / mu.m00, mu.m01 / mu.m00);
		//// calculation of the length of the foot based on if the feet is compeltely inside the sheet 
		
			Mask_foot_lenght = calibrated_dimension(bb_mask_contour.width, params->sheet_size_WIDTH, params->calibrated_length, params->crop_px);

		return { Mask_foot_lenght };
	}
}
/*
Applica scala al modello e calcola le distanze tra i vertici rappresentati i  punti anatomici
* @param params: puntatore ai parametri dell'algoritmo.
* @param dimensioni: array contenete le distanze calcolate.
*/
void deform_Model_Calc_Dimensions(float(&dimensioni)[NUM], const AlgoParams *params) {
	
		//Variabili modello 3D
		Mesh Model3D;
		//char * str;
		static char  str[512];
		if (params->debug == 1)
			strncpy(str, params->debug_save_path,512);
		else
			strncpy(str, "temp/", 512); 
		
		int r = load_model(params->foot_sx, Model3D, params->Model_3D_file); 
		
		if (r == 1)
		{
			cout << "Model not loaded" << endl;
			
		}
		
		/* ----------------------------------------------------------------
		 * ------------------ CAlculation of scales factors --------------------
		 * ----------------------------------------------------------------
		 */
		// calcolo fattore di proiezione sulla bounding box delle distanze lunghezza e larghezza,altezza
		float a, b,c,length,width;
		a = cos(params->angle_Width * pi / 180.0);
		b = cos(params->angle_Length * pi / 180.0);
		c = cos(params->angle_Height * pi / 180.0);
		width= distance(Model3D.vertices(), &params->indices_Mesh_Width[0], &params->indices_Mesh_Width[params->indices_Mesh_Width.size() - 1]);
		// fattore di proporzione del modello
		const auto current_y_scale =  float((dimensioni[2])/(width*a));
		auto current_mesh = scaleY(Model3D, (current_y_scale));
		length = distance(current_mesh.vertices(), &params->indices_Mesh_Length[0], &params->indices_Mesh_Length[params->indices_Mesh_Length.size() - 1]);

		
		const auto current_x_scale = float((dimensioni[0])/(length *b) );
		auto current_mesh_final = scaleX(current_mesh,   current_x_scale);
		dimensioni[1] = distance(current_mesh_final.vertices(), &params->indices_Mesh_Height[0], &params->indices_Mesh_Height[params->indices_Mesh_Height.size() - 1])*c;
		dimensioni[4] = distance(current_mesh_final.vertices(), &params->indices_Mesh_Ball_girth_circumference[0], &params->indices_Mesh_Ball_girth_circumference[params->indices_Mesh_Ball_girth_circumference.size()-1]);
		dimensioni[3] = distance(current_mesh_final.vertices(), &params->indices_Mesh_Instep_circumference[0], &params->indices_Mesh_Instep_circumference[params->indices_Mesh_Instep_circumference.size() - 1]);

		save_obj(current_mesh_final, strcat(str, "Modello3D.obj"), 1.0);
		
	
}




//Point find_center_a4_on_image_segmented(Point p0, Point p1, Point p2, Point p3)
//{
//	float px, py;
//	px = ((p0.x * p1.y - p0.y * p1.x)*(p2.x - p3.x) - (p0.x - p1.x)*(p2.x*p3.y - p2.y * p3.x)) / ((p0.x - p1.x)*(p2.y - p3.y) - (p0.y - p1.y)*(p2.x - p3.x));
//	py = ((p0.x * p1.y - p0.y * p1.x)*(p2.y - p3.y) - (p0.y - p1.y)*(p2.x*p3.y - p2.y * p3.x)) / ((p0.x - p1.x)*(p2.y - p3.y) - (p0.y - p1.y)*(p2.x - p3.x));
//	return {static_cast<int> (px), static_cast<int>(py)};
//}
/*
RIcerca all'i terno della immagine clusterizzatsa la forma del piede.
* @param params: puntatore ai parametri dell'algoritmo.
* @param a4_1: struttura contenete imaagine originale , vertici, maschera e immagine egmentata.
*/
void find_ROI_sheet_plus_feet_shape(Process_A4_s &a4_1, const AlgoParams *params) {


	Mat tt;
	Mat plot_contour = Mat::zeros(a4_1.im.size(), CV_8UC3);
	Mat plot_contour2 = Mat::zeros(a4_1.im.size(), CV_8UC3);
	Mat plot_contour22 = Mat::zeros(a4_1.im.size(), CV_8UC3);
	Mat plot_contour3 = Mat::zeros(a4_1.im.size(), CV_8UC3);
	Mat plot_contour4 = Mat::zeros(a4_1.im.size(), CV_8UC3);

	/*if (params->debug >= 1)
		imsave(a4_1.img_segmented, "K clustering on whole image");*/

	// get the color on the center of the A4 sheet where more likely there will be  the feet
	Point cc_on_img_segmented;// find_center_a4_on_image_segmented(a4_1.vertices[0], a4_1.vertices[1], a4_1.vertices[2], a4_1.vertices[3]);
	auto r = getIntersectionPoint(a4_1.vertices[0], a4_1.vertices[2], a4_1.vertices[1], a4_1.vertices[3], cc_on_img_segmented);
	int re = cc_on_img_segmented.x;// -static_cast<int>(a4_1.vertices[0].x - params->crop_px);
	Vec3b color = a4_1.img_segmented.at<Vec3b>(cc_on_img_segmented.y, re);
	inRange(a4_1.img_segmented, color, color, plot_contour);
	int width_area = (static_cast<int>(a4_1.vertices[2].x) - static_cast<int>(a4_1.vertices[0].x));
	int heigth_area = (max(static_cast<int>(a4_1.vertices[2].y), static_cast<int>(a4_1.vertices[0].y)));
	Rect roi = Rect(a4_1.vertices[0].x, 0, width_area, heigth_area);
	
	// get the mask of the feet and rotated rect bounding box
	Mat mask (a4_1.im.size(), CV_8U, Scalar ( 0 ));
	vector<Point2i> area{
		{static_cast<int>(a4_1.vertices[0].x), 0},
		{static_cast<int>(a4_1.vertices[0].x+width_area),0},
		{static_cast<int>(a4_1.vertices[0].x + width_area) ,heigth_area},
		{static_cast<int>(a4_1.vertices[0].x),  heigth_area}
	};
	const Scalar Color = { 255,255,255 };
	fillConvexPoly(mask,area, Color);
	

	 
	plot_contour.copyTo(plot_contour22, mask);
	//cvtColor(plot_contour22, a4_1.feet_mask.mask, COLOR_GRAY2BGR);
	
	plot_contour22.copyTo(a4_1.feet_mask.mask);
	vector<vector<Point>> contour;
	vector<Vec4i> hierarchy;
	findContours(plot_contour22, contour, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	sort(begin(contour), end(contour), [](const vector<Point>& c1, const vector<Point>& c2) {

		return c1.size() > c2.size();
	});

	//drawContours(plot_contour4, contour, 0, Scalar(128, 255, 255), 8, 8);



	// Find the rotation angle of the feet

	RotatedRect rr = minAreaRect(contour[0]);
	Point2f rr_vertices_f[4];
	Point rr_vertices[4];
	rr.points(rr_vertices_f);
	for (int i = 0; i < 4; ++i) {
		rr_vertices[i] = rr_vertices_f[i];
	}
	
	/*Mat rot = getRotationMatrix2D(rr.center, -90-rr.angle , 1);
	warpAffine(plot_contour22, plot_contour3, rot, plot_contour22.size());
	warpAffine(a4_1.feet_mask.mask, a4_1.feet_mask.mask, rot, a4_1.feet_mask.mask.size());*/
	//findContours(plot_contour3, contour, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	findContours(plot_contour22, contour, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
	sort(begin(contour), end(contour), [](const vector<Point>& c1, const vector<Point>& c2) {

		return c1.size() > c2.size();
	});
	for (int j = 0; j < 4; j++)
	{
		line(plot_contour3, rr_vertices[j], rr_vertices[(j + 1) % 4], Scalar(255, 255, 255), 3, 8);

	}
	Rect bb_feet = boundingRect(contour[0]);
	rectangle(plot_contour22, bb_feet, (0, 0, 255), 2,8); 

	a4_1.feet_mask.bb = bb_feet;
	if (params->debug >= 1)
		imsave(plot_contour22, "Feet roi image-rotated and bounding box");
	
	
	
	/* GRAB CUT Algoritms version.....*/
//	Mat result;
//	Mat bgModel, fgModel;
////	Rect roi = Rect(a4_1.vertices[0].x, 0, (static_cast<int>(a4_1.vertices[2].x) - static_cast<int>(a4_1.vertices[0].x)), (max(static_cast<int>(a4_1.vertices[2].y), static_cast<int>(a4_1.vertices[0].y))));
//	rectangle(plot_contour3, roi, Scalar(128, 255, 255), 8);
//	grabCut(a4_1.im, result, roi, bgModel, fgModel, 1, cv::GC_INIT_WITH_RECT);
//	cv::compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
//
//	cv::Mat foreground(a4_1.im.size(), CV_8UC3, cv::Scalar(255, 255, 255));
//	a4_1.im.copyTo(foreground, result);
//	if (params->debug >= 1)
//		imsave(foreground, "Grabcut algoritmo");
	/* .............................*/

	
}

//Mat find_ROI_sheet_plus_feet_shape( Process_A4_s &a4_1, const AlgoParams *params) {
//
//
//	Mat tt;
//	Mat plot_contour = Mat::zeros(a4_1.im.size(), CV_8UC3);
//	Mat plot_contour2 = Mat::zeros(a4_1.im.size(), CV_8UC3);
//	Mat plot_contour3 = Mat::zeros(a4_1.im.size(), CV_8UC3);
//
//	Point cc_on_img_segmented;// = find_center_a4_on_image_segmented(a4_1.vertices[0], a4_1.vertices[1], a4_1.vertices[2], a4_1.vertices[3]);
//	auto r = getIntersectionPoint(a4_1.vertices[0], a4_1.vertices[2], a4_1.vertices[1], a4_1.vertices[3], cc_on_img_segmented);
//	int re = cc_on_img_segmented.x;// -static_cast<int>(a4_1.vertices[0].x - params->crop_px);
//	Vec3b color = a4_1.img_segmented.at<Vec3b>(cc_on_img_segmented.y,re);
//	inRange(a4_1.img_segmented,color,color, plot_contour);
//		vector<vector<Point>> contour;
//		vector<Vec4i> hierarchy;
//	findContours(plot_contour, contour, hierarchy, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);
//	sort(begin(contour), end(contour), [](const vector<Point>& c1, const vector<Point>& c2) {
//			
//				return c1.size() > c2.size();
//			});
//	RotatedRect rr= minAreaRect(contour[0]);
//	Point2f rect_points[4]; rr.points(rect_points);
//	drawContours(plot_contour2, contour, 0, Scalar(128, 255, 255), 8, 8);
//	for (int j = 0; j < 4; j++)
//		line(plot_contour2, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
//
//	/* GRAB CUT Algoritms version.....*/
//	Mat result;
//	Mat bgModel, fgModel;
//	Rect roi = Rect(a4_1.vertices[0].x , 0, (static_cast<int>(a4_1.vertices[2].x) - static_cast<int>(a4_1.vertices[0].x)), (max(static_cast<int>(a4_1.vertices[2].y), static_cast<int>(a4_1.vertices[0].y))));
//	rectangle(plot_contour3, roi, Scalar(128, 255, 255), 8);
//	grabCut(a4_1.im, result, roi, bgModel, fgModel, 1, cv::GC_INIT_WITH_RECT);
//	cv::compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
//
//	cv::Mat foreground(a4_1.im.size(), CV_8UC3, cv::Scalar(255, 255, 255));
//	a4_1.im.copyTo(foreground, result);
//	/* .............................*/
//	
//	return plot_contour2;
//
//
//
//	// load template shape and find contours to use cv::Mastchshapes
//
//	//	
//	//	tt = imread("C:\\Users\\Massimo\\Desktop\\template.jpg", CV_LOAD_IMAGE_GRAYSCALE);
//	//	if (params->foot_sx == 1)
//	//		flip(tt,tt, -1);
//
//	//	vector<vector<Point>> contour_template;
//	//	vector<Vec4i> hierarchy_template;
//	//	int maxIdx_template;
//	//	Canny(tt, tt, 7 * 10, 7 * 22);
//	//	findContours(tt, contour_template, hierarchy_template, RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
//	//	maxIdx_template = 0;
//	//	// TOGLIERE IL SORT E FARE RICERCA MASSIMO
//	//	sort(begin(contour_template), end(contour_template), [](const vector<Point>& c1, const vector<Point>& c2) {
//	//		//	return contourArea(c1, false) < contourArea(c2, false);
//	//		return c1.size() > c2.size();
//	//	});
//	//	drawContours(plot_contour2, contour_template, 0, Scalar(128, 255, 255), 8, 8);
//	//
//	////	
//
//	////	//minMaxIdx(contour_template, &minVal,&maxVal,&minIdx,&maxIdx);
//	////	// find contour in image segmented
//	//	Mat img_org;
//	//	a4_1.im.copyTo(img_org);
//	//	a4_1.img_segmented.copyTo(img_org);
//	//	vector<vector<Point>> contour_img;
//	//	vector<Vec4i> hierarchy_img;
//	//	
//	//	//autoLevels(img_org);
//	//	Mat grayscale_image;
//
//	//	cvtColor(img_org, grayscale_image, CV_BGR2GRAY);
//	//
//	//	Canny(grayscale_image, grayscale_image, 50, 150); //durata 3ms (compreso dilate ed erode)
//	//	dilate(grayscale_image, grayscale_image, Mat());
//	//	erode(grayscale_image, grayscale_image, Mat());
//
//
//	////	
//	//////	findContours(grayscale_image, contour_img, hierarchy_img, RETR_TREE, CV_CHAIN_APPROX_TC89_KCOS);
//	//	findContours(grayscale_image, contour_img, hierarchy_img, RETR_CCOMP, CHAIN_APPROX_NONE);
//	//	vector<double> vd,vd_hausdorff;
//	//	vector<int> id_cnt_image;
//
//
//	////
//
//	//	sort(begin(contour_img), end(contour_img), [](const vector<Point>& c1, const vector<Point>& c2) {
//	//		//return contourArea(c1, false) > contourArea(c2, false);
//	//		return c1.size() > c2.size();
//	//	});
//	//	/* COnvex hull to close conotur*/
//	//	
//	//	
//	//	for (int idx = 0; idx < 4; idx++)
//	//	{
//	//		vector<Point> res;
//	//		Scalar color(rand() & 255, rand() & 255, rand() & 255);
//	//		drawContours(plot_contour, contour_img, idx, color, 8, 8);
//	//		//convexHull(contour_img[idx], res);
//	//		//result[idx].swap(res);
//	//	//	drawContours(grayscale_image, contour_img, idx, color, FILLED, 8);
//	//		vd.push_back(matchShapes(contour_template[0], contour_img[idx], CV_CONTOURS_MATCH_I2, 0));
//	//		vd_hausdorff.push_back(distance_hausdorff(contour_template[0], contour_img[idx]));
//	//		//drawContours(plot_contour3, res, 0, Scalar(0, 222, 0), FILLED, 8);
//	//		//polylines(plot_contour3, contour_img[idx], true, color);
//
//	//	
//	//	}
//	//	
//	//	//zernike *zer_mon = new zernike();
//	//	
//	//	int minElementIndex = min_element(vd.begin(), vd.end()) -vd.begin();
//	//	int id =  minElementIndex+1;
//	//	cout << "VALORE  " << minElementIndex <<" ID "<<id << endl;
//	//	drawContours(plot_contour2, contour_img, id, Scalar(0, 222, 0), FILLED, 8);
//	//	drawContours(plot_contour3,  result, id, Scalar(0, 222, 0), FILLED, 8);
//
//	}

/**
* Funzione di calcolo dell'area di interesse che nel caso di piede fuori dal foglio
* @param a4_1: struttura che contiene elaborazioen della immagine in input e maskera foglio.
* 
* @return vector<Point> area su cui fare il warping*/
vector<Point2f>  get_roi_sheet_feet(Process_A4_s &a4_1, const AlgoParams *params)
{
	Point top_left, top_right;
	Point top_right_bounging_box;
	Point top_left_bounging_box;
	top_left_bounging_box.x = a4_1.feet_mask.bb.tl().x;
	top_left_bounging_box.y = a4_1.feet_mask.bb.tl().y - 50;
	top_right_bounging_box.x = a4_1.feet_mask.bb.tl().x + a4_1.feet_mask.bb.width;
	top_right_bounging_box.y = a4_1.feet_mask.bb.tl().y - 50; // METTERE A POSTO
	auto r = getIntersectionPoint(a4_1.vertices[0], a4_1.vertices[3], top_left_bounging_box, top_right_bounging_box, top_left);
	r = getIntersectionPoint(a4_1.vertices[1], a4_1.vertices[2], top_left_bounging_box, top_right_bounging_box, top_right);
	vector<Point2f> result;
	result.push_back(top_left);
	result.push_back(top_right);
	result.push_back(a4_1.vertices[2]);
	result.push_back(a4_1.vertices[3]);
	
	if (params->debug >= 1) {

		Mat cim = a4_1.im.clone();

		for (int i = 0; i < result.size(); ++i) {
			
			circle(cim, result[i], 30, { 0, 0, 0 }, 5, LINE_AA);
			char txt[20];
			snprintf(txt, 20, "%i", i);
			putText(cim, txt, result[i] + Point2f{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		}

		imsave(cim, "New warping area  A4");
	}
	
	return (result);
		
}

/**
* Funzione di individuazione dei pixels corrispondenti al lato lungo del foglio
* @param a4_1.veritces: vertici del foglio.
* @param wrp_M: MAtrice di warping.

* @return float numero di pixel corrispondenti al lato lungo del foglio*/
float get_pixels_side_sheet(vector<Point2f> vertices, Mat wrp_M) {

	vector<Point2f> vertices_warped;
	perspectiveTransform(vertices, vertices_warped, wrp_M);
	
	float length_sheet = distance(vertices_warped[0], vertices_warped[3]);
	return length_sheet;
}
vector<Point>convertPoint2f_in_Point2i(vector<Point2f> v)
{
	vector<Point> res;
	for (Point2f p : v)
		res.push_back({ static_cast<int> (p.x), static_cast<int> (p.y)});
	return(res);
}
/**
* Funzione di calcolo dimensione piede sia con maschera che con elaborazioen metodo della sigmoide su mashera foglio
* @param dimensioni:array delle dimensioni
* @param a4_1: struttura che contiene elaborazioen della immagine in input e maskera foglio.
* @param params:parametri di setting per il corretto funzionamento algoritmo
* @return flag di  riuscita calcolo lunghezza e larghezza*/

int getFootDimensions ( float(&dimensioni)[NUM], Process_A4_s &a4_1, AlgoParams *params) {
	
	// Variables
	Mat sheet_foot_unwarped;
	int flagimg = 0;
	Mat img = a4_1.im;
	Mat virual_img_sagoma = Mat(params->virtual_img_HEIGTH, params->virtual_img_WIDTH, CV_8UC1);
	Mat virual_img_mask = Mat(params->virtual_img_HEIGTH, params->virtual_img_WIDTH, CV_8UC1);
	Mat wrp_M;
	float Mask_foot_lenght;
	Point2f center_mass;
	bool whole_foot_inside_sheet;

	/*...............Find Foot Mask .......
	refine the search due to the fact the feet can be outside the sheet......*/
	/*Mat m = K_Cluster(params, a4_1);
	m.copyTo(a4_1.img_segmented);*/
	
	find_ROI_sheet_plus_feet_shape(a4_1, params);
	
	// Check if foot is inside the sheet
	//if ((max(static_cast<int>(a4_1.vertices[0].y), static_cast<int>(a4_1.vertices[1].y)))>= (a4_1.feet_mask.bb.tl().y))
	if ((!a4_1.foot_inside_sheet))
	{

		whole_foot_inside_sheet = false;
		Mat mask = Mat::zeros(img.size(), CV_8UC1);
		
		vector<Point2f> roi_feet = get_roi_sheet_feet(a4_1,params);
		vector<Point> roi_area_int = convertPoint2f_in_Point2i(roi_feet);
		whole_foot_inside_sheet= false;
		const Scalar Color = { 255,255,255 };

		fillConvexPoly(mask, roi_area_int, Color);


		// Applico la maschera della segmentazione del foglio A4, in modo da elaborare solo il piede sul foglio
		//img.copyTo(sheet_foot_unwarped, mask);
		a4_1.img_segmented.copyTo(sheet_foot_unwarped, mask);
		// Calcolo Matrice di trasformazione WARPING su schermo
		wrp_M = warping_Mat(roi_feet, params->virtual_img_WIDTH, params->virtual_img_HEIGTH);
		
		params->calibrated_length = get_pixels_side_sheet(a4_1.vertices, wrp_M);
		Mask_foot_lenght = stima_dim_on_mask(a4_1.feet_mask.mask, virual_img_mask, wrp_M, center_mass, params);
	}
	else
	
	{
				whole_foot_inside_sheet = true;
	
				Mat mask = Mat::zeros(img.size(), CV_8UC1);
				vector<Point2i> sheet{
					a4_1.vertices[0], 
					a4_1.vertices[1],
					a4_1.vertices[2],
					a4_1.vertices[3]
				};

	
	
				const Scalar Color = { 255,255,255 };
				fillConvexPoly(mask, sheet, Color);
				

				// Applico la maschera della segmentazione del foglio A4, in modo da elaborare solo il piede sul foglio
				img.copyTo(sheet_foot_unwarped, mask);

				// Calcolo Matrice di trasformazione WARPING su schermo
				wrp_M = warping_Mat(a4_1.vertices,params->virtual_img_WIDTH, params->virtual_img_HEIGTH);
				/*----------------------------------------------------
				CAlcolo lunghezza su maschera di Massimo
				----------------------------------------------------*/
				params->calibrated_length = params->virtual_img_WIDTH;
				Mask_foot_lenght = stima_dim_on_mask(a4_1.a4_mask.mask, virual_img_mask, wrp_M, center_mass, params);

	}
	

	
	vector <Point2f>  sheet_vertices_warped;
	Mat sheet_foot_warped;
	warpPerspective(sheet_foot_unwarped, sheet_foot_warped,wrp_M,virual_img_mask.size());
	
	perspectiveTransform(a4_1.vertices, sheet_vertices_warped, wrp_M);
	
	/*----------------------------------------------------
	   Processing image ROI with sigmoid!!!
	----------------------------------------------------*/
	int stima = 0;
	if (a4_1.foot_inside_sheet)

		stima = stima_dim_shadow_removal_virtual_image_inside_feet(sheet_foot_warped, &flagimg, wrp_M, virual_img_sagoma, params, dimensioni, center_mass, sheet_vertices_warped);
	else
		stima = stima_dim_shadow_removal_virtual_image(sheet_foot_warped, &flagimg, wrp_M, virual_img_sagoma, params, dimensioni, center_mass, sheet_vertices_warped);

	//const auto stima = stima_dim_shadow_removal(processing_Mat, &flagimg, wrp_M, virual_img_sagoma,params, dimensioni);
	a4_1.qualitaimg = flagimg;

	

	if ((stima == 1)) // errori 
		if (Mask_foot_lenght == 0)
			return{ ERROR_NO_FOOT };
		else
		{
			dimensioni[0] = Mask_foot_lenght;
			dimensioni[1] = 0;
			dimensioni[2] = 0;
			dimensioni[3] = 0;
			dimensioni[4] = 0;
			
			a4_1.qualitaimg |= 16;

		}
	else
		
		deform_Model_Calc_Dimensions(dimensioni, params);
	dimensioni[5] = Mask_foot_lenght;		
	return 0;

}

/**
* Funzione di parsing linea comando, obbiettivo trovare il comando e restituisce il puntatore al argomento del comando.
* @begin:inizio linea comando
* @end :fine linea comando
* @option: opzione da trovare
* @return puntatore al relativo comando*/

char* getCmdOption(char ** begin, char ** end, const string & option)
{
	char ** itr = find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const string& option)
{
	return std::find(begin, end, option) != end;
}

/**
* Funzione di calcolo dei relativi fattori di proporzione per tipologia di fogli usat per la calibrazione delle misure.
* @param params:parametri di setting per il corretto funzionamento algoritmo
* @WIDTH: dimensioni lunghezza foglio
* @HEITH: dimensioni larghezza foglio
* @vr_img: dimensione minima immagine virtuale
* @return void*/

void AdjRatioVirtualImage(AlgoParams *params, const float WIDTH, const float HEIGTH, const int vr_img)
{
	float ratio = (WIDTH / HEIGTH);
	params->virtual_img_HEIGTH = vr_img;
	params->virtual_img_WIDTH = static_cast<int> (ratio * vr_img);
	params->sheet_size_WIDTH = WIDTH;
	params->sheet_size_HEIGTH = HEIGTH;

}

/**************************************************************************************************
//**************************************************************************************************
//***************   Massimo Bortolato - nuove aggiunte 6/11/2018   *********************************
//
//**************************************************************************************************/






//--------------------------------------------------------------------------------------------------
//struct A4_2_s {
//	Mat gray;
//	vector<Point2d> vertices;
//};



void plot_points(Mat im,vector<Point> pts,Scalar color, Point cc, const AlgoParams *params)
{
	if (params->debug >= 1) {
		for (Point p : pts)
		{
			circle(im, p, 10, color, 5, LINE_AA);

		}
		int w = im.cols;
		int h = im.rows;
		
	
		
		circle(im, cc, 10, { 255,100,255 }, 5, LINE_AA);
		imsave(im, "Plot collected Points A4");
	}
	
}
A4_2_s extract_A4_old(Mat &im, const AlgoParams *params, bool &errors) {


	Mat gray;
	cvtColor(im, gray, COLOR_RGB2GRAY);

	//const double pi = 3.14159265358979323846264338327950288;

	int w = im.cols;
	int h = im.rows;
	//Point cc(w/2, h/2);     // oppure centroide della maschera ottenuta con thresholding
	Point cc(w / 2, h / 2);     // oppure centroide della maschera ottenuta con thresholding

	auto collect_edge_points = [&](int start_angle) {

		vector<Point> pts;

		for (int i = start_angle; i < start_angle + 90; ++i) {
			auto th = 2.0*pi / 360.0*i;
			Point2d v(cos(th), sin(th));
			Point2d cc_(cc.x, cc.y);

			int M = 0;
			double T = 0.0;
			int prev_val = gray.at<uchar>(cc.y, cc.x);
			for (double t = 1.0; ; t += 10.0) {
				Point2d p_ = cc_ + t * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));

				if (p.x < 0 || p.y < 0 || p.x >= w || p.y >= h) break;

				int act_val = gray.at<uchar>(p.y, p.x);
				int delta = 0;
				if (act_val < prev_val) delta = prev_val - act_val;

				if (delta > M) {
					M = delta;
					T = t - 1;
				}
				prev_val = act_val;
			}


			if (T > 0.0 && M > 20) {
				Point2d p_ = cc_ + T * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));
				pts.push_back(p);
			}
		}


		return pts;
	};


	auto north_pts = collect_edge_points(45);
	auto west_pts = collect_edge_points(45 + 90);
	auto south_pts = collect_edge_points(45 + 180);
	auto east_pts = collect_edge_points(45 + 270);
	/*--------------------*/
	if (params->debug >= 1) {
		Mat img;
		im.copyTo(img);
		plot_points(img, north_pts, { 255,0,0 }, cc, params);
		plot_points(img, west_pts, { 0,255,0 }, cc, params);
		plot_points(img, south_pts, { 255,0,255 }, cc, params);
		plot_points(img, east_pts, { 0,0,255 }, cc, params);
	}
	/*--------------------*/
	auto my_sort = [](vector<vector<Point>*> &all_pts) {
		sort(all_pts.begin(), all_pts.end(), [](vector<Point> *a, vector<Point> *b) {
			return a->size() > b->size();
		});
	};

	vector<Line_s> lines(4);
	vector<vector<Point>> assigned_points(4);

	vector<vector<Point>*> all_pts = { &north_pts, &west_pts, &south_pts, &east_pts };


	Mat temp;
	im.copyTo(temp);
	for (int i = 0; i < lines.size(); ++i) {
		my_sort(all_pts);
		lines[i] = fit_line_w_ransac_2(*all_pts[0], 100, 10.0);
		if (params->debug >= 1) {
			draw_line(temp, lines[i], 3000, { 0, 0, 255 });
		}
		assigned_points[i] = assign_to_line(lines[i], 30.0, all_pts);
	}

	if (params->debug >= 1) {
		imsave(temp, "Lines detected first Method Massimo estract_2");

	}
	struct Corner_s {
		Point2d p;
		double angle;
	};

	vector<Corner_s> corners;
	for (int i = 0; i < lines.size(); ++i) {
		for (int j = i + 1; j < lines.size(); ++j) {
			auto c = compute_corner(lines[i], lines[j]);
			if (c.x > 0 && c.x < w && c.y>0 && c.y < h) {
				Point2d cc_(cc.x, cc.y);
				auto d = c - cc_;
				double angle = (atan2(d.y, d.x) * 180.0 / pi) + 180.0;
				corners.push_back({ c, angle });
			}
		}
	}
	sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
		return a.angle < b.angle;
	});

	// check if the picture has the foot and leght on top part!!
	//Point2d p1 = corners[0].p;
	//Point2d p2 = corners[1].p;
	//Point2d medio = Point2d{ (p1.x + p2.x) / 2,(p1.y + p2.y) / 2 };
	////VEc3b *pt_img=img.ptr<Vec3b>(i);
	//Point2i midPointNorthSide = { static_cast<int>(medio.x),static_cast<int>(medio.y) };
	//Vec3b pixelValueNorthSide = img.at<Vec3b>(midPointNorthSide.y-50,midPointNorthSide.x);
	//Vec3b pixelValueCentralPicture= img.at<Vec3b>( img.rows / 2, img.cols / 2);
	//double distancePixelValues = norm(pixelValueCentralPicture, pixelValueNorthSide,CV_L2);
	//if (distancePixelValues < 100)
	//{// foot on top part therefore flip image! to be done in next function
	//	sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
	//		return a.angle < b.angle; });
	//}
	//else
	//{
	//	swap(corners[0], corners[3]);
	//	swap(corners[1], corners[2]);
	//}
	// controllo diagonali e rapporto lati
	bool its_good = false;
	if (corners.size() == 4) {
		its_good = true;



		auto dd1 = corners[0].p - corners[2].p;
		auto dd2 = corners[1].p - corners[3].p;
		auto dd1l = sqrt(dd1.dot(dd1));
		auto dd2l = sqrt(dd2.dot(dd2));
		auto dd_err = abs(dd1l - dd2l) / (dd1l + dd2l) * 200.0;
		//if (dd_err > 3.0) its_good = false;
		if (dd_err > 6.0) its_good = false;
		else {
			auto dd1_1 = corners[0].p - corners[1].p;
			auto dd1_2 = corners[1].p - corners[2].p;
			auto dd2_1 = corners[2].p - corners[3].p;
			auto dd2_2 = corners[3].p - corners[0].p;
			auto dd1l = (sqrt(dd1_1.dot(dd1_1)) + sqrt(dd2_1.dot(dd2_1))) / 2.0;
			auto dd2l = (sqrt(dd1_2.dot(dd1_2)) + sqrt(dd2_2.dot(dd2_2))) / 2.0;
			auto ratio = dd2l / dd1l;

			if ((strcmp(params->sheet_format, "A4") == 0) || (strcmp(params->sheet_format, "A5") == 0))
				if (ratio > 1.6 || ratio < 1.10) its_good = false;  // rapporto foglio A4 = 1.4  // TO DO MEttere controllo troppo prospettiva
				else
					its_good = true;
			else
				if (strcmp(params->sheet_format, "LETTER") == 0)
					if (ratio > 1.50 || ratio < 1.0) its_good = false;  // rapporto foglio USA_L = 1.3
					else
						its_good = true;
			

		}
	}


	if (params->debug >= 1) {

		Mat cim = im.clone();

		for (int i = 0; i < corners.size(); ++i) {
			auto c = corners[i].p;
			circle(cim, c, 30, { 0, 0, 0 }, 5, LINE_AA);
			char txt[20];
			snprintf(txt, 20, "%i", i);
			putText(cim, txt, c + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		}

		imsave(cim, "A4 vertices detected by estract 2");
	}

	errors = its_good;
	A4_2_s ret;
	ret.gray = gray;
	for (auto &c : corners) ret.vertices.push_back(c.p);
	return ret;
}


A4_2_s extract_A4_new(Mat &im,Mask_s roi, const AlgoParams *params, bool &errors) {


	Mat gray;
	cvtColor(im, gray, COLOR_RGB2GRAY);

	int w = roi.bb.tl().x + roi.bb.width;
	int h = roi.bb.tl().y +roi.bb.height;
	int tlMinX = roi.bb.tl().x - 50;
	int tlMinY = roi.bb.tl().y - 50;
	int brMaxX = roi.bb.br().x + 50;
	int brMaxY = roi.bb.br().y+ 50;
	if (tlMinX < 0) tlMinX = 0;
	if (tlMinY < 0) tlMinY = 0;
	if (brMaxX > im.cols) brMaxX = im.cols;
	if (brMaxY > im.rows) brMaxY = im.rows;
 // oppure centroide della maschera ottenuta con thresholding
	Point cc(roi.bb.tl().x + (roi.bb.width / 2), roi.bb.tl().y + (roi.bb.height / 2));
	
	auto collect_edge_points = [&](int start_angle) {

		vector<Point> pts;

		for (int i = start_angle; i < start_angle + 90; ++i) {
			auto th = 2.0*pi / 360.0*i;
			Point2d v(cos(th), sin(th));
			Point2d cc_(cc.x, cc.y);

			int M = 0;
			double T = 0.0;
			/*int max_delta_pos = 0;
			int max_delta_neg = 0;*/
			int prev_val = gray.at<uchar>(cc.y, cc.x);
			for (double t = 1.0; ; t += 10.0) {
				Point2d p_ = cc_ + t * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));

			//	if (p.x < 0 || p.y < 0 || p.x >= im.cols || p.y >= im.rows) break;
				if (p.x < tlMinX || p.y < tlMinY || p.x >= brMaxX || p.y >= brMaxY) break;
				int act_val = gray.at<uchar>(p.y, p.x);
				int delta = 0;
				/*int delta_pos = 0;
				int delta_neg = 0;*/
				if (act_val < prev_val) delta = prev_val - act_val;

				//if (act_val < prev_val) delta_neg = prev_val - act_val;
				//else
				//	delta_pos =  act_val- prev_val ;

				/*if (delta_neg > max_delta_neg) {
					max_delta_neg = delta_neg;
					T = t - 1;
				}
				if (delta_pos > max_delta_pos) {
					max_delta_pos = delta_pos;
					
				}*/

				if (delta > M) {
					M = delta;
					T = t - 1;
				}
				prev_val = act_val;
			}

			/*if (T > 0.0 && max_delta_neg > 20 && max_delta_pos>30) {
				Point2d p_ = cc_ + T * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));
				pts.push_back(p);
			}*/
			if (T > 0.0 && M > 20) {
				Point2d p_ = cc_ + T * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));
				pts.push_back(p);
			}
		}


		return pts;
	};


	auto north_pts = collect_edge_points(45);
	auto west_pts = collect_edge_points(45 + 90);
	auto south_pts = collect_edge_points(45 + 180);
	auto east_pts = collect_edge_points(45 + 270);
	/*--------------------*/
	if (params->debug >= 1) {
		Mat img;
		im.copyTo(img);
		plot_points(img, north_pts, { 255,0,0 }, cc, params);
		plot_points(img, west_pts, { 0,255,0 }, cc, params);
		plot_points(img, south_pts, { 255,0,255 }, cc, params);
		plot_points(img, east_pts, { 0,0,255 }, cc, params);
	}
	
	
	/*--------------------*/
	//auto my_sort = [](vector<vector<Point>> &all_pts) {
	//	sort(all_pts.begin(), all_pts.end(), [](vector<Point> a, vector<Point> b) {
	//		return a.size() > b.size();
	//	});
	//};



	vector<Line_s> lines(4);
	vector<vector<Point>> assigned_points(4);
	vector<vector<Point>> all_pts(4);
	all_pts = { north_pts, west_pts, south_pts, east_pts };

	Mat temp;
	im.copyTo(temp);
	for (int i = 0; i < lines.size(); ++i) {
	//	my_sort(all_pts);
	//	lines[i] = fit_line_w_ransac_3(all_pts[i], 100, 10.0, i%2);
		lines[i] = fit_line_w_ransac_2(all_pts[i], 100, 10.0);
		if (params->debug >= 1) {
			draw_line(temp, lines[i], 3000, { 0, 0, 255 });
			
		}
	//	assigned_points[i] = assign_to_line(lines[i], 30.0, all_pts);
		assigned_points[i] = assign_to_line_2(lines[i], 30.0, all_pts[i]);
	}
	if (params->debug >= 1) {
		imsave(temp, "Lines detected with ransac  ");

	}

	struct Corner_s {
		Point2d p;
		double angle;
	};

	vector<Corner_s> corners;
	for (int i = 0; i < lines.size(); ++i) {
		for (int j = i + 1; j < lines.size(); ++j) {
			auto c = compute_corner(lines[i], lines[j]);
			if (c.x > 0 && c.x < im.cols && c.y>0 && c.y < im.rows) {
				Point2d cc_(cc.x, cc.y);
				auto d = c - cc_;
				double angle = (atan2(d.y, d.x) * 180.0 / pi) + 180.0;
				corners.push_back({ c, angle });
			}
		}
	}

	sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
		return a.angle < b.angle;
	});
	 // controllo diagonali e rapporto lati
	bool its_good=false;
	if (corners.size() == 4) {
		its_good = true;

		

		auto dd1 = corners[0].p - corners[2].p;
		auto dd2 = corners[1].p - corners[3].p;
		auto dd1l = sqrt(dd1.dot(dd1));
		auto dd2l = sqrt(dd2.dot(dd2));
		auto dd_err = abs(dd1l - dd2l) / (dd1l + dd2l) * 200.0;
		//if (dd_err > 3.0) its_good = false;
		if (dd_err > 6.0) its_good = false;
		else {
			auto dd1_1 = corners[0].p - corners[1].p;
			auto dd1_2 = corners[1].p - corners[2].p;
			auto dd2_1 = corners[2].p - corners[3].p;
			auto dd2_2 = corners[3].p - corners[0].p;
			auto dd1l = (sqrt(dd1_1.dot(dd1_1)) + sqrt(dd2_1.dot(dd2_1))) / 2.0;
			auto dd2l = (sqrt(dd1_2.dot(dd1_2)) + sqrt(dd2_2.dot(dd2_2))) / 2.0;
			auto ratio = dd2l / dd1l;

			if ((strcmp(params->sheet_format,"A4")==0) || (strcmp(params->sheet_format, "A5")==0))
				if (ratio > 1.6 || ratio < 1.20) its_good = false;  // rapporto foglio A4 = 1.4  // TO DO MEttere controllo troppo prospettiva
				else
					its_good = true;
			else
				if (strcmp(params->sheet_format, "LETTER")==0)
					if (ratio > 1.50 || ratio < 1.10) its_good = false;  // rapporto foglio USA_L = 1.3
					else
						its_good = true;
		


		}
	}
	


	if (params->debug >= 1) {

		Mat cim = im.clone();
		
		for (int i = 0; i < corners.size(); ++i) {
			auto c = corners[i].p;
			circle(cim, c, 30, { 0, 0, 0 }, 5, LINE_AA);
			char txt[20];
			snprintf(txt, 20, "%i", i);
			putText(cim, txt, c + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		}

		imsave(cim, " A4 vertices detected ");
	}
	errors = its_good;
	A4_2_s ret;
	ret.gray = gray;
	for (auto &c : corners) ret.vertices.push_back(c.p);
	return ret;
}



void cleanPoints(vector<Point> &pts, const Mat imgSegmented,const  Point cc) {

	
	Vec3b pxValueFoot = imgSegmented.at<Vec3b>(cc.y, cc.x);
	Vec3b pxValue = 0;
	for (int i =0;i<pts.size(); i++)
		{
			
			pxValue = imgSegmented.at<Vec3b>(pts[i].y, pts[i].x);
			if ((pxValue[0] == pxValueFoot[0]) && (pxValue[1] == pxValueFoot[1])&& (pxValue[2] == pxValueFoot[2]))
				pts.erase(pts.begin() + i);
		}
}

int extract_sheet(Process_A4_s * sheet_s, Mask_s roi, const AlgoParams *params, vector<Point2d>& vecVertices_d) {


	Mat gray;
	cvtColor(sheet_s->im, gray, COLOR_RGB2GRAY);
	sheet_s->gray = gray;
	int w = roi.bb.tl().x + roi.bb.width;
	int h = roi.bb.tl().y + roi.bb.height;
	int tlMinX = roi.bb.tl().x - 50;
	int tlMinY = roi.bb.tl().y - 50;
	int brMaxX = roi.bb.br().x + 50;
	int brMaxY = roi.bb.br().y + 50;
	if (tlMinX < 0) tlMinX = 0;
	if (tlMinY < 0) tlMinY = 0;
	if (brMaxX > sheet_s->im.cols) brMaxX = sheet_s->im.cols;
	if (brMaxY > sheet_s->im.rows) brMaxY = sheet_s->im.rows;
	
	Point cc(roi.bb.tl().x + (roi.bb.width / 2), roi.bb.tl().y + (roi.bb.height / 2));

	auto collect_edge_points = [&](int start_angle) {

		vector<Point> pts;

		for (int i = start_angle; i < start_angle + 90; ++i) {
			auto th = 2.0*pi / 360.0*i;
			Point2d v(cos(th), sin(th));
			Point2d cc_(cc.x, cc.y);

			int M = 0;
			double T = 0.0;

			int prev_val = gray.at<uchar>(cc.y, cc.x);
			for (double t = 1.0; ; t += 10.0) {
				Point2d p_ = cc_ + t * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));

				if (p.x < tlMinX || p.y < tlMinY || p.x >= brMaxX || p.y >= brMaxY) break;
				int act_val = gray.at<uchar>(p.y, p.x);
				int delta = 0;
	
				if (act_val < prev_val) delta = prev_val - act_val;

				
				if (delta > M) {
					M = delta;
					T = t - 1;
				}
				prev_val = act_val;
			}


			if (T > 0.0 && M > 20) {
				Point2d p_ = cc_ + T * v;
				Point p(static_cast<int>(p_.x + 0.5), static_cast<int>(p_.y + 0.5));
				pts.push_back(p);
			}
		}


		return pts;
	};


	auto north_pts = collect_edge_points(45);
	auto west_pts = collect_edge_points(45 + 90);
	auto south_pts = collect_edge_points(45 + 180);
	auto east_pts = collect_edge_points(45 + 270);
	/*--------------------*/
	if (params->debug >= 1) {
		Mat img;
		sheet_s->im.copyTo(img);
		plot_points(img, north_pts, { 255,0,0 }, cc, params);
		plot_points(img, west_pts, { 0,255,0 }, cc, params);
		plot_points(img, south_pts, { 255,0,255 }, cc, params);
		plot_points(img, east_pts, { 0,0,255 }, cc, params);
	}

	cleanPoints(north_pts, sheet_s->img_segmented, cc);
	cleanPoints(west_pts, sheet_s->img_segmented, cc);
	cleanPoints(south_pts, sheet_s->img_segmented, cc);
	cleanPoints(east_pts, sheet_s->img_segmented, cc);

	if (params->debug >= 1) {
		Mat img2;
		sheet_s->im.copyTo(img2);
		plot_points(img2, north_pts, { 255,0,0 }, cc, params);
		plot_points(img2, west_pts , { 0,255,0 }, cc, params);
		plot_points(img2, south_pts, { 255,0,255 }, cc, params);
		plot_points(img2, east_pts , { 0,0,255 }, cc, params);
	}
	vector<Line_s> lines(4);
	vector<vector<Point>> assigned_points(4);
	vector<vector<Point>> all_pts(4);
	all_pts = { north_pts, west_pts, south_pts, east_pts };
	
	
	for (int i = 0; i < lines.size(); ++i) {

		lines[i] = fit_line_w_ransac_2(all_pts[i], 100, 10.0);
		assigned_points[i] = assign_to_line_2(lines[i], 30.0, all_pts[i]);
	}
	if (params->debug >= 1) {
		Mat temp;
		sheet_s->im.copyTo(temp);
		for (int i = 0; i < lines.size(); ++i) {

				draw_line(temp, lines[i], 3000, { 0, 0, 255 });

			}
		imsave(temp, "Lines detected with ransac  ");

	}

	struct Corner_s {
		Point2d p;
		double angle;
	};

	vector<Corner_s> corners;
	for (int i = 0; i < lines.size(); ++i) {
		for (int j = i + 1; j < lines.size(); ++j) {
			auto c = compute_corner(lines[i], lines[j]);
			if (c.x > 0 && c.x < sheet_s->im.cols && c.y>0 && c.y < sheet_s->im.rows) {
				Point2d cc_(cc.x, cc.y);
				auto d = c - cc_;
				double angle = (atan2(d.y, d.x) * 180.0 / pi) + 180.0;
				corners.push_back({ c, angle });
			}
		}
	}

	sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
		return a.angle < b.angle;
	});
	// controllo diagonali e rapporto lati
	bool its_good = false;
	if (corners.size() == 4) {
		its_good = true;



		auto dd1 = corners[0].p - corners[2].p;
		auto dd2 = corners[1].p - corners[3].p;
		auto dd1l = sqrt(dd1.dot(dd1));
		auto dd2l = sqrt(dd2.dot(dd2));
		auto dd_err = abs(dd1l - dd2l) / (dd1l + dd2l) * 200.0;
		//if (dd_err > 3.0) its_good = false;
		if (dd_err > 6.0) its_good = false;
		else {
			auto dd1_1 = corners[0].p - corners[1].p;
			auto dd1_2 = corners[1].p - corners[2].p;
			auto dd2_1 = corners[2].p - corners[3].p;
			auto dd2_2 = corners[3].p - corners[0].p;
			auto dd1l = (sqrt(dd1_1.dot(dd1_1)) + sqrt(dd2_1.dot(dd2_1))) / 2.0;
			auto dd2l = (sqrt(dd1_2.dot(dd1_2)) + sqrt(dd2_2.dot(dd2_2))) / 2.0;
			auto ratio = dd2l / dd1l;

			if ((strcmp(params->sheet_format, "A4") == 0) || (strcmp(params->sheet_format, "A5") == 0))
				if (ratio > 1.6 || ratio < 1.20) its_good = false;  // rapporto foglio A4 = 1.4  // TO DO MEttere controllo troppo prospettiva
				else
					its_good = true;
			else
				if (strcmp(params->sheet_format, "LETTER") == 0)
					if (ratio > 1.50 || ratio < 1.10) its_good = false;  // rapporto foglio USA_L = 1.3
					else
						its_good = true;



		}
	}



	if (params->debug >= 1) {

		Mat cim = sheet_s->im.clone();

		for (int i = 0; i < corners.size(); ++i) {
			auto c = corners[i].p;
			circle(cim, c, 30, { 0, 0, 0 }, 5, LINE_AA);
			char txt[20];
			snprintf(txt, 20, "%i", i);
			putText(cim, txt, c + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
		}

		imsave(cim, " A4 vertices detected ");
	}

	if (its_good)
	{
		for (auto &c : corners) vecVertices_d.push_back(c.p);
		return 0;
	}
	else
		return 1;
	

	
}

//--------------------------------------------------------------------------------------------------//
//A4_2_s extract_A4_2(Mat &im, const AlgoParams *params) {
//
//
//	Mat gray;
//	cvtColor(im, gray, COLOR_RGB2GRAY);
//
//	//const double pi = 3.14159265358979323846264338327950288;
//
//	int w = im.cols;
//	int h = im.rows;
//	//Point cc(w/2, h/2);     // oppure centroide della maschera ottenuta con thresholding
//	Point cc(w / 2, h / 2);     // oppure centroide della maschera ottenuta con thresholding
//
//	auto collect_edge_points = [&](int start_angle) {
//
//		vector<Point> pts;
//
//		for (int i=start_angle; i<start_angle+90; ++i) {
//			auto th = 2.0*pi/360.0*i;
//			Point2d v(cos(th), sin(th));
//			Point2d cc_(cc.x, cc.y);
//
//			int M = 0;
//			double T = 0.0;
//			int prev_val = gray.at<uchar>(cc.y, cc.x);
//			for (double t=1.0; ; t+=10.0) {
//				Point2d p_ = cc_ + t*v;
//				Point p(static_cast<int>(p_.x+0.5), static_cast<int>(p_.y+0.5));
//
//				if (p.x<0 || p.y<0 || p.x>=w || p.y>=h) break;
//
//				int act_val = gray.at<uchar>(p.y, p.x);
//				int delta = 0;
//				if (act_val < prev_val) delta = prev_val - act_val;
//
//				if (delta>M) {
//					M = delta;
//					T = t-1;
//				}
//				prev_val = act_val;
//			}
//
//
//			if (T > 0.0 && M > 20) {
//				Point2d p_ = cc_ + T*v;
//				Point p(static_cast<int>(p_.x+0.5), static_cast<int>(p_.y+0.5));
//				pts.push_back(p);
//			}
//		}
//
//		
//		return pts;
//	};
//
//
//	auto north_pts = collect_edge_points(45);
//	auto west_pts = collect_edge_points(45+90);
//	auto south_pts = collect_edge_points(45+180);
//	auto east_pts = collect_edge_points(45+270);
//	/*--------------------*/
//	Mat img;
//	im.copyTo(img);
//	plot_points(img, north_pts,{ 255,0,0 }, cc, params);
//	plot_points(img, west_pts, { 0,255,0 },  cc,params);
//	plot_points(img, south_pts,{ 255,0,255 },cc,params);
//	plot_points(img, east_pts, { 0,0,255 },  cc,params);
//	/*--------------------*/
//	auto my_sort = [](vector<vector<Point>*> &all_pts) {
//		sort(all_pts.begin(), all_pts.end(), [](vector<Point> *a, vector<Point> *b) {
//			return a->size() > b->size();
//		});
//	};
//
//	vector<Line_s> lines(4);
//	vector<vector<Point>> assigned_points(4);
//
//	vector<vector<Point>*> all_pts = {&north_pts, &west_pts, &south_pts, &east_pts};
//	
//
//	Mat temp;
//	im.copyTo(temp);
//	for (int i=0; i<lines.size(); ++i) {
//		my_sort(all_pts);
//		lines[i] = fit_line_w_ransac_2(*all_pts[0], 100, 10.0);
//		if (params->debug >= 1) {
//			draw_line(temp, lines[i],  3000, { 0, 0, 255 });
//		}
//		assigned_points[i] = assign_to_line(lines[i], 30.0, all_pts);
//	}
//
//	if (params->debug >= 1) {
//		imsave(temp, "Lines detected first Method Massimo estract_2");
//
//	}
//	struct Corner_s {
//		Point2d p;
//		double angle;
//	};
//
//	vector<Corner_s> corners;
//	for (int i=0; i<lines.size(); ++i) {
//		for (int j=i+1; j<lines.size(); ++j) {
//			auto c = compute_corner(lines[i], lines[j]);
//			if (c.x>0 && c.x<w && c.y>0 && c.y<h) {
//				Point2d cc_(cc.x, cc.y);
//				auto d = c - cc_;
//				double angle = (atan2(d.y, d.x) * 180.0 / pi) + 180.0;
//				corners.push_back({c, angle});
//			}
//		}
//	}
//
//	
//
//	//if (corners.size() != 4) return {};
//
//
//	//sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
//	//	return a.angle < b.angle;
//	//});
//
//
//	//// diagonali
//	//auto dd1 = corners[0].p - corners[2].p;
//	//auto dd2 = corners[1].p - corners[3].p;
//	//auto dd1l = sqrt(dd1.dot(dd1));
//	//auto dd2l = sqrt(dd2.dot(dd2));
//	//auto dd_err = abs(dd1l-dd2l) / (dd1l+dd2l) * 200.0;
//	//if (dd_err > 3.0) return {};
//	 // controllo diagonali e rapporto lati
//	bool its_good;
//	if (corners.size() == 4) {
//		its_good = true;
//
//		sort(corners.begin(), corners.end(), [](const Corner_s &a, const Corner_s &b) {
//			return a.angle < b.angle;
//		});
//
//		auto dd1 = corners[0].p - corners[2].p;
//		auto dd2 = corners[1].p - corners[3].p;
//		auto dd1l = sqrt(dd1.dot(dd1));
//		auto dd2l = sqrt(dd2.dot(dd2));
//		auto dd_err = abs(dd1l - dd2l) / (dd1l + dd2l) * 200.0;
//		if (dd_err > 3.0) its_good = false;
//
//		else {
//			auto dd1_1 = corners[0].p - corners[1].p;
//			auto dd1_2 = corners[1].p - corners[2].p;
//			auto dd2_1 = corners[2].p - corners[3].p;
//			auto dd2_2 = corners[3].p - corners[0].p;
//			auto dd1l = (sqrt(dd1_1.dot(dd1_1)) + sqrt(dd2_1.dot(dd2_1))) / 2.0;
//			auto dd2l = (sqrt(dd1_2.dot(dd1_2)) + sqrt(dd2_2.dot(dd2_2))) / 2.0;
//			auto ratio = dd2l / dd1l;
//			if (ratio > 1.50 || ratio < 1.30) its_good = false;  // rapporto foglio A4 = 1.4
//		}
//	}
//	/*---
//	// Check sul parallelismo
//	auto l1 = corners[3].p - corners[0].p;
//	auto l2 = corners[2].p - corners[1].p;
//	auto a1 = atan2(l1.y, l1.x);
//	auto a2 = atan2(l2.y, l2.x);
//	auto a12 = abs(a1 - a2) * 180 / pi;
//
//	cout << a1*180/pi << " " << a2*180/pi << "\t\t" << a12 << endl;
//
//	auto l3 = corners[1].p - corners[0].p;
//	auto l4 = corners[2].p - corners[3].p;
//	auto a3 = atan2(l3.y, l3.x);
//	auto a4 = atan2(l4.y, l4.x);
//	auto a34 = abs(a3 - a4) * 180 / pi;
//
//	cout << a3*180/pi << " " << a4*180/pi << "\t\t" << a34 << endl;
//	cout << endl;
//	*/
//
//
//if (params->debug >= 1) {
//
//	Mat cim = im.clone();
//
//	for (int i = 0; i < corners.size(); ++i) {
//		auto c = corners[i].p;
//		circle(cim, c, 30, { 0, 0, 0 }, 5, LINE_AA);
//		char txt[20];
//		snprintf(txt, 20, "%i", i);
//		putText(cim, txt, c + Point2d{ 20,-10 }, FONT_HERSHEY_PLAIN, 5.0, { 0,0,0 }, 3, LINE_AA);
//	}
//
//	imsave(cim, "A4 vertices detected by estract 2");
//}
//
//A4_2_s ret;
//ret.gray = gray;
//for (auto &c : corners) ret.vertices.push_back(c.p);
//return ret;
//}



//--------------------------------------------------------------------------------------------------
A4_2_s refine_A4_2(const Mat &im, A4_2_s &a4, const AlgoParams *params) {

	if (a4.vertices.size() != 4) return a4;
	auto &gray = a4.gray;
	auto &S = params->findCorners_roiSize;

	TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.0001);
	vector<Point2f> vv;
	for (auto &v : a4.vertices) vv.push_back(Point2f(static_cast<float>(v.x), static_cast<float>(v.y)));
	cornerSubPix(gray, vv, Size(15, 15), Size(-1, -1), termcrit);
	a4.vertices.clear();
	for (auto &v : vv) a4.vertices.push_back(Point2d(static_cast<double>(v.x), static_cast<double>(v.y)));

	if (params->debug >= 1) {

		vector<Mat> channels = { gray, gray, gray };
		Mat cim;
		merge(channels, cim);

		for (auto &v : a4.vertices) {
			Rect r(v.x - S, v.y - S, 2 * S + 1, 2 * S + 1);
			rectangle(cim, r, { 0,0,255 }, 1, LINE_AA);
			circle(cim, v, 3, { 0,255,0 }, 2, LINE_AA);
		}

		for (auto &v : vv) {
			circle(cim, v, 3, { 0,0,255 }, 2, LINE_AA);
		}

		imsave(cim, "Refine for A4 vertices CORNERSUBPIXEL");
	}

	return a4;
}


int refineVertices(Process_A4_s * sheet_s, const AlgoParams *params, vector<Point2d> &vecVertices_d) {

	if (vecVertices_d.size() != 4) return 1;
	auto &gray = sheet_s->gray;
	auto &S = params->findCorners_roiSize;

	TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.0001);
	vector<Point2f> vv;
	for (auto &v : vecVertices_d) vv.push_back(Point2f(static_cast<float>(v.x), static_cast<float>(v.y)));
	cornerSubPix(gray, vv, Size(15, 15), Size(-1, -1), termcrit);
	sheet_s->vertices.clear();
	for (auto &v : vv) sheet_s->vertices.push_back(Point2d(static_cast<double>(v.x), static_cast<double>(v.y)));

	if (params->debug >= 1) {

		vector<Mat> channels = { gray, gray, gray };
		Mat cim;
		merge(channels, cim);

		for (auto &v : sheet_s->vertices) {
			Rect r(v.x - S, v.y - S, 2 * S + 1, 2 * S + 1);
			rectangle(cim, r, { 0,0,255 }, 1, LINE_AA);
			circle(cim, v, 3, { 0,255,0 }, 2, LINE_AA);
		}

		for (auto &v : vv) {
			circle(cim, v, 3, { 0,0,255 }, 2, LINE_AA);
		}

		imsave(cim, "Refine for A4 vertices CORNERSUBPIXEL");
	}

	return 0;
}



int process_for_a4_new(Process_A4_s* args,Mask_s roi, const AlgoParams *params) {// (void *args_) {

	//Process_A4_Args_s *args = (Process_A4_Args_s*)args_;

	auto &im = args->im;

	//--- PREPROCESSING --------------------------------
	/// Viene fatto un piccolo preprocessing:
	/// bilanciamento del colore e blur 3x3
	im = color_balance(im);
	if (params->debug >= 1) imsave(im, "Balanced");

	medianBlur(im, im, 3);


	//--- FOGLIO A4 ------------------------------------

//	auto a4_sopra= extract_A4_2(im, params);
	//bool corners_detected=false;
	vector<Point2d> vecVertices_d;
	int ret= extract_sheet(args, roi, params, vecVertices_d);
	//auto a4_s = extract_A4_new(im,roi, params, corners_detected); // in this version roi will provie the center of the rectangle

	if (ret==1)
		return MB_ERROR_NO_A4;
	/*if (!corners_detected)
		return MB_ERROR_NO_A4;*/
	// CHECK IF THERE IS A FOOT ON /TRHOUGH 
	//auto a4_mask= extract_A4(im, params);
	auto a4_mask = roi;
	//a4_s = refine_A4_2(im, a4_s, params);
	ret=refineVertices(args, params, vecVertices_d);
	if (ret == 1)
		return MB_ERROR_NO_A4;
	auto convert_point_type = [](const vector<Point2d> &dpts) {
		vector<Point2f> fpts(dpts.size());
		for (int i = 0; i < dpts.size(); ++i) {
			fpts[i].x = static_cast<float>(dpts[i].x);
			fpts[i].y = static_cast<float>(dpts[i].y);
		}
		return fpts;
	};


	if ((args->vertices.size() != 4))
	{
		auto a4_vertices = find_a4_corners(im, a4_mask, params);
		if ((a4_vertices.size() == 4))
		{
			/*flip_image(a4_vertices, a4_mask, im, params);*/
			swap(a4_vertices[2], a4_vertices[3]);
			args->im = im;
			args->a4_mask = a4_mask;
			args->vertices = a4_vertices;
			args->qualitaimg = 0;

		}
		else
			return MB_ERROR_NO_A4;
	}
	else
	{
		auto a4_vertices = convert_point_type(vecVertices_d);
		/*	flip_image(a4_vertices, a4_mask, im, args->params);
			swap(a4_vertices[2], a4_vertices[3]);*/
		args->im = im;
		args->a4_mask = a4_mask;
		args->vertices = a4_vertices;
		args->qualitaimg = 0;
	}

	///	PROVA_HOUGH(*args);
	return MB_ERROR_OK;

}
//--------------------------------------------------------------------------------------------------
/// Routine che estrae i vertici del foglio A4 dall'immagine
/// Riempie la struttura Process_A4_Args_s con i risultati
int process_for_a4_old(Process_A4_s* args, const AlgoParams *params){// (void *args_) {


	auto &im = args->im;

	//--- PREPROCESSING --------------------------------
	/// Viene fatto un piccolo preprocessing:
	/// bilanciamento del colore e blur 3x3
	im = color_balance(im);
	if (params->debug >= 1) imsave(im, "Balanced");

	medianBlur(im, im, 3);


	//--- FOGLIO A4 ------------------------------------
	bool corners_detected = false;
	
	auto a4_s = extract_A4_old(im, params, corners_detected);
	if (!corners_detected)
		return MB_ERROR_NO_A4;
	// CHECK IF THERE IS A FOOT ON /TRHOUGH 
	auto a4_mask = extract_A4(im, params);
	a4_s = refine_A4_2(im, a4_s, params);
	
	auto convert_point_type = [](const vector<Point2d> &dpts) {
		vector<Point2f> fpts(dpts.size());
		for (int i = 0; i < dpts.size(); ++i) {
			fpts[i].x = static_cast<float>(dpts[i].x);
			fpts[i].y = static_cast<float>(dpts[i].y);
		}
		return fpts;
	};

	if ((a4_s.vertices.size() != 4))
	{
		auto a4_vertices = find_a4_corners(im, a4_mask, params);
		if ((a4_vertices.size() == 4))
		{
			//flip_image(a4_vertices, a4_mask, im, params);
			//swap(a4_vertices[2], a4_vertices[3]);
			args->im = im;
			args->a4_mask = a4_mask;
			args->vertices = a4_vertices;
			args->qualitaimg = 0;

		}
		else
			return MB_ERROR_NO_A4;
	}
	else
	{
		auto a4_vertices = convert_point_type(a4_s.vertices);
			//flip_image(a4_vertices, a4_mask, im, params);
			//swap(a4_vertices[2], a4_vertices[3]);
		args->im = im;
		args->a4_mask = a4_mask;
		args->vertices = a4_vertices;
		args->qualitaimg = 0;
	}

///	PROVA_HOUGH(*args);
	return MB_ERROR_OK;

}


Rect find_bb_over_blobs(Mat list, int blob1, int blob2)
{
	
	int tl_x, tl_y, width, height;
	int tr_x, bl_y;
	/*tl_x = min(list[blob1][CC_STAT_LEFT], list[blob2][CC_STAT_LEFT]);
	tl_y = min(list[blob1][CC_STAT_TOP], list[blob2][CC_STAT_TOP]);
	tr_x = max(list[blob1][CC_STAT_LEFT] + list[blob1][CC_STAT_WIDTH],
		list[blob2][CC_STAT_LEFT] + list[blob2][CC_STAT_WIDTH]);
	width = tr_x - tl_x;
	bl_y = max(list[blob1][CC_STAT_LEFT] + list[blob1][CC_STAT_HEIGHT],
		list[blob2][CC_STAT_LEFT] + list[blob2][CC_STAT_HEIGHT]);
	height = bl_y - tl_y;*/
	tl_x = min(list.at<int32_t>(blob1,CC_STAT_LEFT), list.at<int32_t>(blob2, CC_STAT_LEFT));
	tl_y = min(list.at<int32_t>(blob1, CC_STAT_TOP), list.at<int32_t>(blob2, CC_STAT_TOP));
	tr_x = max(list.at<int32_t>(blob1, CC_STAT_LEFT) + list.at<int32_t>(blob1, CC_STAT_WIDTH),
		list.at<int32_t>(blob2, CC_STAT_LEFT) + list.at<int32_t>(blob2, CC_STAT_WIDTH));
	width = tr_x - tl_x;
	bl_y = max(list.at<int32_t>(blob1, CC_STAT_TOP) + list.at<int32_t>(blob1, CC_STAT_HEIGHT),
		list.at<int32_t>(blob2, CC_STAT_TOP) + list.at<int32_t>(blob2, CC_STAT_HEIGHT));
	height = bl_y - tl_y;
	return{ tl_x,tl_y,width,height };

}

/**
* Funzione  che estrae individua la zona dove si trova il Foglio A4/USA-letter  nell'immagine.
* @param im:imagine scattata
* @param  params:setting algoritmo
Per torvarla si costruiscono 4 vettori partendo dalla matrice dei connectedcompoenetswithStats
si cercano i primi 4 blobs di area maggiore e si salvano in  stats_ordered ed in area_blob_ordered
poi si calcolano le distance tra i centroidi e si salvano in vec_dist_centroids;
si calcolano le distance tra i punto medio delle distanza con la linea centrale della immagine e si ettono in vector<float> vec_dist_centroids_midline
* @return area e bounging bx dell'area ROI*/

static Mask_s extract_ROI_sheet(Process_A4_s &a4, const AlgoParams *params) {
	
	Mat tmp;
	Mat im;
	a4.im.copyTo(im);
	Mat th = do_threshold(im);
	/***************************/
	
	erode(th, tmp, Mat::ones(3, 3, CV_8U));
	dilate(tmp, th, Mat::ones(3, 3, CV_8U));

	Mat cnn;
	Mat stats, stats_idx;
	Mat centroids;
	const int n = connectedComponentsWithStats(th, cnn, stats, centroids, 4);

	//const int32_t Area = im.cols*im.rows;
	const int32_t minArea_toprocess = (im.cols*im.rows) * params->a4_minAreaPerCent / 100;
	// sort stats_idx in descending order  stats_idx will contain the index of the first 4 bigger blob area
	
	sortIdx(stats, stats_idx, CV_SORT_EVERY_COLUMN + CV_SORT_DESCENDING);
	

	vector<Vec6i>  stats_ordered;
	vector<int> area_blob_ordered;
	vector<Point2f>  centroids_ordered;

	
	Vec6i t;
	Point2f c;
	int num_max_blobs = params->num_max_blobs;
	if (num_max_blobs >= stats.rows)
		num_max_blobs = stats.rows - 1;
	/*stats_ord  reflects the index order of the first 4 ( num_max_blob) bigger area blobs of stats vector */
	for (int i = 1; i <= num_max_blobs; ++i) {

		const int area = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), CC_STAT_AREA);
		t = { 0,0,0,0, 0, stats_idx.at<int32_t>(i, 4) };
		c = Point2f{ 0,0 };
		if (area > minArea_toprocess) {

			t[0] = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 0);
			t[1] = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 1);
			t[2] = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 2);
			t[3] = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 3);
			t[4] = stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 4);
			t[5] = stats_idx.at<int32_t>(i, 4);
			c.x = centroids.at<double>(stats_idx.at<int32_t>(i, 4), 0);
			c.y = centroids.at<double>(stats_idx.at<int32_t>(i, 4), 1);
			area_blob_ordered.push_back(stats.at<int32_t>(stats_idx.at<int32_t>(i, 4), 4));
			stats_ordered.push_back(t);
			centroids_ordered.push_back(c);
		}

	}


	Mat stats_ord = stats.col(CC_STAT_AREA) > minArea_toprocess;
	Mat mask(im.size(), CV_8UC1, Scalar(0));

	for (int i = 1; i < n; i++)
	{
		if (stats_ord.at<uchar>(i, 0))
		{
			mask = mask | (cnn == i);
		}
	}
	/*********************************************************************/
	/*Mask Detection if feet inside or partially outside the sheet*/
	/*Process the distance from centroids for each blob detected, precisely the first 4(num_max_blob)
	vec_blob_combination={index blob 1 ,index blob 2}
	vec_dist_centroids={, distance centroids index blob 1 and index blob 2 }
	vect_midpoints {mid point X distance centroids , mid point X distance centroids}*/
	
	vector<Vec2i> vec_blob_combination;
	vector<float> vec_dist_centroids;
	//vector<float> vec_dist_centroids_midline;
	vector<Point2f> vec_midpoints;
	int min_dist = 0; // distance from the central x of the image
	float center_line_img = im.cols / 2;
	float num_rows = static_cast<float>(im.rows);
	Vec3i el = { 0,0 };
	int idx_final;


	for (int i = 0; i <= stats_ordered.size() - 1; ++i) {
		if (i == 0)
		{
			vec_blob_combination.push_back(Vec2i{ 0,0 });
			vec_dist_centroids.push_back(0); // distance centroid from same centroid
		}
		for (int j = i + 1; j < stats_ordered.size(); ++j)
		{
			//check if the distance is crossed by centerline of the image
			if (((centroids_ordered[j].x <= center_line_img) && (centroids_ordered[j - 1].x >= center_line_img)) |
				((centroids_ordered[j].x >= center_line_img) && (centroids_ordered[j - 1].x <= center_line_img)))

			{
				vec_blob_combination.push_back(Vec2i{ i,j });
				vec_dist_centroids.push_back(abs(distance(centroids_ordered[j], centroids_ordered[j - 1])));
				vector<Point2f> dist_centroids;
				dist_centroids.push_back(centroids_ordered[j]);
				dist_centroids.push_back(centroids_ordered[j - 1]);
					}
		}

	}
	/*Detection blobls to undestand if feet inside or partially outside the sheet*/
	int idx_min_dist, idx_min_dist_centerline;
	if (vec_dist_centroids.size() == 0)
	{
		idx_min_dist = 0;
		Rect bb = { 0,0,0,0 };
		if (params->debug >= 1) {
			Mat cth;
			
			cvtColor(mask, cth, COLOR_GRAY2RGB);
			rectangle(cth, bb, Vec3b(0, 0, 255), 1, LINE_AA);
			imsave(cth, "BLOBS AREAS with rectangles");

		}

		return { th, bb };
	}
	else
	{
		if (vec_dist_centroids.size() == 1)
		
			idx_final = 0;
		else

		{
			vector<float>::iterator iter = next(vec_dist_centroids.begin(), 1);
			idx_min_dist = std::distance(vec_dist_centroids.begin(), min_element(iter, vec_dist_centroids.end()));
			if (mask.at<uchar>(static_cast<int>(centroids_ordered[0].y), static_cast<int>(centroids_ordered[0].x)) == 0)

				idx_final = 0;
			else
				idx_final = idx_min_dist;
		}
	}

	

		/*Mat cnn_copy_removed_first_row;
	cnn(Range(1, cnn.rows), Range(0, cnn.cols)).copyTo(cnn_copy_removed_first_row);*/
	Vec2i el_dist_centroid = vec_blob_combination[idx_final];
	int blob1 = stats_ordered[ el_dist_centroid[0]][5];
	int blob2 = stats_ordered[el_dist_centroid[1]][5];
	//Mat mask_final(cnn_copy_removed_first_row.size(), CV_8UC1, Scalar(0));
	Mat mask_final(cnn.size(), CV_8UC1, Scalar(0));
	for (int i = 1; i < n; i++)
	{
		if (stats_ord.at<uchar>(i, 0))
			//		mask_final = mask_final | (cnn_copy_removed_first_row == blob1) | (cnn_copy_removed_first_row == blob2);
			mask_final = mask_final | (cnn == blob1) | (cnn == blob2);

	}
	Rect bb;
	if (idx_final == 0)
	{
		
		bb.x = stats.at<int32_t>(blob1, CC_STAT_LEFT);
		bb.y = stats.at<int32_t>(blob1, CC_STAT_TOP);
		bb.width = stats.at<int32_t>(blob1, CC_STAT_WIDTH);
		bb.height = stats.at<int32_t>(blob1, CC_STAT_HEIGHT);
		a4.foot_inside_sheet = true;
	}
	else
	{
		double areaBlob1= stats.at<int32_t>(blob1, CC_STAT_AREA);
		double areaBlob2 = stats.at<int32_t>(blob2, CC_STAT_AREA);
		bb = find_bb_over_blobs(stats,blob1,blob2);
		a4.foot_inside_sheet = false;
		
	}
	Mat cth;
	if (params->debug >= 1) {
		
		cvtColor(mask_final, cth, COLOR_GRAY2RGB);
		rectangle(cth, bb, Vec3b(0, 0, 255), 1, LINE_AA);
		imsave(cth, "BLOBS AREAS with rectangles");
	}

	return { cth, bb };
	
}


bool checkBottomSide_OrientationFoot(vector<Point2f> vertices)
{
	/*Line_s leftSideLine = { 0,0,0 };
	Line_s rightSideLine = { 0,0,0 };*/
	
	//vector<Point> vleft = { vertices[0],vertices[3] };
	//vector<Point> vright = { vertices[1],vertices[2] };
	//vleft.push_back(vertices[0]);
	//vleft.push_back(vertices[3]);
	vector<Point> vright,vleft;
	//vector<Point> *pr, *pf;
	vleft.push_back(vertices[0]);
	vleft.push_back(vertices[3]);
	vright.push_back(vertices[2]);
	vright.push_back(vertices[1]);
	auto leftSideLine = fit_line( vleft);
	auto rightSideLine = fit_line( vright);
	auto c = compute_corner(leftSideLine, rightSideLine);
	if (c.y > 0)
		return true;
	else
		return false;

}


/**
* Funzione di che estrae il Foglio A4  dall'immagine.
* @param image_above_fname:imagine del folgio
* @param  params:setting algoritmo
* @param: foglio_a4_1: struttura contenente vertici folgo, immagine originale , qualita immagine , mashera binaria
* @vr_img: dimensione minima immagine virtuale
* @return void*/
int processImage(ClassProcessImage &ProcessImage, const AlgoParams *params) {

	

	// Segmentation with K-means CLUSTERING in order to find the three area (sheet, foot , floor)
	ProcessImage.SetImageSegmented(  kClusterLABimage(ProcessImage.GetOriginalImage());
	//if (params->debug >= 1)
	//{
	//	imsave(img_LAB, "K clustering on LAB_image");

	//}
	if (params->debug >= 1) {

		imsave(ProcessImage.GetImageSegmented(), "Image Segmented");

	}
	// find the area ROI sheet , check if the ROI is in centered in the picture and detect if foot is inside the sheet.
	//auto roi = extract_ROI_sheet(a4_1, params);
	int r=ProcessImage.ExtractROIsheet(params->a4_minAreaPerCent, params->num_max_blobs);
	if (params->debug >= 1) {
		Mat cth;

		ProcessImage.GetMask().copyTo(cth);
		rectangle(cth, ProcessImage.GetROI(), Vec3b(0, 0, 255), 1, LINE_AA);
		imsave(cth, "BLOBS AREAS with rectangles");

	}
	
	int ret1;
	if (ProcessImage.foot_inside_sheet)
		ret1 = process_for_a4_old(&a4_1, params);

	else

		ret1 = process_for_a4_new(&a4_1, roi, params);

	if ((ret1) != MB_ERROR_OK) {
		{
			return MB_ERROR_NO_A4; }
	}



	
	// check if the picture has been taken from another person
	if (!checkBottomSide_OrientationFoot(a4_1.vertices))
	{// foot  part picure taken from another person!!!

		flip(a4_1.im, a4_1.im, -1);
		flip(a4_1.a4_mask.mask, a4_1.a4_mask.mask, -1);
		flip(a4_1.img_segmented, a4_1.img_segmented, -1);
		Point2f temp;
		temp = a4_1.vertices[0];
		a4_1.vertices[0] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[1];
		a4_1.vertices[1] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[2];
		a4_1.vertices[2] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[3];
		a4_1.vertices[3] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);


		swap(a4_1.vertices[1], a4_1.vertices[3]);
		swap(a4_1.vertices[0], a4_1.vertices[2]);
	}
	
	// check if the picture has the foot and leght on top part!! ==> flip!
	Point2f midPointTopLine = midpoint(a4_1.vertices[1], a4_1.vertices[0]);
	Point2f midPointBottomLine = midpoint(a4_1.vertices[3] ,a4_1.vertices[2]);
	

 
	if(midPointBottomLine.y< midPointTopLine.y)
	{
		flip(a4_1.im, a4_1.im, -1);
		flip(a4_1.a4_mask.mask, a4_1.a4_mask.mask, -1);
		flip(a4_1.img_segmented, a4_1.img_segmented, -1);
		Point2f temp;
		temp = a4_1.vertices[0];
		a4_1.vertices[0] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[1];
		a4_1.vertices[1] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[2];
		a4_1.vertices[2] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
		temp = a4_1.vertices[3];
		a4_1.vertices[3] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);

		/*swap(a4_1.vertices[1], a4_1.vertices[2]);
		swap(a4_1.vertices[0], a4_1.vertices[3]);
		swap(a4_1.vertices[2], a4_1.vertices[3]);
		swap(a4_1.vertices[0], a4_1.vertices[1]);*/
		swap(a4_1.vertices[1], a4_1.vertices[3]);
		swap(a4_1.vertices[0], a4_1.vertices[2]);
	
	}
	if (params->debug >= 1) {
		Mat temp;
		a4_1.im.copyTo(temp);
	//	set_imsave_path(params->debug_save_path);
		draw_vertices(temp, a4_1.vertices, { 0, 0, 255 }, -1, true);
		imsave(temp, "Picture to process measurements");

	}
	foglio_a4_1 =  a4_1;

	return MB_ERROR_OK;


}


//int processImage(const char *image_above_fname, const AlgoParams *params, Process_A4_s &foglio_a4_1) {
//
//
//
//	//---- LOAD IMAGE -------------------------------------------------
//	Mat im;
//
//	if (imload_and_resize(image_above_fname, im, params))
//	{
//		return MB_ERROR_LOAD_IMAGE;
//	}
//
//	// initialization struct Process_A4_s
//	Process_A4_s a4_1 = { im, im,{ {},{},{}} };
//	// Segmentation with K-means CLUSTERING in order to find the three area (sheet, foot , floor)
//	a4_1.img_segmented = kClusterLABimage(params, im);
//	if (params->debug >= 1) {
//
//		imsave(a4_1.img_segmented, "Image Segmented");
//
//	}
//	// find the area ROI sheet , check if the ROI is in centered in the picture and detect if foot is inside the sheet.
//	auto roi = extract_ROI_sheet(a4_1, params);
//
//
//	int ret1;
//	if (a4_1.foot_inside_sheet)
//		ret1 = process_for_a4_old(&a4_1, params);
//
//	else
//
//		ret1 = process_for_a4_new(&a4_1, roi, params);
//
//	if ((ret1) != MB_ERROR_OK) {
//		{
//			return MB_ERROR_NO_A4; }
//	}
//
//
//
//
//	// check if the picture has been taken from another person
//	if (!checkBottomSide_OrientationFoot(a4_1.vertices))
//	{// foot  part picure taken from another person!!!
//
//		flip(a4_1.im, a4_1.im, -1);
//		flip(a4_1.a4_mask.mask, a4_1.a4_mask.mask, -1);
//		flip(a4_1.img_segmented, a4_1.img_segmented, -1);
//		Point2f temp;
//		temp = a4_1.vertices[0];
//		a4_1.vertices[0] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[1];
//		a4_1.vertices[1] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[2];
//		a4_1.vertices[2] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[3];
//		a4_1.vertices[3] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//
//
//		swap(a4_1.vertices[1], a4_1.vertices[3]);
//		swap(a4_1.vertices[0], a4_1.vertices[2]);
//	}
//
//	// check if the picture has the foot and leght on top part!! ==> flip!
//	Point2f midPointTopLine = midpoint(a4_1.vertices[1], a4_1.vertices[0]);
//	Point2f midPointBottomLine = midpoint(a4_1.vertices[3], a4_1.vertices[2]);
//
//
//
//	if (midPointBottomLine.y < midPointTopLine.y)
//	{
//		flip(a4_1.im, a4_1.im, -1);
//		flip(a4_1.a4_mask.mask, a4_1.a4_mask.mask, -1);
//		flip(a4_1.img_segmented, a4_1.img_segmented, -1);
//		Point2f temp;
//		temp = a4_1.vertices[0];
//		a4_1.vertices[0] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[1];
//		a4_1.vertices[1] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[2];
//		a4_1.vertices[2] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//		temp = a4_1.vertices[3];
//		a4_1.vertices[3] = Point2f((a4_1.im.cols - 1) - temp.x, (a4_1.im.rows - 1) - temp.y);
//
//		/*swap(a4_1.vertices[1], a4_1.vertices[2]);
//		swap(a4_1.vertices[0], a4_1.vertices[3]);
//		swap(a4_1.vertices[2], a4_1.vertices[3]);
//		swap(a4_1.vertices[0], a4_1.vertices[1]);*/
//		swap(a4_1.vertices[1], a4_1.vertices[3]);
//		swap(a4_1.vertices[0], a4_1.vertices[2]);
//
//	}
//	if (params->debug >= 1) {
//		Mat temp;
//		a4_1.im.copyTo(temp);
//		//	set_imsave_path(params->debug_save_path);
//		draw_vertices(temp, a4_1.vertices, { 0, 0, 255 }, -1, true);
//		imsave(temp, "Picture to process measurements");
//
//	}
//	foglio_a4_1 = a4_1;
//
//	return MB_ERROR_OK;
//
//
//}
