#include "mesh.h"
//#include "imgprocfunctions.h"
#include "utility.h"
#include "loadobj.h"
#include <iostream>

using namespace cv;
using namespace std;





void salva_output_3D(const Mesh &mesh, const string &nomefile, float scala) {
	save_obj(mesh, nomefile.c_str(), scala);
}

Mesh scaleX(const Mesh& mesh, const float current_x_scale)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		p.x *= current_x_scale;
	}
	return result;
}

Mesh scaleY(const Mesh& mesh, const float current_y_scale)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		p.y *= current_y_scale;
	}
	return result;
}

Mesh scale(const Mesh& mesh, const float current_scale)
{
	auto result = mesh;
	for (auto&p : result.vertices_)
	{
		p *= current_scale;
	}
	return result;
}



int load_model(const bool flag_sx,  Mesh &main_object, const char *file_name)
{
	try {
				
		main_object = load_obj(file_name);
		
		//Centratura XY del modello
		auto medio = midpoint(main_object.vertices());

		medio.z = 0; // do not center on Z

		for (auto & vertice : main_object.vertices()) {
			vertice -= medio;
		}
		
	}
	catch (file_not_found_error &err)
	{
		
		cout << "File non trovato " << err.what() << endl;
		return 1;
	}
	if (flag_sx) {
		float miny = 2e9, maxy = 0;
		for (auto & vertice : main_object.vertices()) {
			if (vertice.y > maxy)maxy = vertice.y;
			if (vertice.y < miny)miny = vertice.y;
		}
		for (auto & vertice : main_object.vertices()) {
			vertice.y = maxy - vertice.y + miny; // PAN: Mirrors maintaining min and max y
		}
	}
	
	return 0;
}

