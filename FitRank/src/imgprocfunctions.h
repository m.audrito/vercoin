#pragma once

#ifndef __IMGPROCFUNCTIONS_H__
#define __IMGPROCFUNCTIONS_H__

#include <opencv2/core/mat.hpp>

using namespace cv;
using namespace std;

/**
 * Auto Levels the image histogram, by setting 90% of the samples to be fill the range 0-255 (per channel)
 * @param mat Ingresso/uscita immagine da bilanciare.
 */
void autoLevels(Mat &mat);

void clahe_LChannel(Mat &image);
void BlobDetector(Mat im);
/**
 * Applica un filtro sigmoide all�immagine per enfatizzare il contrasto.
 * @param src Immagine di partenza sorgente.
 * @param dst Variabile per la memorizzazione dell�immagine elaborata.
 * @param E Pendenza del filtro.
 * @param m Centratura sigmoide.
 */
//void sigmoide_contrasto(const Mat& src, Mat& dst, float E, float m);

//cv::Mat color_balance(const Mat& im);
void do_threshold(const Mat& im, Mat& bw);
Mat do_threshold(const Mat& im);
//cv::Mat kClusterLABimage(Mat image);
#endif
